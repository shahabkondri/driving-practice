package com.shahabkondri.drivingpractice.api.identity;

import com.shahabkondri.drivingpractice.entity.identity.AccountRelationship;

public interface AccountRelationshipService {

    AccountRelationship selectAccountRelationshipByEmailAndPassword(String email, String password) throws Exception;

    AccountRelationship selectAccountRelationshipByEmail(String email) throws Exception;

    AccountRelationship selectAccountRelationshipByEmailAndActivationToken(String email, String token) throws Exception;
}
