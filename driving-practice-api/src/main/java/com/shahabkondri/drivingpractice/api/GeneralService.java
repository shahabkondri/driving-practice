package com.shahabkondri.drivingpractice.api;

import com.shahabkondri.drivingpractice.core.data.CriteriaSelect;
import com.shahabkondri.drivingpractice.core.data.DeleteMetaData;
import com.shahabkondri.drivingpractice.core.data.QueryMetaData;
import com.shahabkondri.drivingpractice.core.data.UpdateMetaData;
import com.shahabkondri.drivingpractice.entity.abstracts.BaseIdentity;

import java.io.Serializable;
import java.util.List;

public interface GeneralService extends CriteriaSelect {

    <T extends Serializable> T save(T t) throws Exception;

    <T extends Serializable> List<T> save(List<T> ts) throws Exception;

    <T extends Serializable> T update(T t) throws Exception;

    <T extends Serializable> List<T> update(List<T> ts) throws Exception;

    <T extends Serializable> List<T> saveOrUpdate(List<T> ts) throws Exception;

    <T extends Serializable> T saveOrUpdate(T t) throws Exception;

    <T extends BaseIdentity<?>> void delete(Class<T> clas, Serializable... ids) throws Exception;

    <T extends Serializable> T selectById(Class<T> clas, Serializable id) throws Exception;

    <T extends Serializable> T selectById(Class<T> clas, Serializable id, String graph) throws Exception;

    <T extends Serializable> T selectSingle(QueryMetaData metaData) throws Exception;

    <T extends Serializable> T selectFirst(QueryMetaData metaData) throws Exception;

    <T extends Serializable> List<T> select(QueryMetaData metaData) throws Exception;

    <T extends Serializable> T selectQuerySingle(QueryMetaData metaData) throws Exception;

    <T extends Serializable> T selectQueryFirst(QueryMetaData metaData) throws Exception;

    <T extends Serializable> List<T> selectQuery(QueryMetaData metaData) throws Exception;

    <T extends Serializable> T selectStoredProcedureSingle(QueryMetaData metaData) throws Exception;

    <T extends Serializable> T selectStoredProcedureFirst(QueryMetaData metaData) throws Exception;

    <T extends Serializable> List<T> selectStoredProcedure(QueryMetaData metaData) throws Exception;

    int executeNamedStoredProcedureQuery(QueryMetaData metaData) throws Exception;

    int executeNamedQuery(QueryMetaData metaData) throws Exception;

    int executeQuery(QueryMetaData metaData) throws Exception;

    int update(UpdateMetaData updateMetaData) throws Exception;

    int delete(DeleteMetaData deleteMetaData) throws Exception;

    void flush() throws Exception;

    void clear() throws Exception;
}
