package com.shahabkondri.drivingpractice.api.question;

import com.shahabkondri.drivingpractice.entity.Question;
import com.shahabkondri.drivingpractice.entity.enumerations.ExamType;
import com.shahabkondri.drivingpractice.entity.enumerations.QuestionType;

import javax.ejb.Remote;
import java.util.List;

/**
 * Created by shahab on 5/28/15.
 */

public interface QuestionService {

    List<Question> selectQuestionsByQuestionTypeAndExamType(QuestionType questionType, ExamType examType) throws Exception;

    Question selectQuestionByRowNumberAndQuestionTypeAndQuestionExam(int rowNumber, QuestionType questionType, ExamType examType) throws Exception;

    long selectCount(QuestionType questionType, ExamType examType) throws Exception;


}
