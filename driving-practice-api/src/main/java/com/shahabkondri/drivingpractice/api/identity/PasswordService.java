package com.shahabkondri.drivingpractice.api.identity;


import com.shahabkondri.drivingpractice.entity.identity.Password;

import javax.ejb.Remote;

public interface PasswordService {

    Password selectPasswordByEmail(String email) throws Exception;
}
