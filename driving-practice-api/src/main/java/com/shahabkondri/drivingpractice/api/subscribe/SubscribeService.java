package com.shahabkondri.drivingpractice.api.subscribe;

import com.shahabkondri.drivingpractice.entity.Subscribe;

import javax.ejb.Remote;

/**
 * Created by shahab on 5/28/15.
 */

public interface SubscribeService {

    Subscribe selectSubscribeByEmail(String email) throws Exception;

    Subscribe selectSubscribeByEmailAndToken(String email, String token) throws Exception;
}
