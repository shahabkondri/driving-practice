package com.shahabkondri.drivingpractice.view.bean.subscription;

import com.shahabkondri.drivingpractice.api.GeneralService;
import com.shahabkondri.drivingpractice.api.subscribe.SubscribeService;
import com.shahabkondri.drivingpractice.core.crypto.HashUtils;
import com.shahabkondri.drivingpractice.core.message.Email;
import com.shahabkondri.drivingpractice.core.message.Message;
import com.shahabkondri.drivingpractice.core.utils.Numbers;
import com.shahabkondri.drivingpractice.entity.Subscribe;
import com.shahabkondri.drivingpractice.entity.enumerations.SubscribeStatus;
import com.shahabkondri.drivingpractice.view.abstracts.AbstractView;
import com.shahabkondri.drivingpractice.view.exception.EmailExistException;
import com.shahabkondri.drivingpractice.view.exception.handle.ViewException;
import com.shahabkondri.drivingpractice.view.exception.handle.ViewExceptionHandler;
import com.shahabkondri.drivingpractice.view.service.MailViewService;
import com.shahabkondri.drivingpractice.view.utils.JsfServlet;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.Date;
import java.util.UUID;

@Named
@RequestScoped
public class SubscribeBean extends AbstractView {
    private static final long serialVersionUID = -8634271786485496740L;

    @EJB
    private GeneralService generalService;

    @EJB
    private SubscribeService subscribeService;

    @Inject
    private MailViewService mailService;

    private Subscribe subscribe = new Subscribe();

    public SubscribeBean() {
    }

    public void subscribeAction() {
        if (subscribe.getEmail() != null && subscribe.getEmail().trim().length() != 0) {
            try {
                Subscribe sub = subscribeService.selectSubscribeByEmail(subscribe.getEmail());
                if (sub != null) {
                    if (sub.getStatus().equals(SubscribeStatus.UN_CONFIRMED)) {
                        mailService.send(getEmail(sub.getEmail(), sub.getToken()));
                        jsfMessage.addInfo("Check your email and confirm subscription", null);
                    } else if (sub.getStatus().equals(SubscribeStatus.UN_SUBSCRIBE)) {
                        sub.setDate(new Date());
                        sub.setStatus(SubscribeStatus.UN_CONFIRMED);
                        sub.setToken(createToken());
                        generalService.update(sub);
                        mailService.send(getEmail(sub.getEmail(), sub.getToken()));
                        jsfMessage.addInfo("Thanks for signing up to the newsletter!", null);
                    } else {
                        throw new ViewException(new EmailExistException(getMessages("email.exists")));
                    }
                } else {
                    subscribe.setDate(new Date());
                    subscribe.setStatus(SubscribeStatus.UN_CONFIRMED);
                    subscribe.setToken(createToken());
                    generalService.save(subscribe);
                    mailService.send(getEmail(subscribe.getEmail(), subscribe.getToken()));
                    jsfMessage.addInfo("Thanks for signing up to the newsletter!", null);
                }
            } catch (Exception e) {
                ViewExceptionHandler.handle(e);
            }
        }
    }

    private String createToken() {
        return HashUtils.generateHash(HashUtils.HashAlgorithm.SHA256,
                Numbers.normalizeUUID(UUID.randomUUID()), HashUtils.HashEncoding.HEX);
    }

    private Email getEmail(String email, String token) {
        Email e = new Email();
        Message message = getMessage(email, token);
        e.setSubject(getMessages("subscribe.confirm.email.subject"));
        e.addMessage(message);
        e.addTo(email);
        return e;
    }

    private Message getMessage(String email, String token) {
        String content = getMessages("subscribe.confirm.email.content", getLink(email, token));
        Message message = new Message();
        message.setContent(content);
        return message;
    }

    private String getLink(String email, String token) {
        String serverPath = JsfServlet.getServerPath();
        return serverPath + "subscribe/confirm/" + email + "/" + token;
    }

    public Subscribe getSubscribe() {
        return subscribe;
    }

    public void setSubscribe(Subscribe subscribe) {
        this.subscribe = subscribe;
    }

}
