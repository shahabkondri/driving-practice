package com.shahabkondri.drivingpractice.view.converter;

import javax.enterprise.context.RequestScoped;
import javax.faces.convert.Converter;
import javax.faces.convert.EnumConverter;
import javax.faces.model.SelectItem;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Collection;

@Named
@RequestScoped
public class ConverterBean implements Serializable {
    private static final long serialVersionUID = -7579580573581209440L;

    public <T> Converter converter(Collection<T> entities) {
        if (entities != null && !entities.isEmpty()) {
            T t = entities.iterator().next();
            if (t instanceof Enum<?>) {
                return new EnumConverter(t.getClass());
            }
            if (t instanceof SelectItem) {
                SelectItem selectItem = (SelectItem) t;
                return new EnumConverter(selectItem.getValue().getClass());
            }
        }
        return new IdConverter<>(entities);
    }
}
