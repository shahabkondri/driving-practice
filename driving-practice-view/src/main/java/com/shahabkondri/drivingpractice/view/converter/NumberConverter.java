package com.shahabkondri.drivingpractice.view.converter;


import com.shahabkondri.drivingpractice.core.utils.Numbers;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;

@FacesConverter
public class NumberConverter extends javax.faces.convert.NumberConverter {

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        return Numbers.getTextLocale(super.getAsString(context, component, value));
    }

}
