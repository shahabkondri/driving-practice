package com.shahabkondri.drivingpractice.view.converter;

import com.shahabkondri.drivingpractice.core.utils.Numbers;
import com.shahabkondri.drivingpractice.core.utils.Validator;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@FacesConverter(forClass = String.class)
public class StringConverter implements Converter {

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value == null) {
            return "";
        }
        return Numbers.getTextLocale(value.toString());
    }

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (value == null || value.trim().length() == 0) {
            return null;
        }
        return Numbers.getTextEnglish(Validator.isEmail(value) ? value.trim().toLowerCase() : value.trim());
    }

}
