package com.shahabkondri.drivingpractice.view.utils;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.Serializable;

@Named
@RequestScoped
public class JsfMessage implements Serializable {
    private static final long serialVersionUID = 2035464864016524342L;

    private long life = 3000;
    private boolean sticky = false;
    private boolean showDetail = true;

    public long getLife() {
        return life;
    }

    public void setLife(long life) {
        this.life = life;
    }

    public boolean isSticky() {
        return sticky;
    }

    public void setSticky(boolean sticky) {
        this.sticky = sticky;
    }

    public boolean isShowDetail() {
        return showDetail;
    }

    public void setShowDetail(boolean showDetail) {
        this.showDetail = showDetail;
    }

    public void setInfo(String msg, String id) {
        message(msg, null, id, FacesMessage.SEVERITY_INFO, false);
    }

    public void setInfo(String msg, String detail, String id) {
        showDetail = detail != null;
        message(msg, detail, id, FacesMessage.SEVERITY_INFO, false);
    }

    public void addInfo(String msg, String detail) {
        showDetail = detail != null;
        sticky = false;
        life = 3000;
        message(msg, detail, null, FacesMessage.SEVERITY_INFO, false);
//        RequestContext.getCurrentInstance().update("growl");
    }

    public void addInfo(String msg, String detail, long life) {
        showDetail = detail != null;
        sticky = false;
        this.life = life;
        message(msg, detail, null, FacesMessage.SEVERITY_INFO, false);
//        RequestContext.getCurrentInstance().update("growl");
    }

    public void addStickyInfo(String msg, String detail) {
        showDetail = detail != null;
        sticky = true;
        life = 3000;
        message(msg, detail, null, FacesMessage.SEVERITY_INFO, false);
//        RequestContext.getCurrentInstance().update("growl");
    }

    public void setWarn(String msg, String id) {
        message(msg, null, id, FacesMessage.SEVERITY_WARN, false);
    }

    public void setWarn(String msg, String detail, String id) {
        showDetail = detail != null;
        message(msg, detail, id, FacesMessage.SEVERITY_WARN, false);
    }

    public void addWarn(String msg, String detail) {
        showDetail = detail != null;
        sticky = false;
        life = 3000;
        message(msg, detail, null, FacesMessage.SEVERITY_WARN, false);
//        RequestContext.getCurrentInstance().update("growl");
    }

    public void addWarn(String msg, String detail, long life) {
        showDetail = detail != null;
        sticky = false;
        this.life = life;
        message(msg, detail, null, FacesMessage.SEVERITY_WARN, false);
//        RequestContext.getCurrentInstance().update("growl");
    }

    public void addStickyWarn(String msg, String detail) {
        showDetail = detail != null;
        sticky = true;
        life = 3000;
        message(msg, detail, null, FacesMessage.SEVERITY_WARN, false);
//        RequestContext.getCurrentInstance().update("growl");
    }

    public void setError(String msg, String id) {
        message(msg, null, id, FacesMessage.SEVERITY_ERROR, true);
    }

    public void setError(String msg, String detail, String id) {
        showDetail = detail != null;
        message(msg, detail, id, FacesMessage.SEVERITY_ERROR, true);
    }

    public void addError(String msg, String detail) {
        showDetail = detail != null;
        sticky = false;
        life = 3000;
        message(msg, detail, null, FacesMessage.SEVERITY_ERROR, true);
//        RequestContext.getCurrentInstance().update("growl");
    }

    public void addError(String msg, String detail, long life) {
        showDetail = detail != null;
        sticky = false;
        this.life = life;
        message(msg, detail, null, FacesMessage.SEVERITY_ERROR, true);
//        RequestContext.getCurrentInstance().update("growl");
    }

    public void addStickyError(String msg, String detail) {
        showDetail = detail != null;
        sticky = true;
        life = 3000;
        message(msg, detail, null, FacesMessage.SEVERITY_ERROR, true);
//        RequestContext.getCurrentInstance().update("growl");
    }

    public void setFatal(String msg, String id) {
        message(msg, null, id, FacesMessage.SEVERITY_FATAL, true);
    }

    public void setFatal(String msg, String detail, String id) {
        showDetail = detail != null;
        message(msg, detail, id, FacesMessage.SEVERITY_FATAL, true);
    }

    public void addFatal(String msg, String detail) {
        showDetail = detail != null;
        sticky = false;
        life = 3000;
        message(msg, detail, null, FacesMessage.SEVERITY_FATAL, true);
//        RequestContext.getCurrentInstance().update("growl");
    }

    public void addFatal(String msg, String detail, long life) {
        showDetail = detail != null;
        sticky = false;
        this.life = life;
        message(msg, detail, null, FacesMessage.SEVERITY_FATAL, true);
//        RequestContext.getCurrentInstance().update("growl");
    }

    public void addStickyFatal(String msg, String detail) {
        showDetail = detail != null;
        sticky = true;
        life = 3000;
        message(msg, detail, null, FacesMessage.SEVERITY_FATAL, true);
//        RequestContext.getCurrentInstance().update("growl");
    }

    private void message(String msg, String detail, String id,
                         Severity severity, boolean isError) {
        FacesContext context = JsfServlet.getFacesContext();
        FacesMessage message = new FacesMessage(msg);
        if (detail != null) {
            message.setDetail(detail);
        }
        context.addMessage(id, new FacesMessage(severity, msg, detail));
    }

}
