package com.shahabkondri.drivingpractice.view.bean.subscription;

import com.shahabkondri.drivingpractice.api.GeneralService;
import com.shahabkondri.drivingpractice.api.subscribe.SubscribeService;
import com.shahabkondri.drivingpractice.entity.Subscribe;
import com.shahabkondri.drivingpractice.entity.enumerations.SubscribeStatus;
import com.shahabkondri.drivingpractice.view.abstracts.AbstractView;
import com.shahabkondri.drivingpractice.view.exception.handle.ViewExceptionHandler;
import com.shahabkondri.drivingpractice.view.utils.Dispatcher;
import com.shahabkondri.drivingpractice.view.utils.JsfServlet;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by shahab on 16/12/15.
 */

@Named
@RequestScoped
public class ConfirmSubscribe extends AbstractView {
    private static final long serialVersionUID = -6721063176612459035L;

    @EJB
    private GeneralService generalService;

    @EJB
    private SubscribeService subscribeService;

    @Inject
    private Dispatcher dispatcher;

    private String email;

    private String token;

    public ConfirmSubscribe() {
        email = JsfServlet.getRequestParameter("email");
        token = JsfServlet.getRequestParameter("token");
    }

    public void load() {
        if (email != null && email.trim().length() != 0 && token != null && token.trim().length() != 0) {
            try {
                Subscribe subscribe = subscribeService.selectSubscribeByEmailAndToken(email, token);
                if (subscribe != null) {
                    if (subscribe.getStatus().equals(SubscribeStatus.UN_CONFIRMED)) {
                        subscribe.setStatus(SubscribeStatus.CONFIRMED);
                        generalService.update(subscribe);
                    } else {
                        dispatcher.redirect("/");
                    }
                } else {
                    dispatcher.redirect("/");
                }
            } catch (Exception e) {
                ViewExceptionHandler.handle(e);
                dispatcher.redirect("/");
            }
        } else {
            dispatcher.redirect("/");
        }
    }

    public String getSubscriptionLink() {
        String serverPath = JsfServlet.getServerPath();
        return serverPath + "unsubscribe/" + email + "/" + token;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
