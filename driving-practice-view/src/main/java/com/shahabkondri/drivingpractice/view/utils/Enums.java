package com.shahabkondri.drivingpractice.view.utils;


import javax.faces.model.SelectItem;
import java.util.LinkedList;
import java.util.List;

public class Enums {

    private static String getEnum(Enum<?> clazz) {
        return Bundles.getEnumValueDefault(clazz.getClass().getSimpleName() + "." + clazz.name());
    }

    public static <T extends Enum<T>> List<SelectItem> values(Class<T> enumType) {
        List<SelectItem> items = new LinkedList<>();
        for (T t : enumType.getEnumConstants()) {
            items.add(new SelectItem(t, getEnum(t)));
        }
        return items;
    }

    public static <T extends Enum<T>> List<String> stringValues(Class<T> enumType) {
        List<String> items = new LinkedList<>();
        for (T t : enumType.getEnumConstants()) {
            items.add(getEnum(t));
        }
        return items;
    }

    public static <T extends Enum<T>> List<Integer> ordinalValues(
            Class<T> enumType) {
        List<Integer> items = new LinkedList<>();
        for (T t : enumType.getEnumConstants()) {
            items.add(t.ordinal());
        }
        return items;
    }

}
