package com.shahabkondri.drivingpractice.view.bean.subscription;

import com.shahabkondri.drivingpractice.api.GeneralService;
import com.shahabkondri.drivingpractice.api.subscribe.SubscribeService;
import com.shahabkondri.drivingpractice.entity.Subscribe;
import com.shahabkondri.drivingpractice.entity.enumerations.SubscribeStatus;
import com.shahabkondri.drivingpractice.view.abstracts.AbstractView;
import com.shahabkondri.drivingpractice.view.exception.EmailExistException;
import com.shahabkondri.drivingpractice.view.exception.handle.ViewException;
import com.shahabkondri.drivingpractice.view.exception.handle.ViewExceptionHandler;
import com.shahabkondri.drivingpractice.view.utils.Dispatcher;
import com.shahabkondri.drivingpractice.view.utils.JsfServlet;

import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by shahab on 16/12/15.
 */

@Named
@ViewScoped
public class UnSubscribeBean extends AbstractView {
    private static final long serialVersionUID = -6721063176612459035L;

    @EJB
    private GeneralService generalService;

    @EJB
    private SubscribeService subscribeService;

    @Inject
    private Dispatcher dispatcher;

    private Subscribe subscribe;

    private String email;
    private String token;

    private boolean renderUnSubscription = false;


    public UnSubscribeBean() {
        email = JsfServlet.getRequestParameter("email");
        token = JsfServlet.getRequestParameter("token");
    }

    public void load() {
        if (email != null && email.trim().length() != 0 && token != null && token.trim().length() != 0) {
            if (subscribe == null) {
                try {
                    Subscribe sub = subscribeService.selectSubscribeByEmailAndToken(email, token);
                    if (sub != null) {
                        if (!sub.getStatus().equals(SubscribeStatus.CONFIRMED)) {
                            dispatcher.redirect("/");
                        } else {
                            subscribe = sub;
                        }
                    } else {
                        dispatcher.redirect("/");
                    }
                } catch (Exception e) {
                    ViewExceptionHandler.handle(e);
                    dispatcher.redirect("/");
                }
            }
        } else {
            dispatcher.redirect("/");
        }
    }

    public void confirmUnSubscription() {
        if (subscribe != null) {
            try {
                if (!subscribe.getEmail().equals(email))
                    throw new ViewException(new EmailExistException(getMessages("enter.your.email")));
                subscribe.setStatus(SubscribeStatus.UN_SUBSCRIBE);
                generalService.update(subscribe);
                renderUnSubscription = true;
            } catch (Exception e) {
                ViewExceptionHandler.handle(e);
            }
        }
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Subscribe getSubscribe() {
        return subscribe;
    }

    public void setSubscribe(Subscribe subscribe) {
        this.subscribe = subscribe;
    }

    public boolean isRenderUnSubscription() {
        return renderUnSubscription;
    }

    public void setRenderUnSubscription(boolean renderUnSubscription) {
        this.renderUnSubscription = renderUnSubscription;
    }
}
