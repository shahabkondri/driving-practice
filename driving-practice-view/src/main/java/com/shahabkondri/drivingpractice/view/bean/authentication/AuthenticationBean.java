package com.shahabkondri.drivingpractice.view.bean.authentication;

import com.shahabkondri.drivingpractice.api.GeneralService;
import com.shahabkondri.drivingpractice.api.identity.AccountRelationshipService;
import com.shahabkondri.drivingpractice.entity.identity.AccountRelationship;
import com.shahabkondri.drivingpractice.entity.identity.Password;
import com.shahabkondri.drivingpractice.view.abstracts.AbstractAuthentication;
import com.shahabkondri.drivingpractice.view.exception.AuthenticatorFailedException;
import com.shahabkondri.drivingpractice.view.exception.NotActivationException;
import com.shahabkondri.drivingpractice.view.exception.handle.ViewException;
import com.shahabkondri.drivingpractice.view.mail.ActivationMail;
import com.shahabkondri.drivingpractice.view.service.AuthenticationService;
import com.shahabkondri.drivingpractice.view.utils.Bundles;
import com.shahabkondri.drivingpractice.view.utils.Sessions;

import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@ViewScoped
public class AuthenticationBean extends AbstractAuthentication {
    private static final long serialVersionUID = 6299305395799847331L;

    @EJB
    private AccountRelationshipService accountRelationshipService;

    @EJB
    private GeneralService globalService;

    @Inject
    private AuthenticationService authenticationService;

    @Inject
    private ActivationMail activationMail;

    private Password password = new Password();

    private AccountRelationship accountRelationship;

    public AuthenticationBean() {
    }

    @Override
    public void authentication() throws Exception {
        if (Sessions.get(AccountRelationship.class.getName()) == null) {
            if (password.getEmail() != null && password.getPassword() != null) {
                accountRelationship = accountRelationshipService.selectAccountRelationshipByEmailAndPassword(password.getEmail(), password.getPassword());

                if (accountRelationship == null)
                    throw new ViewException(new AuthenticatorFailedException(),
                            Bundles.getExceptionsValueDefault("e.authentication.email.or.password.is.incorrect"));

                if (!accountRelationship.getActivation().getActivationStatus().isActive()) {
                    throw new ViewException(new NotActivationException(),
                            Bundles.getExceptionsValueDefault("e.authentication.account.not.active"));
                }

                if (isLoginStay()) {
                    authenticationService.createStayLoginCookie(password.getEmail());
                }
                authenticationService.login(accountRelationship);
            }
        }
    }


    public Password getPassword() {
        return password;
    }

    public void setPassword(Password password) {
        this.password = password;
    }

    public ActivationMail getActivationMail() {
        return activationMail;
    }

    public void setActivationMail(ActivationMail activationMail) {
        this.activationMail = activationMail;
    }

    public AccountRelationship getAccountRelationship() {
        return accountRelationship;
    }

    public void setAccountRelationship(AccountRelationship accountRelationship) {
        this.accountRelationship = accountRelationship;
    }

}
