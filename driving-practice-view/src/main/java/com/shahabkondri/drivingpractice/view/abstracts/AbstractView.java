package com.shahabkondri.drivingpractice.view.abstracts;

import com.shahabkondri.drivingpractice.view.utils.Bundles;
import com.shahabkondri.drivingpractice.view.utils.JsfMessage;

import javax.inject.Inject;
import java.io.Serializable;

public class AbstractView implements Serializable {
    private static final long serialVersionUID = 8053894737512825793L;

    @Inject
    protected JsfMessage jsfMessage;

    public String getBundles(String key) {
        return Bundles.getBundleValueDefault(key);
    }

    public String getBundles(String key, Object... params) {
        return Bundles.getBundleValueDefault(key, params);
    }

    public String getMessages(String key) {
        return Bundles.getMessageValueDefault(key);
    }

    public String getMessages(String key, Object... params) {
        return Bundles.getMessageValueDefault(key, params);
    }


}
