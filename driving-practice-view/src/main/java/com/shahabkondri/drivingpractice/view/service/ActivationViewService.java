package com.shahabkondri.drivingpractice.view.service;

import com.shahabkondri.drivingpractice.core.crypto.HashUtils;
import com.shahabkondri.drivingpractice.core.time.DateTime;
import com.shahabkondri.drivingpractice.core.utils.Numbers;
import com.shahabkondri.drivingpractice.entity.identity.Activation;
import com.shahabkondri.drivingpractice.entity.identity.enumeration.ActivationType;

import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

public class ActivationViewService implements Serializable {
    private static final long serialVersionUID = -7566071742444348435L;

    public static Activation createActivation(ActivationType activationType) throws NoSuchAlgorithmException {
        Activation activation = new Activation();
        activation.setRegisterTime(DateTime.now());
        activation.setActivationStatus(activationType);
        String code = HashUtils.generateHash(HashUtils.HashAlgorithm.SHA256,
                Numbers.normalizeUUID(UUID.randomUUID()), HashUtils.HashEncoding.HEX);
        activation.setToken(code);
        return activation;
    }
}