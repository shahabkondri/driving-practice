package com.shahabkondri.drivingpractice.view.utils;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import java.util.Map;
import java.util.Map.Entry;

public final class Sessions {

	private Sessions() {
	}

	public static boolean create() {
		HttpSession session = getSession(true);
		return session != null;
	}

	public static boolean create(String name, Object value) {
		HttpSession session = getSession(true);
		if (session != null) {
			session.setAttribute(name, value);
		}
		return session != null;
	}

	public static void clear() {
		HttpSession session = getSession(false);
		if (session != null) {
			session.invalidate();
		}
	}

	public static boolean save(String name, Object value) {
		HttpSession session = getSession(false);
		if (session == null) {
			session = getSession(true);
		}
		if (session != null) {
			session.setAttribute(name, value);
		}
		return session != null;
	}

	public static void save(String[] names, Object[] values) {
		for (int i = 0; i < names.length; i++) {
			save(names[i], values[i]);
		}
	}

	public static void save(Map<String, Object> params) {
		for (Entry<String, Object> e : params.entrySet()) {
			save(e.getKey(), e.getValue());
		}
	}

	public static boolean delete(String name) {
		return save(name, null);
	}

	public static Object get(String name) {
		HttpSession session = getSession(false);
		Object object = null;
		if (session != null) {
			object = session.getAttribute(name);
		}
		return object;
	}

	private static HttpSession getSession(boolean create) {
		Object session = null;
		FacesContext facesContext = FacesContext.getCurrentInstance();
		if (facesContext != null) {
			ExternalContext externalContext = facesContext.getExternalContext();
			if (externalContext != null) {
				session = externalContext.getSession(create);
			}
		}
		return session != null ? (HttpSession) session : null;
	}

}
