package com.shahabkondri.drivingpractice.view.bean.contact;

import com.shahabkondri.drivingpractice.api.GeneralService;
import com.shahabkondri.drivingpractice.core.message.Email;
import com.shahabkondri.drivingpractice.core.message.Message;
import com.shahabkondri.drivingpractice.entity.Contact;
import com.shahabkondri.drivingpractice.entity.enumerations.MessageStatus;
import com.shahabkondri.drivingpractice.view.abstracts.AbstractView;
import com.shahabkondri.drivingpractice.view.exception.handle.ViewExceptionHandler;
import com.shahabkondri.drivingpractice.view.service.MailViewService;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by shahab on 15/12/15.
 */
@Named
@RequestScoped
public class ContactBean extends AbstractView {
    private static final long serialVersionUID = -6719084554761765831L;

    @EJB
    private GeneralService generalService;

    @Inject
    private MailViewService mailService;

    private Contact contact = new Contact();

    public ContactBean() {
    }

    public void send() {
        try {
            if (contact.getName().trim().length() != 0 && contact.getEmail().trim().length() != 0 &&
                    contact.getMessage().trim().length() != 0) {

                contact.setMessageStatus(MessageStatus.UN_READ);
                generalService.save(contact);
                mailService.send(getEmail(contact));
                jsfMessage.addInfo(getMessages("contact.send.message.successfully"), null);
                contact = new Contact();
            }
        } catch (Exception e) {
            ViewExceptionHandler.handle(e);
        }
    }

    private Email getEmail(Contact contact) {
        Email email = new Email();
        Message message = getMessage(contact);
        email.setSubject(getMessages("contact.mail.subject"));
        email.addMessage(message);
        email.addTo("shahab.kondri@gmail.com");
        return email;
    }

    private Message getMessage(Contact contact) {
        String content = getMessages("contact.mail.content", contact.getName(), getContact().getMessage(), contact.getEmail());
        Message message = new Message();
        message.setContent(content);
        return message;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }
}
