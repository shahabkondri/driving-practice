package com.shahabkondri.drivingpractice.view.converter;

import com.shahabkondri.drivingpractice.core.time.Time;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import java.io.Serializable;
import java.time.format.DateTimeFormatter;

@FacesConverter(forClass = Time.class)
public class TimeConverter implements Converter, Serializable {

    private static final long serialVersionUID = 2032209194285626527L;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (value == null || value.trim().length() == 0) {
            return null;
        }
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm");
        return Time.from(dateTimeFormatter.parse(value));
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value == null) {
            return "";
        }
        return value.toString();
    }

}
