package com.shahabkondri.drivingpractice.view.service;

import com.shahabkondri.drivingpractice.api.GeneralService;
import com.shahabkondri.drivingpractice.entity.UserAgent;
import com.shahabkondri.drivingpractice.entity.identity.AccountRelationship;
import com.shahabkondri.drivingpractice.entity.identity.LoginSession;
import com.shahabkondri.drivingpractice.view.bean.Languages;
import com.shahabkondri.drivingpractice.view.utils.*;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

@Named
@RequestScoped
public class AuthenticationService implements Serializable {
    private static final long serialVersionUID = 1961740664650643455L;

    @EJB
    private GeneralService generalService;

    @Inject
    private Languages languages;

    @Inject
    private Dispatcher dispatcher;

    private boolean cookieSet;

    public void login(AccountRelationship accountRelationship) throws Exception {
        languages.setLocale(accountRelationship.getLocale());
        Sessions.save(AccountRelationship.class.getName(), getLoginSession(accountRelationship, getUserAgent()));
        String previousUrl = (String) Sessions.get("previous-url");
        dispatcher.redirect(previousUrl);
    }

    public void createStayLogin(AccountRelationship accountRelationship) {

    }

    public void createStayLoginCookie(String email) throws Exception {
        if (isCookieSet()) {
            Cookies.deleteWithPathEncrypted(AccountRelationship.class.getName(), JsfServlet.getRequest().getContextPath());
        }
        Cookies.saveWithPathEncrypt(AccountRelationship.class.getName(), email, Cookies.MONTH,
                JsfServlet.getRequest().getContextPath());
    }

    private String getCookieStayLogin() {
        return Cookies.getEncrypted(AccountRelationship.class.getName());
    }

    private UserAgent getUserAgent() throws Exception {
        UserAgent userAgent = new UserAgent();
        userAgent.setBrowser(UserAgentUtils.getBrowser());
        userAgent.setOperatingSystem(UserAgentUtils.getOperationSystem());
        userAgent.setIp(JsfServlet.getClientIp());
        return generalService.save(userAgent);
    }

    private LoginSession getLoginSession(AccountRelationship accountRelationship, UserAgent userAgent) throws Exception {
        return new LoginSession(accountRelationship, userAgent);
    }

    private boolean isCookieSet() {
        try {
            if (getCookieStayLogin() != null) {
                cookieSet = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cookieSet;
    }

    public void setCookieSet(boolean cookieSet) {
        this.cookieSet = cookieSet;
    }
}
