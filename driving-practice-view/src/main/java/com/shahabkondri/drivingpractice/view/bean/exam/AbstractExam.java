package com.shahabkondri.drivingpractice.view.bean.exam;

import com.shahabkondri.drivingpractice.entity.Option;
import com.shahabkondri.drivingpractice.entity.Question;
import com.shahabkondri.drivingpractice.entity.enumerations.TestType;
import com.shahabkondri.drivingpractice.view.abstracts.AbstractView;
import com.shahabkondri.drivingpractice.view.exception.handle.ViewExceptionHandler;
import com.shahabkondri.drivingpractice.view.utils.Sessions;

import java.util.LinkedList;

/**
 * Created by shahab on 20/12/15.
 */
public abstract class AbstractExam extends AbstractView {
    private static final long serialVersionUID = 7395247622680914387L;

    private LinkedList<Question> questions = new LinkedList<>();
    private Question question;
    private Option option;

    private boolean nextQuestionRender = false;
    private boolean previousQuestionRender = false;
    private boolean optionDisabled = false;

    private TestType testType;

    protected void setTestType(TestType testType) {
        this.testType = testType;
    }

    protected boolean hasNextQuestion(int currentQuestionNumber, int numberOfQuestions) {
        return currentQuestionNumber < numberOfQuestions;
    }

    protected boolean isLastQuestion(LinkedList<Question> questions, Question question) {
        return questions.getLast().equals(question);
    }

    protected boolean isFirstQuestion(LinkedList<Question> questions, Question question) {
        return questions.getFirst().equals(question);
    }

    protected void saveSession(byte[] image) {
        if (image != null) {
            Sessions.save("IMAGE_SESSION", question.getImage());
        }
    }

    public void restartHandler() {
        try {
            this.clear();
            question = null;
            questions = new LinkedList<>();
            Sessions.delete("IMAGE_SESSION");
            this.restart();
        } catch (Exception e) {
            ViewExceptionHandler.handle(e);
        }
    }

    protected void clear() {
        option = null;
        nextQuestionRender = false;
        previousQuestionRender = false;
        optionDisabled = false;
    }

    protected abstract void correctAnswer(Question question);

    protected abstract void wrongAnswer(Question question);

    public abstract void next();

    public abstract void previous();

    public abstract void pickQuestion(Question question);

    protected abstract void restart() throws Exception;


    public LinkedList<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(LinkedList<Question> questions) {
        this.questions = questions;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public Option getOption() {
        return option;
    }

    public void setOption(Option option) {
        this.option = option;
    }
    public boolean isNextQuestionRender() {
        return nextQuestionRender;
    }

    public void setNextQuestionRender(boolean nextQuestionRender) {
        this.nextQuestionRender = nextQuestionRender;
    }

    public boolean isPreviousQuestionRender() {
        return previousQuestionRender;
    }

    public void setPreviousQuestionRender(boolean previousQuestionRender) {
        this.previousQuestionRender = previousQuestionRender;
    }

    public boolean isOptionDisabled() {
        return optionDisabled;
    }

    public void setOptionDisabled(boolean optionDisabled) {
        this.optionDisabled = optionDisabled;
    }
}
