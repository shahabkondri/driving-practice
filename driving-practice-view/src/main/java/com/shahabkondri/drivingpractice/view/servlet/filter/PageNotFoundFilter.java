package com.shahabkondri.drivingpractice.view.servlet.filter;

import javax.servlet.FilterChain;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by shahab on 27/12/15.
 */

@WebFilter(urlPatterns = {"/tags/*", "/template/*", "/include/*"})
public class PageNotFoundFilter extends GeneralFilter {

    @Override
    public void filter(ServletRequest request, ServletResponse response, FilterChain chain) throws Exception {
        sendError(HttpServletResponse.SC_NOT_FOUND);
    }
}
