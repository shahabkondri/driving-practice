package com.shahabkondri.drivingpractice.view.bean;

import com.shahabkondri.drivingpractice.api.GeneralService;
import com.shahabkondri.drivingpractice.api.question.QuestionService;
import com.shahabkondri.drivingpractice.entity.Question;
import com.shahabkondri.drivingpractice.entity.enumerations.ExamType;
import com.shahabkondri.drivingpractice.entity.enumerations.Location;
import com.shahabkondri.drivingpractice.entity.enumerations.QuestionType;
import com.shahabkondri.drivingpractice.view.abstracts.AbstractView;
import com.shahabkondri.drivingpractice.view.exception.handle.ViewExceptionHandler;
import com.shahabkondri.drivingpractice.view.service.OntarioSignsService;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by shahabkondri on 29/11/15.
 */

@Named
@RequestScoped
public class InsertData extends AbstractView {
    private static final long serialVersionUID = -3270826957950190289L;

    @EJB
    private GeneralService generalService;

    @EJB
    private QuestionService questionService;

    public InsertData() throws IOException {

    }

    public void insert() {
        File g1sign = new File("/home/shahabkondri/Documents/work/my-project/driving-practice/driving-practice-view/src/main/resources/driving-questions/ontario/g1/sings");
        File g1rule = new File("/home/shahabkondri/Documents/work/my-project/driving-practice/driving-practice-view/src/main/resources/driving-questions/ontario/g1/rules");
        File az = new File("/home/shahabkondri/Documents/work/my-project/driving-practice/driving-practice-view/src/main/resources/driving-questions/ontario/az");


        try {
            List<Question> sQuestions = new OntarioSignsService().getQuestions(g1sign, ExamType.DRIVER, QuestionType.SIGN, Location.ONTARIO);
            List<Question> rQuestions = new OntarioSignsService().getQuestions(g1rule, ExamType.DRIVER, QuestionType.RULE, Location.ONTARIO);
            List<Question> azQuestions = new OntarioSignsService().getQuestions(az, ExamType.TRUCK, QuestionType.AZ_WRITTEN, Location.ONTARIO);

            if (sQuestions != null && !sQuestions.isEmpty()) {
                generalService.save(sQuestions);
            }

            if (rQuestions != null && !rQuestions.isEmpty()) {
                generalService.save(rQuestions);
            }
            if (azQuestions != null && !azQuestions.isEmpty()) {
                generalService.save(azQuestions);
            }
        } catch (Exception e) {
            ViewExceptionHandler.handle(e);
        }
    }
}