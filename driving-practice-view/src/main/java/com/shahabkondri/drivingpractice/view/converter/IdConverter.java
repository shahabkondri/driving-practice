package com.shahabkondri.drivingpractice.view.converter;

import com.shahabkondri.drivingpractice.entity.abstracts.BaseIdentity;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

public class IdConverter<T> implements Converter, Serializable {

    private static final long serialVersionUID = 6917088233427250250L;

    private Collection<T> entities;

    public IdConverter(Collection<T> entities) {
        setEntities(entities);
    }

    @SafeVarargs
    public IdConverter(Collection<T>... entities) {
        setEntities(entities);
    }

    public Collection<T> getEntities() {
        return entities;
    }

    public void setEntities(Collection<T> entities) {
        if (entities == null) {
            this.entities = new ArrayList<>();
        } else {
            this.entities = new ArrayList<>(entities);
        }
    }

    @SuppressWarnings("unchecked")
    public void setEntities(Collection<T>... entities) {
        this.entities = new ArrayList<>();
        if (entities != null && entities.length > 0) {
            Arrays.stream(entities).filter(entity -> entity != null).forEachOrdered((Collection<T> entity) -> this.entities.addAll(entity));
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value == null) {
            return "";
        }
        BaseIdentity<?> t = (BaseIdentity<?>) value;
        return String.valueOf(t.getId());
    }

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (value == null || value.trim().length() == 0) {
            return null;
        }
        return entities.stream().filter(e -> String.valueOf(((BaseIdentity<?>) e).getId()).equals(String.valueOf(value))).findFirst().orElse(null);
    }

}
