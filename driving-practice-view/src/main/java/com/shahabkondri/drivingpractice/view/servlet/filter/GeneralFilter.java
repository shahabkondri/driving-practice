package com.shahabkondri.drivingpractice.view.servlet.filter;


import com.shahabkondri.drivingpractice.core.localization.Dispatcher;
import com.shahabkondri.drivingpractice.view.utils.Sessions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by shahab on 27/12/15.
 */
public abstract class GeneralFilter implements Filter {
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    protected HttpServletRequest httpServletRequest;
    protected HttpServletResponse httpServletResponse;
    protected ServletContext servletContext;

    public abstract void filter(ServletRequest request,
                                ServletResponse response, FilterChain chain) throws Exception;

    @Override
    public void init(FilterConfig config) throws ServletException {
        servletContext = config.getServletContext();
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain) throws IOException, ServletException {
        try {
            this.httpServletRequest = (HttpServletRequest) request;
            this.httpServletResponse = (HttpServletResponse) response;
            filter(request, response, chain);
        } catch (Exception e) {
            logger.error("Error in filter.", e);
        }
    }

    @Override
    public void destroy() {
    }

    protected void sendError(int errorCode) throws IOException {
        httpServletResponse.sendError(errorCode);
    }

    protected void saveCurrentUrl() {
        Sessions.save("url-dispatch", httpServletRequest.getServletPath());
    }

    protected String getCurrentUrl() {
        return httpServletRequest.getServletPath();
    }

    protected void redirect(String url) throws IOException {
        Dispatcher.redirect(url, httpServletRequest, httpServletResponse);
    }

}
