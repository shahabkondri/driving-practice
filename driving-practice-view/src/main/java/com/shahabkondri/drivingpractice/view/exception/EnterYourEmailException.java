package com.shahabkondri.drivingpractice.view.exception;

public class EnterYourEmailException extends Exception {
    private static final long serialVersionUID = -1428389996840086233L;

    public EnterYourEmailException() {
    }

    public EnterYourEmailException(String message) {
        super(message);
    }
}
