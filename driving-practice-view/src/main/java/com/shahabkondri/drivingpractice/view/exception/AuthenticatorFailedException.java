package com.shahabkondri.drivingpractice.view.exception;

/**
 * Created by shahab on 5/29/15.
 */
public class AuthenticatorFailedException extends Exception {
    private static final long serialVersionUID = 4351328193215499908L;

    public AuthenticatorFailedException() {
    }

    public AuthenticatorFailedException(String message) {
        super(message);
    }
}
