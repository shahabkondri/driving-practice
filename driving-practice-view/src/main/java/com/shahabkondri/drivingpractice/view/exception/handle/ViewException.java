package com.shahabkondri.drivingpractice.view.exception.handle;

public class ViewException extends Exception {
    private static final long serialVersionUID = 2090010257893534930L;

    private String message;

    private Throwable throwable;

    public ViewException(Throwable throwable) {
        this.throwable = throwable;
    }

    public ViewException(Throwable throwable, String message) {
        super(throwable);
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Throwable getThrowable() {
        return throwable;
    }

    public void setThrowable(Throwable throwable) {
        this.throwable = throwable;
    }
}