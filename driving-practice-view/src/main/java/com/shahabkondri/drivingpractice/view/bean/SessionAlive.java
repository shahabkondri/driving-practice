package com.shahabkondri.drivingpractice.view.bean;

import com.shahabkondri.drivingpractice.view.abstracts.AbstractView;
import com.shahabkondri.drivingpractice.view.utils.JsfServlet;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 * Created by shahab on 30/12/15.
 */

@Named
@RequestScoped
public class SessionAlive extends AbstractView {
    private static final long serialVersionUID = -2442381503672528676L;

    public SessionAlive() {
    }

    public void keepUserSessionAlive() {
        JsfServlet.getRequest().getSession();
    }
}
