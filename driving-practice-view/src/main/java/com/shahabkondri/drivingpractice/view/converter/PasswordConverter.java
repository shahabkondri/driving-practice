package com.shahabkondri.drivingpractice.view.converter;

import com.shahabkondri.drivingpractice.core.crypto.HashUtils;
import com.shahabkondri.drivingpractice.core.utils.Numbers;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@FacesConverter("passwordConverter")
public class PasswordConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String value) {
        if (value == null || (value.trim().length() == 0)) {
            return null;
        }
        return HashUtils.generateHash(HashUtils.HashAlgorithm.SHA512, Numbers.getTextEnglish(value),
                HashUtils.HashEncoding.HEX);
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o) {
        return o == null ? null : o.toString();
    }
}
