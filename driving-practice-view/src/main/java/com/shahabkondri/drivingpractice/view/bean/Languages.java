package com.shahabkondri.drivingpractice.view.bean;


import com.shahabkondri.drivingpractice.core.localization.Locales;
import com.shahabkondri.drivingpractice.entity.enumerations.Language;
import com.shahabkondri.drivingpractice.entity.enumerations.LocaleType;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Locale;

@Named
@RequestScoped
public class Languages implements Serializable {
    private static final long serialVersionUID = -2872091449269889458L;

    private Locale locale;

    public Locale getLocale() {
        locale = Locales.getLocale();
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    public void setLocale(LocaleType locale) {
        switch (locale) {
            case ENGLISH:
                english();
                break;
            case PERSIAN:
                persian();
                break;
            case TURKISH:
                turkish();
                break;
            case GERMAN:
                german();
                break;
        }
    }

    public void setLocale(Language language) {
        switch (language) {
            case ENGLISH:
                english();
                break;
            case PERSIAN:
                persian();
                break;
            case TURKISH:
                turkish();
                break;
            case GERMAN:
                german();
                break;
        }
    }


    public void persian() {
        save(Locales.PERSIAN);
    }

    public void arabic() {
        save(Locales.ARABIC);
    }

    public void kurdish() {
        save(Locales.KURDISH);
    }

    public void turkish() {
        save(Locales.TURKISH);
    }

    public void english() {
        save(Locales.ENGLISH);
    }

    public void french() {
        save(Locales.FRENCH);
    }

    public void italian() {
        save(Locales.ITALIAN);
    }

    public void german() {
        save(Locales.GERMAN);
    }

    private void save(Locale locale) {
        setLocale(locale);
        Locales.setLocale(locale);
    }

}
