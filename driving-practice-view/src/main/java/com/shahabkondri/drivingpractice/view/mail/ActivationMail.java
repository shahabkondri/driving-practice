package com.shahabkondri.drivingpractice.view.mail;

import com.shahabkondri.drivingpractice.core.message.Email;
import com.shahabkondri.drivingpractice.core.message.Message;
import com.shahabkondri.drivingpractice.entity.identity.Account;
import com.shahabkondri.drivingpractice.entity.identity.Activation;
import com.shahabkondri.drivingpractice.view.abstracts.AbstractView;
import com.shahabkondri.drivingpractice.view.service.MailViewService;
import com.shahabkondri.drivingpractice.view.utils.Bundles;
import com.shahabkondri.drivingpractice.view.utils.JsfServlet;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@ViewScoped
public class ActivationMail extends AbstractView {
    private static final long serialVersionUID = -6331161530783660427L;

    @Inject
    private MailViewService mailViewService;

    private int resendCount = 0;

    public ActivationMail() {
    }

    public void sendActivationMail(Activation activation, Account account) {
        mailViewService.send(createRegisterEmail(activation, account));
    }

    public void resendActivationMail(Activation activation, Account account) {
        if (activation != null && account != null) {
            if (resendCount < 3) {
                sendActivationMail(activation, account);
                jsfMessage.addInfo(Bundles.getMessageValueDefault("register.activation.message"), null);
                resendCount = resendCount + 1;
            } else {
                jsfMessage.addInfo(Bundles.getMessageValueDefault("register.activation.resend.error.message"), null);
            }
        }
    }

    private Email createRegisterEmail(Activation activation, Account account) {
        Email email = new Email();
        Message message = getMessage(activation, account);
        email.setSubject(Bundles.getMessageValueDefault("activation.mail.subject.message"));
        email.addMessage(message);
        email.addTo(account.getEmail());
        return email;
    }

    private Message getMessage(Activation activation, Account account) {
        String content = Bundles.getMessageValueDefault("activation.mail.content.message",
                account.getFirstName(), account.getLastName(), getLink(activation, account));
        Message message = new Message();
        message.setContent(content);
        return message;
    }

    private String getLink(Activation activation, Account account) {
        return JsfServlet.getServerPath() + "activation" + "?"
                + "token" + "=" + activation.getToken() + "&"
                + "email" + "=" + getEncryptedEmail(account.getEmail());
    }

    private String getEncryptedEmail(String email) {
        return email;
    }

    public int getResendCount() {
        return resendCount;
    }

    public void setResendCount(int resendCount) {
        this.resendCount = resendCount;
    }
}
