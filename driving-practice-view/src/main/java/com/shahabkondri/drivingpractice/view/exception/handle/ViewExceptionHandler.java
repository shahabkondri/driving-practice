package com.shahabkondri.drivingpractice.view.exception.handle;


import com.shahabkondri.drivingpractice.view.utils.JsfServlet;

import javax.faces.application.FacesMessage;
import java.io.Serializable;

public class ViewExceptionHandler implements Serializable {
    private static final long serialVersionUID = 390384193470734619L;

    public static void handle(Exception e) {
        if (e instanceof ViewException) {
            ViewException exception = (ViewException) e;
            exception.printStackTrace();

            FacesMessage facesMessage = null;
            if (exception.getMessage() != null && exception.getMessage().trim().length() != 0) {
                facesMessage = new FacesMessage();
                facesMessage.setSeverity(FacesMessage.SEVERITY_ERROR);
                facesMessage.setSummary(exception.getMessage());

            } else if (exception.getThrowable().getMessage() != null) {
                facesMessage = new FacesMessage();
                facesMessage.setSeverity(FacesMessage.SEVERITY_ERROR);
                facesMessage.setSummary(exception.getThrowable().getMessage());
            }

            if (facesMessage != null) {
                JsfServlet.getFacesContext().addMessage(null, facesMessage);
            }

        } else {
            e.printStackTrace();
        }
    }
}
