package com.shahabkondri.drivingpractice.view.utils;

import eu.bitwalker.useragentutils.Browser;
import eu.bitwalker.useragentutils.OperatingSystem;
import eu.bitwalker.useragentutils.UserAgent;
import eu.bitwalker.useragentutils.Version;

import java.io.Serializable;

/**
 * Created by shahabfereydooni on 8/9/15.
 */
public final class UserAgentUtils implements Serializable {
    private static final long serialVersionUID = -6927639246597528645L;

    private UserAgentUtils() {
    }

    public static UserAgent getUserAgent() {
        return UserAgent.parseUserAgentString(JsfServlet.getRequest().getHeader("User-Agent"));
    }

    public static Browser getBrowser() {
        return getUserAgent().getBrowser();
    }

    public static Version getBrowserVersion() {
        return getUserAgent().getBrowserVersion();
    }

    public static OperatingSystem getOperationSystem() {
        return getUserAgent().getOperatingSystem();
    }

}
