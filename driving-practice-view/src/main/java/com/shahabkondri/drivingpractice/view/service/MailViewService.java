package com.shahabkondri.drivingpractice.view.service;


import com.shahabkondri.drivingpractice.core.mail.Mail;
import com.shahabkondri.drivingpractice.core.message.Email;

import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.mail.MessagingException;
import javax.mail.Session;
import java.io.Serializable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by shahab on 6/2/15.
 */

@RequestScoped
public class MailViewService implements Serializable {
    private static final long serialVersionUID = 3134715811151887017L;

    @Resource(mappedName = "java:/driving-written-test-mail")
    private Session session;

    public void send(Email email) {
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        executorService.execute(() -> {
            try {
                Mail mail = new Mail(session);
                email.setFrom("drivingwrittentest@gmail.com");
                mail.send(email);
            } catch (MessagingException e) {
                e.printStackTrace();
            }
        });
        executorService.shutdown();
    }
}
