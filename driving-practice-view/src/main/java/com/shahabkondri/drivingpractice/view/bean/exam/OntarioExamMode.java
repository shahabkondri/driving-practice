package com.shahabkondri.drivingpractice.view.bean.exam;

import com.shahabkondri.drivingpractice.api.question.QuestionService;
import com.shahabkondri.drivingpractice.core.utils.Numbers;
import com.shahabkondri.drivingpractice.entity.Question;
import com.shahabkondri.drivingpractice.entity.enumerations.Answer;
import com.shahabkondri.drivingpractice.entity.enumerations.ExamType;
import com.shahabkondri.drivingpractice.entity.enumerations.QuestionType;
import com.shahabkondri.drivingpractice.entity.enumerations.TestType;
import com.shahabkondri.drivingpractice.view.exception.handle.ViewExceptionHandler;
import com.shahabkondri.drivingpractice.view.model.Result;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.util.LinkedList;

/**
 * Created by shahabkondri on 26/11/15.
 */

@Named
@SessionScoped
public class OntarioExamMode extends AbstractExam {
    private static final long serialVersionUID = 8531878923434630520L;

    @EJB
    private QuestionService questionService;

    private LinkedList<Integer> signQuestionNumbers;
    private LinkedList<Integer> ruleQuestionNumbers;

    private Result result = new Result();
    private int counter = 0;

    private int questionCount = 20;
    private int signCounter = 0;
    private int ruleCounter = 0;


    public OntarioExamMode() {
    }

    @PostConstruct
    public void init() {
        try {
            setTestType(TestType.EXAM_MODE);
            int signQuestionsCount = (int) questionService.selectCount(QuestionType.SIGN, ExamType.DRIVER);
            int ruleQuestionsCount = (int) questionService.selectCount(QuestionType.SIGN, ExamType.DRIVER);
            this.initialize(signQuestionsCount, ruleQuestionsCount);
        } catch (Exception e) {
            ViewExceptionHandler.handle(e);
        }
    }

    private void initialize(int signQuestionsCount, int ruleQuestionsCount) throws Exception {
        if (getQuestion() == null) {
            signQuestionNumbers = Numbers.pickNumberFromFirst(questionCount, Numbers.generateRandomNumbers(signQuestionsCount));
            ruleQuestionNumbers = Numbers.pickNumberFromFirst(questionCount, Numbers.generateRandomNumbers(ruleQuestionsCount));
            setQuestion(questionService.selectQuestionByRowNumberAndQuestionTypeAndQuestionExam(signQuestionNumbers.get(signCounter),
                    QuestionType.SIGN, ExamType.DRIVER));
            getQuestions().add(getQuestion());
            saveSession(getQuestion().getImage());
        }
    }

    public String getImageToken() {
        return "signs/" + getQuestion().getToken() + ".png";
    }

    public void optionAction() {
        try {
            if (getOption() != null) {
                Question question = getQuestions().get(counter);
                question.setSelectedOption(getOption());

                if (!getOption().isCorrectAnswer()) {
                    wrongAnswer(question);
                    if (hasNextQuestion(counter, signQuestionNumbers.size() + ruleQuestionNumbers.size() - 1)) {
                        setNextQuestionRender(true);
                    } else {
                        result.setRender(true);
//                        RequestContext.getCurrentInstance().update("dialog");
//                        RequestContext.getCurrentInstance().execute("PF('result').show();");
                    }
                }

                if (getOption().isCorrectAnswer()) {
                    correctAnswer(question);
                    if (hasNextQuestion(counter, signQuestionNumbers.size() + ruleQuestionNumbers.size())) {
                        if (counter < questionCount) {
                            super.setQuestion(questionService.selectQuestionByRowNumberAndQuestionTypeAndQuestionExam(signQuestionNumbers.get(signCounter),
                                    QuestionType.SIGN, ExamType.DRIVER));
                        } else {
                            super.setQuestion(questionService.selectQuestionByRowNumberAndQuestionTypeAndQuestionExam(ruleQuestionNumbers.get(ruleCounter),
                                    QuestionType.RULE, ExamType.DRIVER));
                        }
                        saveSession(super.getQuestion().getImage());
                        getQuestions().add(super.getQuestion());
                        clear();
                    } else {
                        setOptionDisabled(true);
                        result.setRender(true);
//                        RequestContext.getCurrentInstance().update("dialog");
//                        RequestContext.getCurrentInstance().execute("PF('result').show();");
                    }
                }
                setPreviousQuestionRender(!isFirstQuestion(getQuestions(), super.getQuestion()));
            }
        } catch (Exception e) {
            ViewExceptionHandler.handle(e);
        }
    }

    @Override
    protected void correctAnswer(Question question) {
        question.setAnswer(Answer.CORRECT);
        counter++;
        if (question.getQuestionType().equals(QuestionType.SIGN)) {
            signCounter++;
            result.setSignCorrectAnswer(result.getSignCorrectAnswer() + 1);
        } else if (question.getQuestionType().equals(QuestionType.RULE)) {
            result.setRuleCorrectAnswer(result.getRuleCorrectAnswer() + 1);
            if (ruleCounter < 19) {
                ruleCounter++;
            }
        }
        result.setCorrectAnswer(result.getCorrectAnswer() + 1);
    }

    @Override
    protected void wrongAnswer(Question question) {
        question.setAnswer(Answer.WRONG);
        if (question.getQuestionType().equals(QuestionType.SIGN)) {
            result.setSignWrongAnswer(result.getSignWrongAnswer() + 1);
        } else if (question.getQuestionType().equals(QuestionType.RULE)) {
            result.setRuleWrongAnswer(result.getRuleWrongAnswer() + 1);
        }
        result.setWrongAnswer(result.getWrongAnswer() + 1);
        setOptionDisabled(true);
    }

    @Override
    public void next() {
        int index = getQuestions().indexOf(getQuestion());
        if (getQuestions().size() - 1 > index) {
            if (getQuestions().size() - 2 == index) {
                clear();
            }
            setQuestion(getQuestions().get(index + 1));
            setOption(getQuestion().getSelectedOption());
            saveSession(getQuestion().getImage());
            if (isLastQuestion(getQuestions(), getQuestion())) {
                if (getQuestion().getSelectedOption() == null) {
                    clear();
                } else setNextQuestionRender(getQuestions().size() < questionCount * 2);
            } else {
                setNextQuestionRender(true);
            }
            setPreviousQuestionRender(!isFirstQuestion(getQuestions(), getQuestion()));
            setOptionDisabled(!getQuestion().getAnswer().equals(Answer.NO_ANSWERING));
        } else {
            counter++;
            if (hasNextQuestion(counter, signQuestionNumbers.size() + ruleQuestionNumbers.size())) {
                try {
                    if (counter < questionCount) {
                        signCounter++;
                        setQuestion(questionService.selectQuestionByRowNumberAndQuestionTypeAndQuestionExam(signQuestionNumbers.get(signCounter),
                                QuestionType.SIGN, ExamType.DRIVER));
                    } else {
                        if (ruleCounter < 19) {
                            ruleCounter++;
                        }
                        setQuestion(questionService.selectQuestionByRowNumberAndQuestionTypeAndQuestionExam(ruleQuestionNumbers.get(ruleCounter),
                                QuestionType.RULE, ExamType.DRIVER));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                saveSession(getQuestion().getImage());
                getQuestions().add(getQuestion());
                clear();
                setPreviousQuestionRender(!isFirstQuestion(getQuestions(), getQuestion()));
            }
        }
    }

    @Override
    public void previous() {
        if (!isFirstQuestion(getQuestions(), getQuestion())) {
            int index = getQuestions().indexOf(getQuestion());
            setQuestion(getQuestions().get(index - 1));
            setOption(getQuestion().getSelectedOption());
            saveSession(getQuestion().getImage());
            setNextQuestionRender(true);

            if (isFirstQuestion(getQuestions(), getQuestion())) {
                setPreviousQuestionRender(false);
            }
            setOptionDisabled(!getQuestion().getAnswer().equals(Answer.NO_ANSWERING));
        }
    }

    @Override
    public void pickQuestion(Question question) {
        try {
            super.setQuestion(question);
            setOption(question.getSelectedOption());
            if (isLastQuestion(getQuestions(), question)) {
                if (question.getSelectedOption() == null) {
                    clear();
                } else setNextQuestionRender(getQuestions().size() < questionCount * 2);
            } else {
                setNextQuestionRender(true);
            }
            setPreviousQuestionRender(!isFirstQuestion(getQuestions(), question));
            setOptionDisabled(!question.getAnswer().equals(Answer.NO_ANSWERING));
            saveSession(question.getImage());
        } catch (Exception e) {
            ViewExceptionHandler.handle(e);
        }
    }

    @Override
    protected void restart() throws Exception {
        result = new Result();
        counter = 0;
        signCounter = 0;
        ruleCounter = 0;
        this.initialize(signQuestionNumbers.size(), ruleQuestionNumbers.size());
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }
}