package com.shahabkondri.drivingpractice.view.utils;

import com.shahabkondri.drivingpractice.core.crypto.EncrypterDecrypter;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

public final class Cookies {

	public static final int UNTIL_BROWSER_OPEN = -1;
	public static final int MINUTE = 60;
	public static final int HOUR = MINUTE * 60;
	public static final int DAY = HOUR * 24;
	public static final int WEEK = DAY * 7;
	public static final int MONTH = DAY * 30;
	public static final int YEAR = DAY * 365;
	public static final int MAXIMUM = Integer.MAX_VALUE;

	private Cookies() {
	}

	private static String encode(String string) {
		try {
			return URLEncoder.encode(string, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return null;
	}

	private static String decode(String string) {
		try {
			return URLDecoder.decode(string, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return null;
	}

	private static String encrypt(String string) {
		try {
			return EncrypterDecrypter.encrypt(string, "shahabkondri");
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private static String decrypt(String string) throws Exception {
		return EncrypterDecrypter.decrypt(string, "shahabkondri");
	}

	public static void save(String name, String value, int age) {
		name = encode(name);
		value = encode(value);
		javax.servlet.http.Cookie cookie = new javax.servlet.http.Cookie(name,
				value);
		cookie.setMaxAge(age);
		JsfServlet.getResponse().addCookie(cookie);
	}

	public static void saveEncrypt(String name, String value, int age) {
		save(encrypt(name), encrypt(value), age);
	}

	public static void saveWithPath(String name, String value, int age,
									String path) {
		name = encode(name);
		value = encode(value);
		javax.servlet.http.Cookie cookie = new javax.servlet.http.Cookie(name,
				value);
		cookie.setMaxAge(age);
		cookie.setPath(path);
		JsfServlet.getResponse().addCookie(cookie);
	}

	public static void saveWithPathEncrypt(String name, String value, int age,
										   String path) {
		saveWithPath(encrypt(name), encrypt(value), age, path);
	}

	public static void saveWithDomain(String name, String value, int age,
									  String domain) {
		name = encode(name);
		value = encode(value);
		javax.servlet.http.Cookie cookie = new javax.servlet.http.Cookie(name,
				value);
		cookie.setMaxAge(age);
		cookie.setDomain(domain);
		JsfServlet.getResponse().addCookie(cookie);
	}

	public static void saveWithDomainEncrypt(String name, String value,
											 int age, String domain) {
		saveWithDomain(encrypt(name), encrypt(value), age, domain);
	}

	public static void save(String name, String value, int age, String path,
							String domain) {
		name = encode(name);
		value = encode(value);
		javax.servlet.http.Cookie cookie = new javax.servlet.http.Cookie(name,
				value);
		cookie.setMaxAge(age);
		cookie.setPath(path);
		cookie.setDomain(domain);
		JsfServlet.getResponse().addCookie(cookie);
	}

	public static void saveEncrypt(String name, String value, int age,
								   String path, String domain) {
		save(encrypt(name), encrypt(value), age, path, domain);
	}

	public static void delete(String name) {
		name = encode(name);
		javax.servlet.http.Cookie[] cookies = JsfServlet.getRequest()
				.getCookies();
		if (cookies != null) {
			for (javax.servlet.http.Cookie c : cookies) {
				if (c.getName().equals(name)) {
					c.setMaxAge(0);
					c.setValue(null);
					JsfServlet.getResponse().addCookie(c);
					return;
				}
			}
		}
	}

	public static void deleteWithPath(String name, String path) {
		name = encode(name);
		javax.servlet.http.Cookie[] cookies = JsfServlet.getRequest()
				.getCookies();
		if (cookies != null) {
			for (javax.servlet.http.Cookie c : cookies) {
				if (c.getName().equals(name)) {
					c.setMaxAge(0);
					c.setPath(path);
					c.setValue(null);
					JsfServlet.getResponse().addCookie(c);
					return;
				}
			}
		}
	}

	public static void deleteEncrypted(String name) {
		delete(encrypt(name));
	}

	public static void deleteWithPathEncrypted(String name, String path) {
		deleteWithPath(encrypt(name), path);
	}

	public static String get(String name) {
		name = encode(name);
		javax.servlet.http.Cookie[] cookies = JsfServlet.getRequest()
				.getCookies();
		if (cookies != null) {
			for (javax.servlet.http.Cookie c : cookies) {
				if (c.getName() != null && c.getName().trim().length() != 0
						&& c.getName().equals(name) && c.getValue() != null
						&& c.getValue().trim().length() != 0) {
					return decode(c.getValue());
				}
			}
		}
		return null;
	}

	public static String getEncrypted(String name) {
		name = encrypt(name);
		name = encode(name);
		javax.servlet.http.Cookie[] cookies = JsfServlet.getRequest()
				.getCookies();
		if (cookies != null) {
			for (javax.servlet.http.Cookie c : cookies) {
				if (c.getName() != null && c.getName().trim().length() != 0
						&& c.getName().equals(name) && c.getValue() != null
						&& c.getValue().trim().length() != 0) {
					try {
						return decrypt(decode(c.getValue()));
					} catch (Exception e) {
						delete(name);
					}
				}
			}
		}
		return null;
	}

	public static Map<String, String> getAll() {
		Map<String, String> map = new HashMap<>();
		javax.servlet.http.Cookie[] cookies = JsfServlet.getRequest()
				.getCookies();
		if (cookies != null) {
			for (javax.servlet.http.Cookie c : cookies) {
				map.put(decode(c.getName()), decode(c.getValue()));
			}
		}
		return map;
	}

	public static Map<String, String> getAllEncrypted() {
		Map<String, String> map = new HashMap<>();
		javax.servlet.http.Cookie[] cookies = JsfServlet.getRequest()
				.getCookies();
		if (cookies != null) {
			for (javax.servlet.http.Cookie c : cookies) {
				try {
					map.put(decrypt(decode(c.getName())),
							decrypt(decode(c.getValue())));
				} catch (Exception e) {
					delete(c.getName());
				}
			}
		}
		return map;
	}

}
