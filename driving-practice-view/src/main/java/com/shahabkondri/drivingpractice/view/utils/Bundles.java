package com.shahabkondri.drivingpractice.view.utils;

import com.shahabkondri.drivingpractice.core.localization.Locales;

import java.io.UnsupportedEncodingException;
import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;

public class Bundles {

    private Bundles() {
    }

    public static String getBundleValueDefault(String key) {
        return getValueLocale("com.shahabkondri.drivingpractice.view.localization.bundle", key);
    }

    public static String getBundleValueDefault(String key, Object... params) {
        return getValueLocale("com.shahabkondri.drivingpractice.view.localization.bundle", key, params);
    }

    public static String getMessageValueDefault(String key) {
        return getValueLocale("com.shahabkondri.drivingpractice.view.localization.messages", key);
    }

    public static String getMessageValueDefault(String key, Object... params) {
        return getValueLocale("com.shahabkondri.drivingpractice.view.localization.messages", key, params);
    }

    public static String getExceptionsValueDefault(String key) {
        return getValueLocale("com.shahabkondri.drivingpractice.view.localization.exceptions", key);
    }

    public static String getExceptionsValueDefault(String key, Object... params) {
        return getValueLocale("com.shahabkondri.drivingpractice.view.localization.exceptions", key);
    }

    public static String getEnumValueDefault(String key) {
        return getValueLocale("com.shahabkondri.drivingpractice.view.localization.enum", key);
    }

    public static String getEnumValueDefault(String key, Object... params) {
        return getValueLocale("com.shahabkondri.drivingpractice.view.localization.enum", key, params);
    }


    public static String getValueLocale(String address, String key) {
        ResourceBundle rs = ResourceBundle.getBundle(address, Locales.getLocale());
        return rs.getString(key);
    }

    public static String getValueLocale(String address, String key, Object... params) {
        ResourceBundle rs = ResourceBundle.getBundle(address, Locales.getLocale());
        String message = rs.getString(key);
        if (params != null && params.length != 0) {
            message = MessageFormat.format(message, params);
        }
        return message;
    }

    public static String getValueLocaleUTF8(String address, String key) {
        ResourceBundle rs = ResourceBundle.getBundle(address, Locales.getLocale());
        String str = rs.getString(key);
        try {
            str = new String(rs.getString(key).getBytes("ISO-8859-1"), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return str;
    }

    public static String getValueLocaleUTF8(String address, String key, Object... params) {
        ResourceBundle rs = ResourceBundle.getBundle(address, Locales.getLocale());
        String str = rs.getString(key);
        if (params != null && params.length != 0) {
            str = MessageFormat.format(str, params);
        }
        try {
            str = new String(rs.getString(key).getBytes("ISO-8859-1"), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return str;
    }

    public static String getValueLocale(String address, String key, String fromEncoding, String toEncoding)
            throws UnsupportedEncodingException {
        ResourceBundle rs = ResourceBundle.getBundle(address, Locales.getLocale());
        return new String(rs.getString(key).getBytes(fromEncoding), toEncoding);
    }

    public static String getValueUTF8(String address, String key) {
        ResourceBundle rs = ResourceBundle.getBundle(address);
        String str = rs.getString(key);
        try {
            str = new String(rs.getString(key).getBytes("ISO-8859-1"), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return str;
    }

    public static String getValueUTF8(String address, String key, Object... params) {
        ResourceBundle rs = ResourceBundle.getBundle(address);
        String str = rs.getString(key);
        if (params != null && params.length != 0) {
            str = MessageFormat.format(str, params);
        }
        try {
            str = new String(rs.getString(key).getBytes("ISO-8859-1"), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return str;
    }

    public static String getValue(String address, String key, Locale locale) {
        ResourceBundle rs = ResourceBundle.getBundle(address, locale);
        return rs.getString(key);
    }

    public static String getValue(String address, String key, Locale locale, Object... params) {
        ResourceBundle rs = ResourceBundle.getBundle(address, locale);
        String str = rs.getString(key);
        if (params != null && params.length != 0) {
            str = MessageFormat.format(str, params);
        }
        return str;
    }

    public static String getValueUTF8(String address, String key, Locale locale) {
        ResourceBundle rs = ResourceBundle.getBundle(address, locale);
        String str = rs.getString(key);
        try {
            str = new String(rs.getString(key).getBytes("ISO-8859-1"), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return str;
    }

    public static String getValue(String address, String key, String locale) {
        return getValue(address, key, new Locale(locale));
    }

    public static String getValueUTF8(String address, String key, String locale) {
        return getValueUTF8(address, key, new Locale(locale));
    }

    public static String getValue(String address, String key,
                                  String fromEncoding, String toEncoding)
            throws UnsupportedEncodingException {
        ResourceBundle rs = ResourceBundle.getBundle(address);
        return new String(rs.getString(key).getBytes(fromEncoding),
                toEncoding);
    }

    public static String getValue(String address, String key, Locale locale,
                                  String fromEncoding, String toEncoding)
            throws UnsupportedEncodingException {
        ResourceBundle rs = ResourceBundle.getBundle(address, locale);
        return new String(rs.getString(key).getBytes(fromEncoding),
                toEncoding);
    }

    public static String getValue(String address, String key, String locale,
                                  String fromEncoding, String toEncoding)
            throws UnsupportedEncodingException {
        return getValue(address, key, new Locale(locale), fromEncoding,
                toEncoding);
    }

}
