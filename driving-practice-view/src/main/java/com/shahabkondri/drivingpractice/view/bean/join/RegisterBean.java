package com.shahabkondri.drivingpractice.view.bean.join;

import com.shahabkondri.drivingpractice.api.GeneralService;
import com.shahabkondri.drivingpractice.api.identity.PasswordService;
import com.shahabkondri.drivingpractice.core.utils.Validator;
import com.shahabkondri.drivingpractice.entity.enumerations.Gender;
import com.shahabkondri.drivingpractice.entity.enumerations.LocaleType;
import com.shahabkondri.drivingpractice.entity.identity.Account;
import com.shahabkondri.drivingpractice.entity.identity.AccountRelationship;
import com.shahabkondri.drivingpractice.entity.identity.Activation;
import com.shahabkondri.drivingpractice.entity.identity.Password;
import com.shahabkondri.drivingpractice.entity.identity.enumeration.ActivationType;
import com.shahabkondri.drivingpractice.view.abstracts.AbstractRegister;
import com.shahabkondri.drivingpractice.view.exception.EmailExistException;
import com.shahabkondri.drivingpractice.view.exception.handle.ViewException;
import com.shahabkondri.drivingpractice.view.exception.handle.ViewExceptionHandler;
import com.shahabkondri.drivingpractice.view.mail.ActivationMail;
import com.shahabkondri.drivingpractice.view.mail.RegistrationMail;
import com.shahabkondri.drivingpractice.view.service.ActivationViewService;
import com.shahabkondri.drivingpractice.view.service.AuthenticationService;
import com.shahabkondri.drivingpractice.view.utils.Bundles;
import com.shahabkondri.drivingpractice.view.utils.Enums;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

/**
 * Created by shahab on 05/06/16.
 */

@Named
@RequestScoped
public class RegisterBean extends AbstractRegister {
    private static final long serialVersionUID = -579337627672736725L;

    @EJB
    private GeneralService service;

    @EJB
    private PasswordService passwordService;

    @Inject
    private ActivationMail activationMail;

    @Inject
    private RegistrationMail registrationMail;

    @Inject
    private AuthenticationService authenticationService;

    private Account account = new Account();

    private Activation activation;

    private boolean render = false;


    public RegisterBean() {
    }

    @Override
    public void register() throws Exception {
        if (!Validator.isBlank(getPassword().getPassword())) {
            Password p = passwordService.selectPasswordByEmail(getPassword().getEmail());
            if (p != null) {
                throw new ViewException(new EmailExistException(), Bundles.getExceptionsValueDefault("e.register.email.exist"));
            }
            account.setEmail(getPassword().getEmail());
            activation = ActivationViewService.createActivation(ActivationType.Active);
            AccountRelationship accountRelationship = new AccountRelationship();
            accountRelationship.setAccount(account);
            accountRelationship.setPassword(getPassword());
            accountRelationship.setActivation(activation);
            accountRelationship.setLocale(LocaleType.PERSIAN);
            service.save(accountRelationship);
            registerSuccessMessage("register.save.successfully");
//            activationMail.sendActivationMail(activation, account);
            registrationMail.sendRegistrationMail(account);
            setRender(true);
            authenticate(accountRelationship);
        }
    }

    private void authenticate(AccountRelationship accountRelationship) {
        try {
            authenticationService.createStayLoginCookie(accountRelationship.getAccount().getEmail());
            authenticationService.login(accountRelationship);
        } catch (Exception e) {
            ViewExceptionHandler.handle(e);
        }
    }


    public List<SelectItem> genders() {
        return Enums.values(Gender.class);
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public boolean isRender() {
        return render;
    }

    public void setRender(boolean render) {
        this.render = render;
    }

    public Activation getActivation() {
        return activation;
    }

    public void setActivation(Activation activation) {
        this.activation = activation;
    }

    public ActivationMail getActivationMail() {
        return activationMail;
    }

    public void setActivationMail(ActivationMail activationMail) {
        this.activationMail = activationMail;
    }
}
