package com.shahabkondri.drivingpractice.view.mail;

import com.shahabkondri.drivingpractice.core.message.Email;
import com.shahabkondri.drivingpractice.core.message.Message;
import com.shahabkondri.drivingpractice.entity.identity.Account;
import com.shahabkondri.drivingpractice.view.abstracts.AbstractView;
import com.shahabkondri.drivingpractice.view.service.MailViewService;
import com.shahabkondri.drivingpractice.view.utils.Bundles;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@ViewScoped
public class RegistrationMail extends AbstractView {
    private static final long serialVersionUID = -6331161530783660427L;

    @Inject
    private MailViewService mailViewService;

    public RegistrationMail() {
    }

    public void sendRegistrationMail(Account account) {
        mailViewService.send(createRegistrationEmail(account));
    }

    private Email createRegistrationEmail(Account account) {
        Email email = new Email();
        Message message = getMessage(account);
        email.setSubject(Bundles.getMessageValueDefault("registration.mail.subject.message"));
        email.addMessage(message);
        email.addTo(account.getEmail());
        return email;
    }

    private Message getMessage(Account account) {
        String content = Bundles.getMessageValueDefault("registration.mail.content.message");
        Message message = new Message();
        message.setContent(content);
        return message;
    }

}
