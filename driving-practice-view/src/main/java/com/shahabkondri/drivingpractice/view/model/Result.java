package com.shahabkondri.drivingpractice.view.model;

import com.shahabkondri.drivingpractice.entity.enumerations.ResultType;
import com.shahabkondri.drivingpractice.entity.enumerations.TestType;

import java.io.Serializable;

/**
 * Created by shahab on 05/12/15.
 */
public class Result implements Serializable {
    private static final long serialVersionUID = -1691365542974366380L;

    private String name;

    private int correctAnswer;

    private int wrongAnswer;

    private int signCorrectAnswer;

    private int ruleCorrectAnswer;

    private int signWrongAnswer;

    private int ruleWrongAnswer;

    private TestType testType;

    private ResultType resultType;

    private boolean render = false;

    public Result() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCorrectAnswer() {
        return correctAnswer;
    }

    public void setCorrectAnswer(int correctAnswer) {
        this.correctAnswer = correctAnswer;
    }

    public int getWrongAnswer() {
        return wrongAnswer;
    }

    public void setWrongAnswer(int wrongAnswer) {
        this.wrongAnswer = wrongAnswer;
    }

    public int getSignCorrectAnswer() {
        return signCorrectAnswer;
    }

    public void setSignCorrectAnswer(int signCorrectAnswer) {
        this.signCorrectAnswer = signCorrectAnswer;
    }

    public int getRuleCorrectAnswer() {
        return ruleCorrectAnswer;
    }

    public void setRuleCorrectAnswer(int ruleCorrectAnswer) {
        this.ruleCorrectAnswer = ruleCorrectAnswer;
    }

    public int getSignWrongAnswer() {
        return signWrongAnswer;
    }

    public void setSignWrongAnswer(int signWrongAnswer) {
        this.signWrongAnswer = signWrongAnswer;
    }

    public int getRuleWrongAnswer() {
        return ruleWrongAnswer;
    }

    public void setRuleWrongAnswer(int ruleWrongAnswer) {
        this.ruleWrongAnswer = ruleWrongAnswer;
    }

    public TestType getTestType() {
        return testType;
    }

    public void setTestType(TestType testType) {
        this.testType = testType;
    }

    public ResultType getResultType() {
        return resultType;
    }

    public void setResultType(ResultType resultType) {
        this.resultType = resultType;
    }

    public boolean isRender() {
        return render;
    }

    public void setRender(boolean render) {
        this.render = render;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Result result = (Result) o;

        if (correctAnswer != result.correctAnswer) return false;
        if (wrongAnswer != result.wrongAnswer) return false;
        if (name != null ? !name.equals(result.name) : result.name != null) return false;
        if (testType != result.testType) return false;
        return resultType == result.resultType;

    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + correctAnswer;
        result = 31 * result + wrongAnswer;
        result = 31 * result + (testType != null ? testType.hashCode() : 0);
        result = 31 * result + (resultType != null ? resultType.hashCode() : 0);
        return result;
    }
}
