package com.shahabkondri.drivingpractice.view.service;

import com.shahabkondri.drivingpractice.entity.Option;
import com.shahabkondri.drivingpractice.entity.Question;
import com.shahabkondri.drivingpractice.entity.enumerations.Answer;
import com.shahabkondri.drivingpractice.entity.enumerations.ExamType;
import com.shahabkondri.drivingpractice.entity.enumerations.Location;
import com.shahabkondri.drivingpractice.entity.enumerations.QuestionType;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by shahabkondri on 30/11/15.
 */
public class OntarioSignsService {

    private List<Question> questions;

    public List<Question> getQuestions(File file, ExamType examType, QuestionType questionType, Location location) throws IOException {
        List<File> questionsFile = new ArrayList<>();

        if (file.exists()) {
            for (File f : file.listFiles()) {
                if (f.isFile() && f.getName().contains(".txt")) {
                    questionsFile.add(f);
                }
            }

            questions = new ArrayList<>();
            int count = 1;
            for (File f : questionsFile) {
                Question question = new Question();
                List<Option> options = new ArrayList<>();
                List<String> lines = Files.readAllLines(Paths.get(f.toURI()));

                String q = null;
                String o1 = null;
                String o2 = null;
                String o3 = null;
                String o4 = null;
                String correct = null;
                String hint = null;
                String description = null;
                String token = null;
                for (String line : lines) {
                    if (line.startsWith("question =")) {
                        q = line.substring(line.indexOf("=") + 1, line.length()).trim();
                    }
                    if (line.startsWith("option 1 =")) {
                        o1 = line.substring(line.indexOf("=") + 1, line.length()).trim();
                    }
                    if (line.startsWith("option 2 =")) {
                        o2 = line.substring(line.indexOf("=") + 1, line.length()).trim();
                    }
                    if (line.startsWith("option 3 =")) {
                        o3 = line.substring(line.indexOf("=") + 1, line.length()).trim();
                    }
                    if (line.startsWith("option 4 =")) {
                        o4 = line.substring(line.indexOf("=") + 1, line.length()).trim();
                    }
                    if (line.startsWith("correct answer =")) {
                        correct = line.substring(line.indexOf("=") + 1, line.length()).trim();
                    }
                    if (line.startsWith("hint =")) {
                        hint = line.substring(line.indexOf("=") + 1, line.length()).trim();
                    }
                    if (line.startsWith("description =")) {
                        description = line.substring(line.indexOf("=") + 1, line.length()).trim();
                    }
                    if (line.startsWith("Sign token =")) {
                        token = line.substring(line.indexOf("=") + 1, line.length()).trim();
                    }
                }

                question.setTitle(q);
                question.setExamType(examType);
                question.setQuestionType(questionType);
                question.setLocation(location);
                question.setQuestionNumber(count);
                if (token != null) {
                    question.setToken(token);
                }
                byte[] image = getImage(new File(file, getImageFile(f)));
                if (image != null) {
                    question.setImage(image);
                }
                question.setAnswer(Answer.NO_ANSWERING);
                if (hint != null && hint.trim().length() != 0) {
                    question.setHint(hint);
                }

                if (description != null && description.trim().length() != 0) {
                    question.setDescription(description);
                }

                int correctNumber = Integer.parseInt(correct);

                Option option1 = new Option(o1, correctNumber == 1, question);
                Option option2 = new Option(o2, correctNumber == 2, question);
                Option option3 = new Option(o3, correctNumber == 3, question);
                Option option4 = new Option(o4, correctNumber == 4, question);

                options.add(option1);
                options.add(option2);
                options.add(option3);
                options.add(option4);

                question.setOptions(options);
                questions.add(question);
                count++;
            }
        }
        return questions;
    }

    public static String getImageFile(File file) {
        return file.getName().substring(0, file.getName().indexOf(".")) + ".png";
    }

    public static byte[] getImage(File file) throws IOException {
        byte[] imageInByte = null;
        if (file.exists()) {
            BufferedImage originalImage = ImageIO.read(file);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(originalImage, "png", baos);
            baos.flush();
            imageInByte = baos.toByteArray();
            baos.close();
        }
        return imageInByte;
    }
}
