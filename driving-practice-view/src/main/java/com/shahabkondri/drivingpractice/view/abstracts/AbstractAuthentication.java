package com.shahabkondri.drivingpractice.view.abstracts;


import com.shahabkondri.drivingpractice.view.exception.handle.ViewExceptionHandler;

import javax.faces.event.ValueChangeEvent;

public abstract class AbstractAuthentication extends AbstractView {
    private static final long serialVersionUID = 919082469513552570L;

    private boolean loginStay;

    public void authenticationAction() {
        try {
            authentication();
        } catch (Exception e) {
            ViewExceptionHandler.handle(e);
        }
    }

    public abstract void authentication() throws Exception;

    public boolean loginStayValueChangeListener(ValueChangeEvent event) {
        if ((boolean) event.getNewValue()) {
            setLoginStay(true);
            return true;
        } else {
            setLoginStay(false);
            return false;
        }
    }

    public boolean isLoginStay() {
        return loginStay;
    }

    public void setLoginStay(boolean loginStay) {
        this.loginStay = loginStay;
    }
}
