package com.shahabkondri.drivingpractice.view.config;

import org.ocpsoft.rewrite.annotation.RewriteConfiguration;
import org.ocpsoft.rewrite.config.Configuration;
import org.ocpsoft.rewrite.config.ConfigurationBuilder;
import org.ocpsoft.rewrite.config.Invoke;
import org.ocpsoft.rewrite.el.El;
import org.ocpsoft.rewrite.faces.config.PhaseOperation;
import org.ocpsoft.rewrite.servlet.config.HttpConfigurationProvider;
import org.ocpsoft.rewrite.servlet.config.rule.Join;

import javax.faces.event.PhaseId;
import javax.servlet.ServletContext;

/**
 * Created by shahab on 28/12/15.
 */

@RewriteConfiguration
public class PrettyConfiguration extends HttpConfigurationProvider {

    @Override
    public Configuration getConfiguration(ServletContext servletContext) {
        return ConfigurationBuilder.begin()
                .addRule(Join.path("/home").to("/pages/public/home.xhtml"))
                .addRule(Join.path("/login").to("/pages/public/login.xhtml"))
                .addRule(Join.path("/join").to("/pages/public/register.xhtml"))
                .addRule(Join.path("/on-g1-exam-mode").to("/pages/public/ontario/g1/exam-mode.xhtml"))
                .addRule(Join.path("/on-g1-road-signs").to("/pages/public/ontario/g1/road-signs.xhtml"))
                .addRule(Join.path("/on-g1-road-rules").to("/pages/public/ontario/g1/road-rules.xhtml"))
                .addRule(Join.path("/on-az-written-examination").to("/pages/public/ontario/az/written-examination.xhtml"))
                .addRule(Join.path("/session/expired").to("/pages/public/error/sessionExpired.xhtml"))

                .addRule(Join.path("/subscribe/confirm/{email}/{token}").to("/pages/public/subscribe/confirm.xhtml"))
                .perform(PhaseOperation.enqueue(Invoke.binding(El.retrievalMethod("confirmSubscribe.load")))
                        .after(PhaseId.RESTORE_VIEW))

                .addRule(Join.path("/unsubscribe/{email}/{token}").to("/pages/public/subscribe/unsubscribe.xhtml"))
                .perform(PhaseOperation.enqueue(Invoke.binding(El.retrievalMethod("unSubscribeBean.load")))
                        .after(PhaseId.RESTORE_VIEW));
    }

    @Override
    public int priority() {
        return 10;
    }
}
