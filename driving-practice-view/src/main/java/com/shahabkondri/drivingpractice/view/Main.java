package com.shahabkondri.drivingpractice.view;

import com.shahabkondri.drivingpractice.core.utils.Files;
import com.shahabkondri.drivingpractice.core.utils.Numbers;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

/**
 * Created by shahabkondri on 07/01/17.
 */
public class Main {
    public static void main(String[] args) {
        try {
            addTokenForSigns(new File("/home/shahabkondri/Documents/work/my-project/driving-practice/driving-practice-view/src/main/resources/driving-questions/ontario/g1/sings"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void addTokenForSigns(File file) throws IOException {
        if (file != null && file.exists()) {
            File[] files = file.listFiles();
            if (files != null) {
                for (File f : files) {
                    if (f.getName().endsWith("txt")) {
                        File image = new File(f.getParentFile(), f.getName().substring(0, f.getName().indexOf(".")) + ".png");
                        if (image.exists()) {
                            String token = getToken();
                            String line = "\nSign token = " + token;
                            Files.writeStringToFile(f, line, true);

                            File destTxt = new File(f.getParent(), token + ".txt");
                            File destPng = new File(f.getParent(), token + ".png");
                            System.out.println(destTxt.getPath());
                            System.out.println(destPng.getPath());
                            f.renameTo(destTxt);
                            image.renameTo(destPng);
                        } else {
                            throw new IOException("Not found");
                        }
                    }
                }
            }
        }
    }

    public static String getToken() {
        return Numbers.normalizeUUID(UUID.randomUUID());
    }
}
