package com.shahabkondri.drivingpractice.view.exception;

/**
 * Created by shahab on 5/29/15.
 */

public class NotActivationException extends Exception {
    private static final long serialVersionUID = 1829453258242103008L;

    public NotActivationException() {
    }

    public NotActivationException(String message) {
        super(message);
    }

}
