package com.shahabkondri.drivingpractice.view.utils;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.servlet.ServletException;
import java.io.IOException;
import java.io.Serializable;

@Named
@RequestScoped
public class Dispatcher implements Serializable {
    private static final long serialVersionUID = 1357537257066399912L;

    public void redirect(String url) {
        try {
            JsfServlet.getExternalContext().redirect(
                    JsfServlet.getExternalContext().getRequestContextPath() + (url.startsWith("/") ? url : "/" + url));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void dispatch(String url) {
        try {
            JsfServlet.getRequest()
                    .getRequestDispatcher(url.startsWith("/") ? url : "/" + url).forward(JsfServlet.getRequest(), JsfServlet.getResponse());
        } catch (ServletException | IOException e) {
            e.printStackTrace();
        }
    }

}
