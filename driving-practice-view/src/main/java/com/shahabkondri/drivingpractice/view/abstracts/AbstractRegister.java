package com.shahabkondri.drivingpractice.view.abstracts;


import com.shahabkondri.drivingpractice.entity.identity.Password;
import com.shahabkondri.drivingpractice.view.exception.handle.ViewExceptionHandler;
import com.shahabkondri.drivingpractice.view.utils.Bundles;

public abstract class AbstractRegister extends AbstractView {
    private static final long serialVersionUID = 919082469513552570L;

    private Password password = new Password();

    public void registerAction() {
        try {
            register();
        } catch (Exception e) {
            ViewExceptionHandler.handle(e);
        }
    }

    public abstract void register() throws Exception;

    public void registerSuccessMessage(String key) {
        jsfMessage.addInfo(Bundles.getMessageValueDefault(key), null);
    }

    public void registerSuccessMessage(String key, Object... params) {
        jsfMessage.addInfo(Bundles.getMessageValueDefault(key, params), null);
    }

    public Password getPassword() {
        return password;
    }

    public void setPassword(Password password) {
        this.password = password;
    }
}
