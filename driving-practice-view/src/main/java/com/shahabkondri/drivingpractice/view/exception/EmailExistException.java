package com.shahabkondri.drivingpractice.view.exception;

public class EmailExistException extends Exception {
    private static final long serialVersionUID = -1428389996840086233L;

    public EmailExistException() {
    }

    public EmailExistException(String message) {
        super(message);
    }
}
