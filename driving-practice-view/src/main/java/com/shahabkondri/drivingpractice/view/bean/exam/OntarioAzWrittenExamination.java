package com.shahabkondri.drivingpractice.view.bean.exam;

import com.shahabkondri.drivingpractice.api.question.QuestionService;
import com.shahabkondri.drivingpractice.core.utils.Numbers;
import com.shahabkondri.drivingpractice.entity.Question;
import com.shahabkondri.drivingpractice.entity.enumerations.Answer;
import com.shahabkondri.drivingpractice.entity.enumerations.ExamType;
import com.shahabkondri.drivingpractice.entity.enumerations.QuestionType;
import com.shahabkondri.drivingpractice.entity.enumerations.TestType;
import com.shahabkondri.drivingpractice.view.exception.handle.ViewExceptionHandler;
import com.shahabkondri.drivingpractice.view.model.Result;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.util.LinkedList;

@Named
@SessionScoped
public class OntarioAzWrittenExamination extends AbstractExam {
    private static final long serialVersionUID = 8531878923434630520L;

    @EJB
    private QuestionService questionService;

    private LinkedList<Integer> questionNumbers;

    private Result result = new Result();

    private int counter = 0;

    public OntarioAzWrittenExamination() {
    }

    @PostConstruct
    public void postConstructor() {
        try {
            setTestType(TestType.AZ_WRITTEN_EXAMINATION);
            int azQuestionsCount = (int) questionService.selectCount(QuestionType.AZ_WRITTEN, ExamType.TRUCK);
            this.initializer(azQuestionsCount);
        } catch (Exception e) {
            ViewExceptionHandler.handle(e);
        }
    }

    private void initializer(int azQuestionsCount) throws Exception {
        if (super.getQuestion() == null) {
            questionNumbers = Numbers.generateRandomNumbers(azQuestionsCount);
            super.setQuestion(questionService.selectQuestionByRowNumberAndQuestionTypeAndQuestionExam(questionNumbers.get(counter),
                    QuestionType.AZ_WRITTEN, ExamType.TRUCK));
            super.getQuestions().add(super.getQuestion());
//            saveSession(super.getQuestion().getImage());
        }
    }

    public void optionAction() {
        try {
            if (super.getOption() != null) {
                Question question = super.getQuestions().get(counter);
                question.setSelectedOption(super.getOption());

                if (!super.getOption().isCorrectAnswer()) {
                    wrongAnswer(question);
                    if (hasNextQuestion(counter, questionNumbers.size() - 1)) {
                        super.setNextQuestionRender(true);
                    }
                }

                if (super.getOption().isCorrectAnswer()) {
                    correctAnswer(question);
                    if (hasNextQuestion(counter, questionNumbers.size())) {
                        super.setQuestion(questionService.selectQuestionByRowNumberAndQuestionTypeAndQuestionExam(questionNumbers.get(counter),
                                QuestionType.AZ_WRITTEN, ExamType.TRUCK));
//                        saveSession(super.getQuestion().getImage());
                        super.getQuestions().add(super.getQuestion());
                        clear();
                    } else {
                        setOptionDisabled(true);
                    }
                }
                setPreviousQuestionRender(!isFirstQuestion(super.getQuestions(), super.getQuestion()));
            }
        } catch (Exception e) {
            ViewExceptionHandler.handle(e);
        }
    }

    @Override
    protected void correctAnswer(Question question) {
        question.setAnswer(Answer.CORRECT);
        counter++;
        result.setCorrectAnswer(result.getCorrectAnswer() + 1);
    }

    @Override
    protected void wrongAnswer(Question question) {
        question.setAnswer(Answer.WRONG);
        result.setWrongAnswer(result.getWrongAnswer() + 1);
        super.setOptionDisabled(true);
    }

    @Override
    public void pickQuestion(Question question) {
        try {
            super.setQuestion(question);
            super.setOption(question.getSelectedOption());
            if (isLastQuestion(super.getQuestions(), question)) {
                if (question.getSelectedOption() == null) {
                    clear();
                } else super.setNextQuestionRender(super.getQuestions().size() < questionNumbers.size() -1);
            } else {
                super.setNextQuestionRender(true);
            }
            super.setPreviousQuestionRender(!isFirstQuestion(super.getQuestions(), question));
            super.setOptionDisabled(!question.getAnswer().equals(Answer.NO_ANSWERING));
//            saveSession(question.getImage());
        } catch (Exception e) {
            ViewExceptionHandler.handle(e);
        }
    }

    @Override
    public void next() {
        int index = super.getQuestions().indexOf(super.getQuestion());
        if (super.getQuestions().size() - 1 > index) {
            if (super.getQuestions().size() - 2 == index) {
                clear();
            }
            super.setQuestion(super.getQuestions().get(index + 1));
            setOption(super.getQuestion().getSelectedOption());
            saveSession(super.getQuestion().getImage());
            if (isLastQuestion(super.getQuestions(), super.getQuestion())) {
                if (super.getQuestion().getSelectedOption() == null) {
                    clear();
                } else setNextQuestionRender(super.getQuestions().size() < questionNumbers.size() -1);
            } else {
                setNextQuestionRender(true);
            }
            setPreviousQuestionRender(!isFirstQuestion(super.getQuestions(), super.getQuestion()));
            setOptionDisabled(!super.getQuestion().getAnswer().equals(Answer.NO_ANSWERING));
        } else {
            counter++;
            if (hasNextQuestion(counter, this.questionNumbers.size())) {
                try {
                    super.setQuestion(questionService.selectQuestionByRowNumberAndQuestionTypeAndQuestionExam(questionNumbers.get(counter),
                            QuestionType.AZ_WRITTEN, ExamType.TRUCK));
                } catch (Exception e) {
                    e.printStackTrace();
                }
//                saveSession(super.getQuestion().getImage());
                super.getQuestions().add(super.getQuestion());
                clear();
                setPreviousQuestionRender(!isFirstQuestion(super.getQuestions(), super.getQuestion()));
            }
        }
    }

    @Override
    public void previous() {
        if (!isFirstQuestion(super.getQuestions(), super.getQuestion())) {
            int index = super.getQuestions().indexOf(super.getQuestion());
            super.setQuestion(super.getQuestions().get(index - 1));
            super.setOption(super.getQuestion().getSelectedOption());
//            saveSession(super.getQuestion().getImage());
            setNextQuestionRender(true);
            if (isFirstQuestion(super.getQuestions(), super.getQuestion())) {
                setPreviousQuestionRender(false);
            }
            setOptionDisabled(!super.getQuestion().getAnswer().equals(Answer.NO_ANSWERING));
        }
    }

    @Override
    protected void restart() throws Exception {
        counter = 0;
        result = new Result();
        this.initializer(questionNumbers.size());
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }
}