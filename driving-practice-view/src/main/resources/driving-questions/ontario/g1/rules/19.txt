question = If your brakes fail:
option 1 = Shift into lower gear to slow down the vehicle
option 2 = Pump the brake pedal
option 3 = Apply the parking brake gently but firmly
option 4 = All of the above
correct answer = 2
hint = 
description = Although very rare (only 5% of accidents are caused by brake failure), it is still crucial to know what to do should brake failure occur. 1) If you are driving a car with a manual transmission, downshift as quickly as you can without losing control of the car. Don't shift into first or second gear too suddenly;wait until the car has slowed somewhat. If you are in a car with an automatic transmission, shift down one gear at the time. The first of the low gears is usually labeled "1." 2) Apply a mild pumping action to the brake pedal to build up some pressure, allowing you to stop in a safe manner (do this even if you do have ABS brakes). 3) Apply the parking brake gently but firmly. Apply this brake gradually to avoid locking your wheels.