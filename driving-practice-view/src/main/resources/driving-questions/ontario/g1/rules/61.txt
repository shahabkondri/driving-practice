question = When approaching a sign that reads "merging traffic," you must:
option 1 = Honk your horn first before proceeding
option 2 = Let the cars behind you go first
option 3 = Adjust your speed and position to avoid a collision with other vehicles
option 4 = Stop your vehicle before proceeding
correct answer = 3
hint = 
description = When approaching the 'Merging Traffic' sign, you must always adjust your speed and position to avoid a collision with other vehicles.