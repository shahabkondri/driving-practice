question = Upon approaching a stop sign, a driver must:
option 1 = Slow down, and if the way is clear, proceed
option 2 = Slow down, sound horn and then proceed
option 3 = Stop, and when it is safe to do so, proceed
option 4 = Stop, sound horn, then proceed
correct answer = 3
hint = 
description = Upon approaching a stop sign, a driver must always come to a full stop, then proceed when it is safe to do so.