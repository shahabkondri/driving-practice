question = You are required to keep a safe distance behind the vehicle in front of you. At 50 km/h, you should keep at least:
option 1 = Five car lengths behind the other vehicle
option 2 = Seven car lengths behind the other vehicle
option 3 = Three car lengths behind the other vehicle
option 4 = One car length behind the other vehicle
correct answer = 3
hint = 
description = At 50 km/h, you should keep at least three car lengths behind the other vehicle (approximately one car length for every 15km/h). The distance must be increased if you are driving in bad weather/driving conditions.