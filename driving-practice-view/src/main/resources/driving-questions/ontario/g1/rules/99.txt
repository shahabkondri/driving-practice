question = You are driving on a wet road and have to make a stop quickly. The best way to do this if your vehicle does not have ABS is to:
option 1 = Pump the brakes
option 2 = Press down on the brake pedal, release slightly if wheels are locking up, re-apply
option 3 = Turn off the ignition and apply the hand brake
option 4 = Roll down the window and signal
correct answer = 2
hint = 
description = 