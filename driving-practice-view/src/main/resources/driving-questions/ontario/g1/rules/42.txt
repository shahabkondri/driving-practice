question = If a traffic signal changes while a pedestrian is still in the street, which of the following has the right-of-way?
option 1 = The pedestrian
option 2 = Motorists coming from his right
option 3 = Motorists coming from his left
option 4 = Motorists making turns
correct answer = 1
hint = 
description = You must let the pedestrian finish crossing the road.