question = If you are driving and suddenly one of your tires blows out, you should:
option 1 = Bring the vehicle to a stop off the road
option 2 = Concentrate on steering
option 3 = Take your foot off the gas pedal to slow down
option 4 = All of the above
correct answer = 4
hint = 
description = If one of your tires blows while you're driving, you must focus on steering, take your foot off the gas pedal to slow down, and bring your vehicle to a full stop OFF the road.