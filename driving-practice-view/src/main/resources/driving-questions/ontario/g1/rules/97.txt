question = Examples of Criminal Code convictions include:
option 1 = Failing to remain at the scene of a collision
option 2 = Driving while disqualified
option 3 = Drinking and driving
option 4 = All of these
correct answer = 4
hint = 
description = If you are caught driving when your licence is suspended for a Criminal Code conviction, your vehicle will be impounded for at least 45 days. Examples of Criminal Code convictions include: - drinking and driving; - driving while disqualified; - failing to remain at the scene of a collision.