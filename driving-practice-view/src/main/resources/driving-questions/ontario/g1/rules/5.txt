question = Level one and two drivers must have a blood alcohol level of zero when driving. New drivers caught drinking and driving will get ______ suspension.
option 1 = 1-year
option 2 = 60-day
option 3 = 90-day
option 4 = 30-day
correct answer = 4
hint = 
description = New drivers caught drinking and driving will get a 30-day suspension for violating a condition of their Level One or Level Two licence. They can also be charged under the Criminal Code.