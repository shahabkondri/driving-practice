question = When driving in heavy fog, you should use:
option 1 = Parking lights and high beam headlights
option 2 = Low beam headlights
option 3 = Parking lights
option 4 = High beam headlights
correct answer = 2
hint = 
description = If there is a fog warning, delay your trip until it clears, if possible. Use your low beam headlights. High beams reflect off the moisture droplets in the fog, making it harder to see.