question = After dark, drivers are required to switch from high-beams to low beam headlights:
option 1 = Within 150 m of the approach of another vehicle
option 2 = Within 300 m of the approach of another vehicle
option 3 = Upon the approach of another vehicle
option 4 = This is a safety practice, not a law
correct answer = 1
hint = 
description = After dark, drivers are required to switch from high-beams to low beam headlights within 150 m when approaching another vehicle, or within 60 m, when following another vehicle.