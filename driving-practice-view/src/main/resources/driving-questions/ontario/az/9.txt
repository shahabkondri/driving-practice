question = When must a driver perform a vehicle inspection?
option 1 = Only when the driver suspects there may be a defect
option 2 = When it is convenient to do so
option 3 = Before starting the day's trip
option 4 = During the trip
correct answer = 3
hint = 
description = 
