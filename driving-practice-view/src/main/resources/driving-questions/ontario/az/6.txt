question = The maximum length of a single tractor trailer combination must not exceed:
option 1 = 23 meters (74.75 feet) unless it conforms to special requirements, in which case can be 25 meters (82 feet)
option 2 = 23 meters (74.75 feet)
option 3 = 22 meters (72 feet)
option 4 = 21 meters (69 feet)
correct answer = 2
hint = 
description = 
