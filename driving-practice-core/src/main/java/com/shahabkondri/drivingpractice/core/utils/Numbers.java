package com.shahabkondri.drivingpractice.core.utils;


import com.shahabkondri.drivingpractice.core.localization.Locales;

import java.math.BigDecimal;
import java.util.*;

public final class Numbers {

    public static LinkedList<Integer> generateRandomNumbers(int count) {
        Random rng = new Random();
        Set<Integer> generated = new LinkedHashSet<>();
        while (generated.size() < count) {
            Integer next = rng.nextInt(count) + 1;
            generated.add(next);
        }
        return new LinkedList<>(generated);
    }

    /**
     * Only use for exam mode. pick 20 first questions from all questions.
      * @param pickCount
     * @param numbers
     * @return
     */
    public static LinkedList<Integer> pickNumberFromFirst(int pickCount, LinkedList<Integer> numbers) {
        Set<Integer> pickedNumbers = new LinkedHashSet<>(pickCount);
        for (int number = 0; number < pickCount; number++) {
            pickedNumbers.add(numbers.get(number));
        }
        return new LinkedList<>(pickedNumbers);
    }

    public static String round(double number, int decimalLen) {
        return new BigDecimal(number).setScale(decimalLen,
                BigDecimal.ROUND_CEILING).toString();
    }

    public static String getTextLocale(String text) {
        return Locales.getLocale().getLanguage().equalsIgnoreCase("fa") ? getTextPersian(text)
                : getTextEnglish(text);
    }

    public static String getTextPersian(String text) {
        if (text == null || text.trim().length() == 0) {
            return text;
        }
        String s = "";
        char[] cs = text.toCharArray();
        for (char c : cs) {
            if (Character.isDigit(c)) {
                s += (char) (Character.getNumericValue(c) + 1776);
            } else {
                s += c;
            }
        }
        return s;
    }

    public static String getTextEnglish(String text) {
        if (text == null || text.trim().length() == 0) {
            return text;
        }
        String s = "";
        char[] cs = text.toCharArray();
        for (char c : cs) {
            if (Character.isDigit(c)) {
                s += Character.getNumericValue(c);
            } else {
                s += c;
            }
        }
        return s;
    }

    public static String normalizeUUID(UUID uuid) {
        return uuid.toString().replaceAll("-", "");
    }

    public static String split(String s) {
        boolean b = isDecimal(s);
        String decimal = "";
        String result = "";
        if (b && s.indexOf('.') != -1) {
            decimal = s.substring(s.lastIndexOf('.'), s.length());
            s = s.substring(0, s.lastIndexOf('.'));
        }
        String[] strings = new StringBuilder(s).reverse().toString()
                .split("(?<=\\G...)");
        result += new StringBuilder(strings[strings.length - 1]).reverse()
                .toString();
        for (int i = strings.length - 2; i >= 0; i--) {
            result += "," + new StringBuilder(strings[i]).reverse().toString();
        }
        if (b && !decimal.matches("\\.[0]*")) {
            result += decimal;
        }
        return result;
    }

    private static boolean isDecimal(String string) {
        try {
            @SuppressWarnings("unused")
            Double d = Double.valueOf(getTextEnglish(string));
            return true;
        } catch (Throwable e) {
            return false;
        }
    }

}
