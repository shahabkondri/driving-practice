package com.shahabkondri.drivingpractice.core.time;

import java.io.Serializable;
import java.time.*;
import java.time.chrono.AbstractChronology;
import java.time.chrono.Era;
import java.time.chrono.IsoEra;
import java.time.format.ResolverStyle;
import java.time.temporal.ChronoField;
import java.time.temporal.TemporalAccessor;
import java.time.temporal.TemporalField;
import java.time.temporal.ValueRange;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * The Persian calendar system.
 */
public final class PersianChronology extends AbstractChronology implements Serializable {

    /**
     * Singleton instance of the Persian chronology.
     */
    public static final PersianChronology INSTANCE = new PersianChronology();

    /**
     * Serialization version.
     */
    private static final long serialVersionUID = -1440403870442975015L;

    /**
     * Restricted constructor.
     */
    private PersianChronology() {
    }

    /**
     * Gets the ID of the chronology - 'Persian'.
     */
    @Override
    public String getId() {
        return "Persian";
    }

    /**
     * Gets the calendar type of the underlying calendar system - 'iso8601'.
     */
    @Override
    public String getCalendarType() {
        return "iso8601";
    }

    //-----------------------------------------------------------------------

    /**
     * Obtains an Persian local date from the era, year-of-era, month-of-year
     * and day-of-month fields.
     */
    @Override
    public Date date(Era era, int yearOfEra, int month, int dayOfMonth) {
        return date(prolepticYear(era, yearOfEra), month, dayOfMonth);
    }

    /**
     * Obtains an Persian local date from the proleptic-year, month-of-year
     * and day-of-month fields.
     */
    @Override
    public Date date(int prolepticYear, int month, int dayOfMonth) {
        return Date.of(prolepticYear, PersianMonth.of(month), dayOfMonth);
    }

    /**
     * Obtains an Persian local date from the era, year-of-era and day-of-year fields.
     */
    @Override
    public Date dateYearDay(Era era, int yearOfEra, int dayOfYear) {
        return dateYearDay(prolepticYear(era, yearOfEra), dayOfYear);
    }

    /**
     * Obtains an Persian local date from the proleptic-year and day-of-year fields.
     */
    @Override
    public Date dateYearDay(int prolepticYear, int dayOfYear) {
        return Date.ofYearDay(prolepticYear, dayOfYear);
    }

    /**
     * Obtains an Persian local date from the epoch-day.
     */
    @Override
    public Date dateEpochDay(long epochDay) {
        return Date.ofEpochDay(epochDay);
    }

    /**
     * Obtains an Persian local date from another date-time object.
     */
    @Override
    public Date date(TemporalAccessor temporal) {
        return Date.from(temporal);
    }

    /**
     * Obtains an Persian local date-time from another date-time object.
     */
    @Override
    public DateTime localDateTime(TemporalAccessor temporal) {
        return DateTime.from(temporal);
    }

    /**
     * Obtains an Persian zoned date-time from another date-time object.
     */
    @Override
    public ZonedDateTime zonedDateTime(TemporalAccessor temporal) {
        return ZonedDateTime.from(temporal);
    }

    /**
     * Obtains an Persian zoned date-time in this chronology from an {@code Instant}.
     */
    @Override
    public ZonedDateTime zonedDateTime(Instant instant, ZoneId zone) {
        return ZonedDateTime.ofInstant(instant, zone);
    }

    /**
     * Obtains the current Persian local date from the system clock in the default time-zone.
     */
    @Override
    public Date dateNow() {
        return dateNow(Clock.systemDefaultZone());
    }

    /**
     * Obtains the current Persian local date from the system clock in the specified time-zone.
     */
    @Override
    public Date dateNow(ZoneId zone) {
        return dateNow(Clock.system(zone));
    }

    /**
     * Obtains the current Persian local date from the specified clock.
     */
    @Override
    public Date dateNow(Clock clock) {
        Objects.requireNonNull(clock, "clock");
        return date(Date.now(clock));
    }

    /**
     * Checks if the year is a leap year, according to the Persian proleptic
     * calendar system rules.
     */
    @Override
    public boolean isLeapYear(long prolepticYear) {
        return ((prolepticYear % 33 == 1) || (prolepticYear % 33 == 5) || (prolepticYear % 33 == 9) || (prolepticYear % 33 == 13)
                || (prolepticYear % 33 == 17) || (prolepticYear % 33 == 22) || (prolepticYear % 33 == 26) || (prolepticYear % 33 == 30));
    }

    @Override
    public int prolepticYear(Era era, int yearOfEra) {
        if (!(era instanceof IsoEra)) {
            throw new ClassCastException("Era must be IsoEra");
        }
        return (era == IsoEra.CE ? yearOfEra : 1 - yearOfEra);
    }

    @Override
    public IsoEra eraOf(int eraValue) {
        return IsoEra.of(eraValue);
    }

    @Override
    public List<Era> eras() {
        return Arrays.asList(IsoEra.values());
    }

    /**
     * Resolves parsed {@code ChronoField} values into a date during parsing.
     */
    @Override
    public Date resolveDate(Map<TemporalField, Long> fieldValues, ResolverStyle resolverStyle) {
        return (Date) super.resolveDate(fieldValues, resolverStyle);
    }

    @Override
    public ValueRange range(ChronoField field) {
        return field.range();
    }

    /**
     * Obtains a period for this chronology based on years, months and days.
     */
    @Override
    public Period period(int years, int months, int days) {
        return Period.of(years, months, days);
    }

}
