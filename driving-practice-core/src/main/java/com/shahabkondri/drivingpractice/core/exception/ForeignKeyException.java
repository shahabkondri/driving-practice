package com.shahabkondri.drivingpractice.core.exception;

import javax.ejb.ApplicationException;

@ApplicationException
public class ForeignKeyException extends KondriException {

    private static final long serialVersionUID = 8460803306428442691L;

    private final String constraint;

    public ForeignKeyException(String message, String constraint) {
        super(message);
        this.constraint = constraint;
    }

    public String getConstraint() {
        return constraint;
    }

}
