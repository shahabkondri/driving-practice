package com.shahabkondri.drivingpractice.core.message;

public enum RecipientType {

    TO, CC, BCC

}
