package com.shahabkondri.drivingpractice.core.message;

public class Attachment {

    private String name;
    private String contentType;
    private byte[] attachment;

    public Attachment() {
    }

    public Attachment(String name, String contentType, byte[] attachment) {
        this.name = name;
        this.contentType = contentType;
        this.attachment = attachment;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContentType() {
        if (contentType == null) {
            contentType = "application/octet-stream";
        }
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public byte[] getAttachment() {
        return attachment;
    }

    void setAttachment(byte[] attachment) {
        this.attachment = attachment;
    }

}
