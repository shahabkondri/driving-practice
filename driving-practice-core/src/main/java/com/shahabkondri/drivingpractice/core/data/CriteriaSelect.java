package com.shahabkondri.drivingpractice.core.data;

import java.io.Serializable;
import java.util.List;

public interface CriteriaSelect {

    <T extends Serializable> T selectSingle(CriteriaMetaData metaData) throws Exception;

    <T extends Serializable> T selectFirst(CriteriaMetaData metaData) throws Exception;

    <T extends Serializable> List<T> select(CriteriaMetaData metaData) throws Exception;

    int selectCount(CriteriaMetaData metaData) throws Exception;

}
