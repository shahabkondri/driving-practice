package com.shahabkondri.drivingpractice.core.message;

import jodd.mail.EmailFilter;

/**
 * Created by shahab on 3/21/15.
 */
public class ReceiveFilter extends EmailFilter {

    private String folder;

    public String getFolder() {
        return folder;
    }

    public void setFolder(String folder) {
        this.folder = folder;
    }
}