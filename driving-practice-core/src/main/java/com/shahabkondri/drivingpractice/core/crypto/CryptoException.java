package com.shahabkondri.drivingpractice.core.crypto;

/**
 * <b>Title:</b> CryptoException
 * Created on 9/2/15.
 *
 * @author shahabfereydooni
 * @version 1.0.0
 */
public class CryptoException extends RuntimeException {
    private static final long serialVersionUID = 3393315517717362996L;

    public CryptoException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public CryptoException(Throwable t) {
        super(t);
    }
}