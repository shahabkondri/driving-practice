package com.shahabkondri.drivingpractice.core.localization;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public final class Dispatcher {

	private Dispatcher() {
	}

	public static void dispatch(String url, ServletContext context,
			ServletRequest request, ServletResponse response)
			throws IOException, ServletException {
		context.getRequestDispatcher(url.startsWith("/") ? url : "/" + url)
				.forward(request, response);
	}

	public static void redirect(String url, ServletRequest request,
			ServletResponse response) throws IOException {
		((HttpServletResponse) response)
				.sendRedirect(((HttpServletRequest) request).getContextPath()
						+ (url.startsWith("/") ? url : "/" + url));
	}

	public static void dispatch(String url, ServletRequest request,
			ServletResponse response) throws IOException, ServletException {
		request.getRequestDispatcher(url.startsWith("/") ? url : "/" + url)
				.forward(request, response);
	}

}
