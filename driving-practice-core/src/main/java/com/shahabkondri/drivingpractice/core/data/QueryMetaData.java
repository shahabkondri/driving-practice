package com.shahabkondri.drivingpractice.core.data;

import java.util.HashMap;
import java.util.Map;

public class QueryMetaData extends MetaData {

    private final String query;
    private Map<String, Object> params;
    private Map<Integer, Object> positionParams;
    private String graph;

    public QueryMetaData(String query) {
        this.query = query;
    }

    public String getQuery() {
        return query;
    }

    public Map<String, Object> getParams() {
        return params;
    }

    public void setParams(Map<String, Object> params) {
        this.params = params;
    }

    public void addParam(String name, Object value) {
        if (params == null) {
            params = new HashMap<>();
        }
        params.put(name, value);
    }

    public Map<Integer, Object> getPositionParams() {
        return positionParams;
    }

    public void setPositionParams(Map<Integer, Object> positionParams) {
        this.positionParams = positionParams;
    }

    public void addPositionParam(int position, Object value) {
        if (positionParams == null) {
            positionParams = new HashMap<>();
        }
        positionParams.put(position, value);
    }

    public String getGraph() {
        return graph;
    }

    public void setGraph(String graph) {
        this.graph = graph;
    }

}
