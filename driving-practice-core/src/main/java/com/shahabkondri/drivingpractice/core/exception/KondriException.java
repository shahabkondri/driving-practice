package com.shahabkondri.drivingpractice.core.exception;

public class KondriException extends Exception {

    public KondriException() {
    }

    public KondriException(String message) {
        super(message);
    }

    public KondriException(String message, Throwable cause) {
        super(message, cause);
    }

}
