package com.shahabkondri.drivingpractice.core.data;

import java.io.Serializable;

public class MetaData implements Serializable {

    private Integer first;
    private Integer max;

    public MetaData() {
    }

    public Integer getFirst() {
        return first;
    }

    public void setFirst(Integer first) {
        this.first = first;
    }

    public Integer getMax() {
        return max;
    }

    public void setMax(Integer max) {
        this.max = max;
    }

}
