package com.shahabkondri.drivingpractice.core.utils;

import javax.validation.Validation;
import javax.validation.ValidatorFactory;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class Validator {

    private Validator() {
    }

    public static boolean isValid(String pattern, String matcher) {
        Pattern patt = Pattern.compile(pattern);
        Matcher match = patt.matcher(matcher);
        return match.matches();
    }

    public static <T> boolean isValid(T t) {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        javax.validation.Validator validator = factory.getValidator();
        return validator.validate(t).size() == 0;
    }

    public static boolean isEmail(String email) {
        email = Numbers.getTextEnglish(email);
        return isValid(
                "^([0-9a-zA-Z]([-.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$", email);
    }

    public static boolean isDomain(String domain) {
        domain = Numbers.getTextEnglish(domain);
        return isValid(
                "^([a-zA-Z0-9]([a-zA-Z0-9\\-]{0,61}[a-zA-Z0-9])?\\.)+[a-zA-Z]{2,6}$",
                domain);
    }

    public static boolean isHtml(String content) {
        return content.startsWith("<html>");
    }

    public static boolean isNumber(String number) {
        number = Numbers.getTextEnglish(number);
        return isValid("[-]?(([0-9]+)|([0-9]+\\.[0-9]+))", number);
    }

    public static boolean isTel(String tel) {
        tel = Numbers.getTextEnglish(tel);
        return isValid("(0|\\+98|0098)+[1-8]{1}+[0-9]{9}", tel);
    }

    public static boolean isCell(String cell) {
        cell = Numbers.getTextEnglish(cell);
        return isValid("((09)|(\\+989)|(00989))+[0-9]{9}", cell);
    }

    public static boolean isTelOrCell(String number) {
        number = Numbers.getTextEnglish(number);
        return isValid("((0)|(\\+98)|(0098))+[1-9]{1}+[0-9]{9}", number);
    }

    public static boolean isPostalCode(String postalCode) {
        postalCode = Numbers.getTextEnglish(postalCode);
        return isValid("([0-9]{5}[-][0-9]{5})||[0-9]{5,10}", postalCode);
    }

    public static boolean isUsername(String username) {
        username = Numbers.getTextEnglish(username);
        return isValid("^[a-zA-Z0-9_-]{4,64}$", username);
    }

    public static boolean isPassword(String password) {
        password = Numbers.getTextEnglish(password);
        return isValid("^[a-zA-Z0-9@#$%_-]{4,64}$", password);
    }

    public static boolean isIp(String ip) {
        ip = Numbers.getTextEnglish(ip);
        return isValid(
                "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])$",
                ip);
    }

    public static boolean isColorHexadecimal(String color) {
        color = Numbers.getTextEnglish(color);
        return isValid("^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$", color);
    }

    public static boolean isISBN13(String isbn) {
        int check = 0;
        for (int i = 0; i < 12; i += 2) {
            check += Integer.valueOf(isbn.substring(i, i + 1));
        }
        for (int i = 1; i < 12; i += 2) {
            check += Integer.valueOf(isbn.substring(i, i + 1)) * 3;
        }
        check += Integer.valueOf(isbn.substring(12));
        return check % 10 == 0;
    }

    public static boolean isISBN10(String isbn) {
        int a = 0, b = 0;
        for (int i = 0; i < 10; i++) {
            if (isbn.charAt(i) == 'X') {
                a += 10;
            } else {
                a += isbn.charAt(9 - i) - '0';
            }
            b += a;
        }
        return b % 11 == 0;
    }

    public static boolean isISBN(String isbn) {
        return isISBN10(isbn) || isISBN13(isbn);
    }

    public static boolean isTime(String time) {
        time = Numbers.getTextEnglish(time);
        return isValid(
                "(((([0-1]{1}[0-9]{1})|(2[0-3]{1})|([0-9]{1}))|((([0-1]{1}[0-9]{1})|(2[0-3]{1})|([0-9]{1}))((:(([0-5]{1}[0-9]{1})|([0-9]{1})):(([0-5]{1}[0-9]{1})|([0-9]{1})))|(:(([0-5]{1}[0-9]{1})|([0-9]{1}))))))|(((([0-1]{1}[0-9]{1})|(2[0-3]{1}))|((([0-1]{1}[0-9]{1})|(2[0-3]{1}))(([0-5]{1}[0-9]{1}[0-5]{1}[0-9]{1})|([0-5]{1}[0-9]{1}))))))",
                time);
    }

    public static boolean isPersianDate(String date) {
        return JalaliCalendarValidator.isValid(date);
    }


    public static boolean isBlank(String str) {
        return str == null || str.trim().length() == 0;
    }

}
