package com.shahabkondri.drivingpractice.core.exception;

import javax.enterprise.context.Dependent;
import java.io.FileNotFoundException;
import java.nio.file.AccessDeniedException;
import java.util.regex.Pattern;
import java.util.zip.ZipException;

@Dependent
public class Exceptions {

    private Exceptions() {
    }

    public boolean isDuplicateZipEntryException(Throwable throwable) {
        if (throwable instanceof ZipException && throwable.getMessage().startsWith("duplicate entry")) {
            return true;
        } else {
            Throwable cause = throwable.getCause();
            return cause != null && isDuplicateZipEntryException(cause);
        }
    }

    public boolean isAccessDeniedException(Throwable throwable) {
        if (throwable instanceof AccessDeniedException || (throwable instanceof FileNotFoundException &&
                (throwable.getMessage().endsWith("(Permission denied)") || throwable.getMessage().endsWith("(Access is denied)")))) {
            return true;
        } else {
            Throwable cause = throwable.getCause();
            return cause != null && isAccessDeniedException(cause);
        }
    }

    public String isForeignKeyException(Throwable throwable) {
        if (Pattern.compile(Pattern.quote("foreign key constraint"), Pattern.CASE_INSENSITIVE).matcher(throwable.getMessage()).find()
                || Pattern.compile(Pattern.quote("integrity constraint"), Pattern.CASE_INSENSITIVE).matcher(throwable.getMessage()).find()) {
            int fkIndex = throwable.getMessage().toLowerCase().indexOf("fk_");
//            int endIndex = throwable.getMessage().indexOf(')', fkIndex);
//            if (endIndex < 0) {
//                endIndex = throwable.getMessage().indexOf('"', fkIndex);
//            }
            int endIndex = throwable.getMessage().indexOf('"', fkIndex);
            if (endIndex < 0) {
                endIndex = throwable.getMessage().indexOf('\'', fkIndex);
            }
            return throwable.getMessage().substring(fkIndex, endIndex).toUpperCase();
        } else {
            Throwable cause = throwable.getCause();
            if (cause == null) {
                return null;
            } else {
                return isForeignKeyException(cause);
            }
        }
    }

    public String isUniqueException(Throwable throwable) {
        if (Pattern.compile(Pattern.quote("unique"), Pattern.CASE_INSENSITIVE).matcher(throwable.getMessage()).find()
                || Pattern.compile(Pattern.quote("duplicate"), Pattern.CASE_INSENSITIVE).matcher(throwable.getMessage()).find()) {
            int ukIndex = throwable.getMessage().toLowerCase().indexOf("uk_");
//            int endIndex = throwable.getMessage().indexOf(')', ukIndex);
//            if (endIndex < 0) {
//                endIndex = throwable.getMessage().indexOf('"', ukIndex);
//            }
            int endIndex = throwable.getMessage().indexOf('"', ukIndex);
            if (endIndex < 0) {
                endIndex = throwable.getMessage().indexOf('\'', ukIndex);
            }
            return throwable.getMessage().substring(ukIndex, endIndex).toUpperCase();
        } else {
            Throwable cause = throwable.getCause();
            if (cause == null) {
                return null;
            } else {
                return isUniqueException(cause);
            }
        }
    }

    public boolean isConcurrentException(Throwable throwable) {
        if (throwable.getClass().getName().equals("org.hibernate.StaleObjectStateException")
                || throwable.getClass().getName().equals("org.eclipse.persistence.exceptions.OptimisticLockException")) {
            return true;
        } else {
            Throwable cause = throwable.getCause();
            return cause != null && isConcurrentException(cause);
        }
    }

}
