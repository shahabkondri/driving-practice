package com.shahabkondri.drivingpractice.core.utils;

import java.io.*;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("ResultOfMethodCallIgnored")
public class Files {

    public static void copyFileUsingFileChannels(File source, File out)
            throws IOException {
        FileChannel inputChannel = null;
        FileChannel outputChannel = null;
        try {
            inputChannel = new FileInputStream(source).getChannel();
            outputChannel = new FileOutputStream(out).getChannel();
            outputChannel.transferFrom(inputChannel, 0, inputChannel.size());
        } finally {
            if (inputChannel != null) {
                inputChannel.close();
            }
            if (outputChannel != null) {
                outputChannel.close();
            }
        }
    }

    private static void copy(File source, File target, List<String> exclusionList,
                             final boolean excludeHidden, final String... excludes) throws IOException {
        File[] files = source.listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                for (String s : excludes) {
                    if (pathname.getName().equals(s) || (excludeHidden && pathname.isHidden())) {
                        return false;
                    }
                }
                return true;
            }
        });
        target.mkdirs();
        for (File file : files) {
            File targetFile = new File(target, file.getName());
            if (exclusionList == null || !exclusionList.contains(file.getCanonicalPath())) {
                if (file.isDirectory()) {
                    copy(file, targetFile, exclusionList, excludeHidden, excludes);
                } else {
                    FileInputStream fileInputStream = new FileInputStream(file);
                    if (!targetFile.exists() || targetFile.length() != file.length()) {
                        FileOutputStream fileOutputStream = new FileOutputStream(targetFile);
                        byte[] buffer = new byte[4096];
                        int length;
                        while ((length = fileInputStream.read(buffer)) > 0) {
                            fileOutputStream.write(buffer, 0, length);
                        }
                        fileInputStream.close();
                        fileOutputStream.flush();
                        fileOutputStream.close();
                    }
                }
            }
        }
    }

    public static void copy(File source, File target, final boolean excludeHidden,
                            final String... excludes) throws IOException {
        List<String> exclusionList = null;
        if (target.getCanonicalPath().startsWith(source.getCanonicalPath())) {
            File[] files = source.listFiles(new FileFilter() {
                @Override
                public boolean accept(File pathname) {
                    for (String s : excludes) {
                        if (pathname.getName().equals(s) || (excludeHidden && pathname.isHidden())) {
                            return false;
                        }
                    }
                    return true;
                }
            });
            if (files != null && files.length > 0) {
                exclusionList = new ArrayList<String>(files.length);
                for (File file : files) {
                    File copiedFile = new File(target, file.getName());
                    exclusionList.add(copiedFile.getCanonicalPath());
                }
            }
        }
        copy(source, target, exclusionList, excludeHidden, excludes);
    }

    public static void copy(final InputStream src, final OutputStream dest) throws IOException {
        final ReadableByteChannel inputChannel = Channels.newChannel(src);
        final WritableByteChannel outputChannel = Channels.newChannel(dest);
        fastCopy(inputChannel, outputChannel);
    }

    private static void fastCopy(final ReadableByteChannel src, final WritableByteChannel dest) throws IOException {
        final ByteBuffer buffer = ByteBuffer.allocateDirect(16 * 1024);

        while (src.read(buffer) != -1) {
            buffer.flip();
            dest.write(buffer);
            buffer.compact();
        }

        buffer.flip();

        while (buffer.hasRemaining()) {
            dest.write(buffer);
        }
    }

    public static String readFileToString(File file) throws IOException {
        return readURLToString(file.toURI().toURL());
    }

    public static String readInputStreamToString(InputStream fis) throws IOException {
        BufferedReader bufferedReader = null;
        InputStreamReader inputStreamReader = null;
        try {
            bufferedReader = new BufferedReader(inputStreamReader = new InputStreamReader(fis, "UTF8"));

            StringBuilder ret = new StringBuilder();
            String sep = System.getProperty("line.separator");
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                if (ret.length() > 0) {
                    ret.append(sep);
                }
                ret.append(line);
            }
            return ret.toString();
        } finally {
            if (bufferedReader != null) {
                bufferedReader.close();
            }
            if (inputStreamReader != null) {
                inputStreamReader.close();
            }
        }
    }

    public static String readURLToString(URL resourceURL) throws IOException {
        InputStream fis = null;
        try {
            fis = resourceURL.openStream();
            return readInputStreamToString(fis);
        } finally {
            if (fis != null) {
                fis.close();
            }
        }
    }

    public static void writeStringToFile(File file, String string, boolean append) throws IOException {
        if (file == null) {
            throw new IllegalArgumentException("File is null.");
        }
        if (!append && file.exists()) {
            throw new IllegalArgumentException("File already exists: " + file);
        }
        file.createNewFile();
        if (!file.isFile()) {
            throw new IllegalArgumentException("Is not a file: " + file);
        }
        if (!file.canWrite()) {
            throw new IllegalArgumentException("Cannot write to file: " + file);
        }

        FileWriter fw = new FileWriter(file.getAbsoluteFile(), append);
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(string);
        bw.close();
    }

    public static void writeToFile(File file, byte[] data)
            throws IOException {
        if (file == null) {
            throw new IllegalArgumentException("File is null.");
        }
        if (file.exists()) {
            throw new IllegalArgumentException("File already exists: " + file);
        }
        file.createNewFile();
        if (!file.isFile()) {
            throw new IllegalArgumentException("Is not a file: " + file);
        }
        if (!file.canWrite()) {
            throw new IllegalArgumentException("Cannot write to file: " + file);
        }
        FileOutputStream out = new FileOutputStream(file);
        out.write(data);
        out.close();
    }

    public static boolean delete(File file) {
        if (file.isDirectory()) {
            String[] children = file.list();
            for (String child : children) {
                boolean success = delete(new File(file, child));
                if (!success) {
                    return false;
                }
            }
        }
        return file.delete();
    }

    public static String getExtension(String string) {
        return string.split("\\.(?=[^\\.]+$)")[string.split("\\.(?=[^\\.]+$)").length - 1];
    }
}