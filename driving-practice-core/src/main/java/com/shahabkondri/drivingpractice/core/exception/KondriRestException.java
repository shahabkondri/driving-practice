package com.shahabkondri.drivingpractice.core.exception;

import javax.ws.rs.client.ResponseProcessingException;
import javax.ws.rs.core.Response;

public class KondriRestException extends ResponseProcessingException {

    public KondriRestException(Response response, Throwable cause) {
        super(response, cause);
    }

    public KondriRestException(Response response, String message, Throwable cause) {
        super(response, message, cause);
    }

    public KondriRestException(Response response, String message) {
        super(response, message);
    }

    public KondriRestException(Throwable cause) {
        super(Response.serverError().build(), cause);
    }

    public KondriRestException(String message, Throwable cause) {
        super(Response.serverError().build(), message, cause);
    }

    public KondriRestException(String message) {
        super(Response.serverError().build(), message);
    }

}
