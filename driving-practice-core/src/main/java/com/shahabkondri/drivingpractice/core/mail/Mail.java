package com.shahabkondri.drivingpractice.core.mail;

import com.shahabkondri.drivingpractice.core.message.Attachment;
import com.shahabkondri.drivingpractice.core.message.Message;
import com.shahabkondri.drivingpractice.core.message.MessageRecipient;
import com.shahabkondri.drivingpractice.core.message.RecipientType;
import jodd.mail.*;
import jodd.mail.att.ByteArrayAttachment;

import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by shahab on 6/2/15.
 */
public class Mail implements Serializable {
    private static final long serialVersionUID = -8428751528618945671L;

    private SendMailSession sendMailSession;

    public Mail(Session session) throws NoSuchProviderException {
        sendMailSession = new SendMailSession(session, session.getTransport());
    }

    public void send(List<com.shahabkondri.drivingpractice.core.message.Email> emails) throws AddressException {
        for (com.shahabkondri.drivingpractice.core.message.Email email : emails) {
            send(email);
        }
    }

    public void send(com.shahabkondri.drivingpractice.core.message.Email email) throws AddressException {
        sendMailSession.open();
        Email e = createDefaultEmail();
        e.setFrom(new MailAddress(email.getFrom()));
        e.setSentDate(new Date());
        e.setSubject(email.getSubject());
        e.setTo(getMailAddress(email, RecipientType.TO));
        e.setCc(getMailAddress(email, RecipientType.CC));
        e.setBcc(getMailAddress(email, RecipientType.BCC));
        for (Message message : email.getMessages()) {
            if (message.getMimeType().equals(Message.TEXT_HTML)) {
                e.addHtml(message.getContent(), "UTF-8");
            } else {
                e.addText(message.getContent(), "UTF-8");
            }
        }
        setAttachment(email, e);
        sendMailSession.sendMail(e);
        sendMailSession.close();
    }

    private List<com.shahabkondri.drivingpractice.core.message.Email> parseEmail(ReceivedEmail[] receivedEmails) {
        List<com.shahabkondri.drivingpractice.core.message.Email> emails = new ArrayList<>();
        if (receivedEmails.length != 0) {
            for (ReceivedEmail receivedEmail : receivedEmails) {
                com.shahabkondri.drivingpractice.core.message.Email email = new com.shahabkondri.drivingpractice.core.message.Email();
                email.setSubject(receivedEmail.getSubject());
                email.setFrom(receivedEmail.getFrom().getEmail());
                email.setDate(receivedEmail.getReceiveDate());

                if (receivedEmail.getTo().length != 0) {
                    for (MailAddress to : receivedEmail.getTo()) {
                        email.addTo(to.getEmail());
                    }
                }
                if (receivedEmail.getCc().length != 0) {
                    for (MailAddress cc : receivedEmail.getCc()) {
                        email.addCc(cc.getEmail());
                    }
                }
                if (receivedEmail.getBcc().length != 0) {
                    for (MailAddress bcc : receivedEmail.getBcc()) {
                        email.addBcc(bcc.getEmail());
                    }
                }

                if (receivedEmail.getAttachments() != null && !receivedEmail.getAttachments().isEmpty()) {
                    for (EmailAttachment emailAttachment : receivedEmail.getAttachments()) {
                        email.addAttachment(emailAttachment.getName(),
                                emailAttachment.getContentId(), emailAttachment.toByteArray());
                    }
                }

                receivedEmail.getAllMessages().stream().filter(message -> message.getContent() != null).forEach(message -> {
                    Message m = new Message();
                    m.setContent(message.getContent());
                    m.setMimeType(message.getMimeType());
                    m.setEncoding(message.getEncoding());
                    email.addMessage(m);
                });
                emails.add(email);
            }
        }
        return emails;
    }

    private MailAddress[] getMailAddress(com.shahabkondri.drivingpractice.core.message.Email email, RecipientType type)
            throws AddressException {
        List<MailAddress> addresses = new ArrayList<>();
        if (email.getRecipients() != null && !email.getRecipients().isEmpty()) {
            for (MessageRecipient recipient : email.getRecipients()) {
                if (recipient.getRecipientType() == type) {
                    addresses.add(new MailAddress(new InternetAddress(recipient.getRecipient())));
                }
            }
        }
        return listToArray(addresses);
    }

    private MailAddress[] listToArray(List<MailAddress> addresses) {
        MailAddress[] mailAddresses = new MailAddress[0];
        if (!addresses.isEmpty()) {
            mailAddresses = new MailAddress[addresses.size()];
            for (int i = 0; i < addresses.size(); i++) {
                mailAddresses[i] = addresses.get(i);
            }
        }
        return mailAddresses;
    }

    private void setAttachment(com.shahabkondri.drivingpractice.core.message.Email email, Email e) {
        if (email.getAttachments() != null && !email.getAttachments().isEmpty()) {
            for (Attachment attachment : email.getAttachments()) {
                EmailAttachment emailAttachment = new ByteArrayAttachment(attachment.getAttachment(),
                        attachment.getContentType(), attachment.getName(), attachment.getName());
                e.attach(emailAttachment);
            }
        }
    }

    private Email createDefaultEmail() {
        return Email.create();
    }

}