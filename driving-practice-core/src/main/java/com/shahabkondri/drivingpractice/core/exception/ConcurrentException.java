package com.shahabkondri.drivingpractice.core.exception;

import javax.ejb.ApplicationException;

@ApplicationException
public class ConcurrentException extends KondriException {

    private static final long serialVersionUID = 9109133422680478768L;

    public ConcurrentException(String message) {
        super(message);
    }

}
