package com.shahabkondri.drivingpractice.core.utils;

import java.text.DecimalFormat;
import java.util.Objects;

/**
 * @author Shahram Goodarzi
 */
public class Numeric2TextFa {

    private final String[] YEKAN_TA_19 = {"", "یک", "دو", "سه", "چهار", "پنج",
            "شش", "هفت", "هشت", "نه", "ده", "یازده", "دوازده", "سیزده",
            "چهارده", "پانزده", "شانزده", "هفده", "هجده", "نوزده"};

    private final String[] DAHGAN = {"", "", "بیست", "سی", "چهل", "پنجاه",
            "شصت", "هفتاد", "هشتاد", "نود"};

    private final String[] SADGAN = {"", "یک صد", "دویست", "سیصد", "چهارصد",
            "پانصد", "ششصد", "هفتصد", "هشتصد", "نهصد"};
    /*
     * Million Milliard Billion Billiard Trillion Trilliard Quadrillion
     * Quadrilliard Quintillion Quintilliard Sextillion Sextilliard Septillion
     * Septilliard Octillion Octilliard Nonillion Nonilliard Decillion
     * Decilliard Undecillion Undecilliard Duodecillion Duodecilliard
     * Tredecillion Tredecilliard Quattuordecillion Quattuordecilliard
     * Quindecillion Quindecilliard Sexdecillion Sexdecilliard Septendecillion
     * Septendecilliard Octodecillion Octodecilliard Novemdecillion
     * Novemdecilliard Vigintillion Vigintilliard Quinquavigintilliard
     * Trigintilliard Quinquatrigintilliard Quadragintilliard
     * Quinquaquadragintilliard Quinquagintilliard Unquinquagintillion
     * Unquinquagintilliard Duoquinquagintillion Quinquaquinquagintilliard
     * Sexaquinquagintillion Sexagintilliard Unsexagintillion
     * Quinquasexagintilliard Septuagintilliard Quinquaseptuagintilliard
     * Octogintilliard Quinquaoctogintilliard Nonagintilliard
     * Quinquanonagintilliard Centilliard Quinquagintacentilliard Ducentilliard
     * Quinquagintaducentilliard Trecentilliard Quinquagintatrecentilliard
     * Quadringentilliard Quinquagintaquadringentilliard Quingentilliard
     */
    private final String[] DEGREE = {"", "هزار", "میلیون", "میلیارد",
            "بیلیون", "بیلیارد", "ترلیون", "ترلیارد", "کوادرلیون",
            "کوادرلیارد", "سکسیلیون", "سکسیلیارد", "سپتلیون", "سپتیلیارد",
            "اوکتیلیون", "اوکتلیارد", "نانلیون", "نانلیارد", "دکلیون",
            "دکلیارد", "آندکلیون", "آندکلیارد", "دودکلیون", "دودکلیارد",
            "تردسلیون", "تردسلیارد", "کواتتوردسلیون", "کواتتوردسلیارد",
            "کویندسلیون", "کویندسلیارد", "سکسدسلیون", "سکسدسلیارد",
            "سپتندسیلیون", "سپتندسیلیارد", "اوکتودسلیون", "اوکتودسلیارد",
            "نومدسلیون", "نومدسلیارد", "ویجینتلیون", "ویجینتلیارد",
            "کوینکواویجینتلیون", "کوینکواویجینتلیارد", "تریجینتیلیون",
            "تریجینتیلیارد", "کوینکواتریجینتیلیون", "کوینکواتریجینتیلیارد",
            "کوادراجینتیلیون", "کوادراجینتیلیارد", "کوینکواکوادراجینتیلیون",
            "کوینکواکوادراجینتیلیارد", "کوینکواجینتلیون", "کوینکواجینتلیارد",
            "انکوینکواجینتلیون", "انکوینکواجینتلیارد", "دوکوینکواجینتیلیون",
            "دوکوینکواجینتیلیارد", "", "", "", "", "", "", "", "", "", "", "",
            "", "", "", "", "", "", "", ""};

    private String result = "";

    public Numeric2TextFa() {
    }

    public Numeric2TextFa(String no) {
        no = Numbers.getTextEnglish(no);
        this.result = noToTxt(no);
    }

    public Numeric2TextFa(Long no) {
        this.result = noToTxt(no.toString());
    }

    public Numeric2TextFa(Double no) {
        this.result = noToTxt(no.toString());
    }

    public String toString() {
        return this.result;
    }

    public void setNumber(long no) {
        this.result = noToTxt(String.valueOf(no));
    }

    public void setNumber(String no) {
        no = Numbers.getTextEnglish(no);
        this.result = noToTxt(no);
    }

    public String getText() {
        return this.result;
    }

    private String number1Digit(int no1Dig) {
        String result = YEKAN_TA_19[no1Dig];
        return result.trim();
    }

    private String number2Digit(int no2Dig) {
        String str = String.valueOf(no2Dig).trim();
        String result;
        if (no2Dig < 20) {
            result = YEKAN_TA_19[no2Dig];
        } else {
            byte dah = Byte.valueOf(String.valueOf(str.charAt(0)));
            if (str.charAt(1) == '0') {
                result = DAHGAN[dah];
            } else {
                result = DAHGAN[dah]
                        + " و "
                        + number1Digit(Integer.parseInt(String.valueOf(str
                        .charAt(1))));
            }
        }
        return result.trim();
    }

    private String number3Digit(long no3Dig) {
        String str = String.valueOf(no3Dig).trim();
        String result;
        byte sad = Byte.valueOf(String.valueOf(str.charAt(0)));
        if ((str.charAt(1) == '0') && (str.charAt(2) == '0')) {
            return SADGAN[sad].trim();
        }
        result = SADGAN[sad].trim()
                + " و "
                + number2Digit(Byte.valueOf(String.valueOf(str.charAt(1))
                + String.valueOf(str.charAt(2))));
        return result.trim();
    }

    private String prossNumber(int no) {
        String str = String.valueOf(no).trim();
        int len = str.length();
        String result = "";
        switch (len) {
            case 1:
                result = number1Digit(no);
                break;
            case 2:
                result = number2Digit(no);
                break;
            case 3:
                result = number3Digit(no);
                break;
        }
        return result.trim();
    }

    public String noToTxt(String no) {
        no = Numbers.getTextEnglish(no);
        String num = no.trim();
        int c = 0;
        long[] nums = new long[24];
        String res = "";
        boolean isNegative = false;

        if ((num.length() == 0) || (num.length() > 72))
            return "";
        if (num.charAt(0) == '-') {
            isNegative = true;
            num = num.substring(1, num.length()).trim();
        }

        if ((num.length() == 1) && (Objects.equals(num, "0"))) {
            return "صفر";
        }

        for (int i = num.length() - 1; i >= 0; i -= 3) {
            String tr = "";
            if (i - 2 >= 0) {
                tr += num.charAt(i - 2);
            }
            if (i - 1 >= 0) {
                tr += num.charAt(i - +1);
            }
            if (i >= 0) {
                tr += num.charAt(i);
            }

            try {
                if (!Objects.equals(tr, "")) {
                    nums[c] = Long.parseLong(tr);
                }
            } catch (Exception ignored) {
            }
            c++;
        }

        for (int i = 0; i <= c - 1; i++) {
            if (nums[i] == 0) {
                res = prossNumber((int) nums[i]) + res;
            } else {
                res = prossNumber((int) nums[i]) + " " + DEGREE[i] + " و " + res;
            }
        }
        try {
            if (isNegative) {
                res = (" منفی " + res.substring(0, res.length() - 3)).trim();
            } else {
                res = res.substring(0, res.length() - 3);
            }
        } catch (Exception ignored) {
        }
        return res;
    }

}
