package com.shahabkondri.drivingpractice.core.time;

import java.io.Serializable;
import java.time.*;
import java.time.chrono.ChronoLocalDate;
import java.time.chrono.Era;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.*;
import java.time.zone.ZoneOffsetTransition;
import java.time.zone.ZoneRules;
import java.util.Objects;

import static com.shahabkondri.drivingpractice.core.time.Time.SECONDS_PER_DAY;
import static java.time.temporal.ChronoField.*;

/**
 * A date without a time-zone.
 */
public final class Date implements Temporal, TemporalAdjuster, ChronoLocalDate, Serializable {


    /**
     * The minimum supported {@code Date}, '-999999999-01-01'.
     * This could be used by an application as a "far past" date.
     */
    public static final Date MIN = Date.of(Year.MIN_VALUE, Month.JANUARY, 1);
    /**
     * The maximum supported {@code Date}, '+999999999-12-31'.
     * This could be used by an application as a "far future" date.
     */
    public static final Date MAX = Date.of(Year.MAX_VALUE, Month.DECEMBER, 31);
    /**
     * Serialization version.
     */
    private static final long serialVersionUID = 2942565459149668126L;
    /**
     * The number of days in a 400 year cycle.
     */
    private static final int DAYS_PER_CYCLE = 146097;
    /**
     * The number of days from year zero to year 1970.
     * There are five 400 year cycles from year zero to 2000.
     * There are 7 leap years from 1970 to 2000.
     */
    static final long DAYS_0000_TO_1970 = (DAYS_PER_CYCLE * 5L) - (30L * 365L + 7L);
    /**
     * The year.
     */
    private final int year;
    /**
     * The month-of-year.
     */
    private final short month;
    /**
     * The day-of-month.
     */
    private final short day;

    private final Calendar calendar;

    /**
     * Constructor, previously validated.
     *
     * @param year       the year to represent, from MIN_YEAR to MAX_YEAR
     * @param month      the month-of-year to represent, not null
     * @param dayOfMonth the day-of-month to represent, valid for year-month, from 1 to 31
     */
    private Date(int year, Month month, int dayOfMonth) {
        calendar = new Calendar(year, month.getValue() - 1, dayOfMonth);
        this.year = year;
        this.month = (short) month.getValue();
        this.day = (short) dayOfMonth;
    }

    /**
     * Constructor, previously validated.
     *
     * @param year       the year to represent, from MIN_YEAR to MAX_YEAR
     * @param month      the month-of-year to represent, not null
     * @param dayOfMonth the day-of-month to represent, valid for year-month, from 1 to 31
     */
    private Date(int year, PersianMonth month, int dayOfMonth) {
        calendar = new Calendar(year + "/" + month.getValue() + "/" + dayOfMonth);
        this.year = calendar.get(Calendar.YEAR);
        this.month = (short) (calendar.get(Calendar.MONTH) + 1);
        this.day = (short) calendar.get(Calendar.DAY_OF_MONTH);
    }

    /**
     * Obtains the current date from the system clock in the default time-zone.
     *
     * @return the current date using the system clock and default time-zone, not null
     */
    public static Date now() {
        return now(Clock.systemDefaultZone());
    }

    /**
     * Obtains the current date from the system clock in the specified time-zone.
     *
     * @param zone the zone ID to use, not null
     * @return the current date using the system clock, not null
     */
    public static Date now(ZoneId zone) {
        return now(Clock.system(zone));
    }

    /**
     * Obtains the current date from the specified clock.
     *
     * @param clock the clock to use, not null
     * @return the current date, not null
     */
    public static Date now(Clock clock) {
        Objects.requireNonNull(clock, "clock");
        final Instant now = clock.instant();
        ZoneOffset offset = clock.getZone().getRules().getOffset(now);
        long epochSec = now.getEpochSecond() + offset.getTotalSeconds();
        long epochDay = Math.floorDiv(epochSec, SECONDS_PER_DAY);
        return Date.ofEpochDay(epochDay);
    }

    /**
     * Obtains an instance of {@code Date} from a year, month and day.
     *
     * @param year       the year to represent, from MIN_YEAR to MAX_YEAR
     * @param month      the month-of-year to represent, not null
     * @param dayOfMonth the day-of-month to represent, from 1 to 31
     * @return the local date, not null
     * @throws DateTimeException if the value of any field is out of range,
     *                           or if the day-of-month is invalid for the month-year
     */
    public static Date of(int year, Month month, int dayOfMonth) {
        YEAR.checkValidValue(year);
        Objects.requireNonNull(month, "month");
        DAY_OF_MONTH.checkValidValue(dayOfMonth);
        if (dayOfMonth > 28) {
            int dom = 31;
            switch (month.getValue()) {
                case 2:
                    dom = (PersianChronology.INSTANCE.isLeapYear(year) ? 29 : 28);
                    break;
                case 4:
                case 6:
                case 9:
                case 11:
                    dom = 30;
                    break;
            }
            if (dayOfMonth > dom) {
                if (dayOfMonth == 29) {
                    throw new DateTimeException("Invalid date 'February 29' as '" + year + "' is not a leap year");
                } else {
                    throw new DateTimeException("Invalid date '" + Month.of(month.getValue()).name() + " " + dayOfMonth + "'");
                }
            }
        }
        return new Date(year, month, dayOfMonth);
    }

    /**
     * Obtains an instance of {@code Date} from a year, month and day.
     *
     * @param year       the year to represent, from MIN_YEAR to MAX_YEAR
     * @param month      the month-of-year to represent, not null
     * @param dayOfMonth the day-of-month to represent, from 1 to 31
     * @return the local date, not null
     * @throws DateTimeException if the value of any field is out of range,
     *                           or if the day-of-month is invalid for the month-year
     */
    public static Date of(int year, PersianMonth month, int dayOfMonth) {
        YEAR.checkValidValue(year);
        Objects.requireNonNull(month, "month");
        DAY_OF_MONTH.checkValidValue(dayOfMonth);
        if (dayOfMonth > 29) {
            int dom = 31;
            switch (month.getValue()) {
                case 12:
                    dom = (PersianChronology.INSTANCE.isLeapYear(year) ? 30 : 29);
                    break;
                case 7:
                case 8:
                case 9:
                case 10:
                case 11:
                    dom = 30;
                    break;
            }
            if (dayOfMonth > dom) {
                if (dayOfMonth == 30) {
                    throw new DateTimeException("Invalid date 'ESFAND 30' as '" + year + "' is not a leap year");
                } else {
                    throw new DateTimeException("Invalid date '" + Month.of(month.getValue()).name() + " " + dayOfMonth + "'");
                }
            }
        }
        return new Date(year, month, dayOfMonth);
    }

    /**
     * Obtains an instance of {@code Date} from a year and day-of-year.
     *
     * @param year      the year to represent, from MIN_YEAR to MAX_YEAR
     * @param dayOfYear the day-of-year to represent, from 1 to 366
     * @return the local date, not null
     * @throws DateTimeException if the value of any field is out of range,
     *                           or if the day-of-year is invalid for the month-year
     */
    public static Date ofYearDay(int year, int dayOfYear) {
        YEAR.checkValidValue(year);
        DAY_OF_YEAR.checkValidValue(dayOfYear);
        boolean leap = PersianChronology.INSTANCE.isLeapYear(year);
        if (dayOfYear == 366 && !leap) {
            throw new DateTimeException("Invalid date 'DayOfYear 366' as '" + year + "' is not a leap year");
        }
        Month moy = Month.of((dayOfYear - 1) / 31 + 1);
        int monthEnd = moy.firstDayOfYear(leap) + moy.length(leap) - 1;
        if (dayOfYear > monthEnd) {
            moy = moy.plus(1);
        }
        int dom = dayOfYear - moy.firstDayOfYear(leap) + 1;
        return new Date(year, moy, dom);
    }

    public static Date ofPersianYearDay(int year, int dayOfYear) {
        //TODO
        return null;
    }

    /**
     * Obtains an instance of {@code Date} from the epoch day count.
     *
     * @param epochDay the Epoch Day to convert, based on the epoch 1970-01-01
     * @return the local date, not null
     * @throws DateTimeException if the epoch days exceeds the supported date range
     */
    public static Date ofEpochDay(long epochDay) {
        long zeroDay = epochDay + DAYS_0000_TO_1970;
        zeroDay -= 60;
        long adjust = 0;
        if (zeroDay < 0) {
            long adjustCycles = (zeroDay + 1) / DAYS_PER_CYCLE - 1;
            adjust = adjustCycles * 400;
            zeroDay += -adjustCycles * DAYS_PER_CYCLE;
        }
        long yearEst = (400 * zeroDay + 591) / DAYS_PER_CYCLE;
        long doyEst = zeroDay - (365 * yearEst + yearEst / 4 - yearEst / 100 + yearEst / 400);
        if (doyEst < 0) {
            yearEst--;
            doyEst = zeroDay - (365 * yearEst + yearEst / 4 - yearEst / 100 + yearEst / 400);
        }
        yearEst += adjust;
        int marchDoy0 = (int) doyEst;

        int marchMonth0 = (marchDoy0 * 5 + 2) / 153;
        int month = (marchMonth0 + 2) % 12 + 1;
        int dom = marchDoy0 - (marchMonth0 * 306 + 5) / 10 + 1;
        yearEst += marchMonth0 / 10;

        int year = YEAR.checkValidIntValue(yearEst);
        return new Date(year, Month.of(month), dom);
    }

    public static Date ofPersianEpochDay(long epochDay) {
        //TODO
        return null;
    }

    /**
     * Obtains an instance of {@code Date} from a temporal object.
     *
     * @param temporal the temporal object to convert, not null
     * @return the local date, not null
     * @throws DateTimeException if unable to convert to a {@code Date}
     */
    public static Date from(TemporalAccessor temporal) {
        Objects.requireNonNull(temporal, "temporal");
        LocalDate date = temporal.query(TemporalQueries.localDate());
        if (date == null) {
            throw new DateTimeException("Unable to obtain Date from TemporalAccessor: " + temporal + " of type " + temporal.getClass().getName());
        }
        return new Date(date.getYear(), date.getMonth(), date.getDayOfMonth());
    }

    /**
     * Obtains an instance of {@code Date} from a text string such as {@code 2007-12-03}.
     *
     * @param text the text to parse such as "2007-12-03", not null
     * @return the parsed local date, not null
     * @throws DateTimeParseException if the text cannot be parsed
     */
    public static Date parse(CharSequence text) {
        return parse(text, DateTimeFormatter.ISO_LOCAL_DATE);
    }

    public static Date parsePersian(String text) {
        return Date.from(new Calendar(text).toZonedDateTime().toLocalDate());
    }

    /**
     * Obtains an instance of {@code Date} from a text string using a specific formatter.
     *
     * @param text      the text to parse, not null
     * @param formatter the formatter to use, not null
     * @return the parsed local date, not null
     * @throws DateTimeParseException if the text cannot be parsed
     */
    public static Date parse(CharSequence text, DateTimeFormatter formatter) {
        Objects.requireNonNull(formatter, "formatter");
        return formatter.parse(text, Date::from);
    }

    /**
     * Resolves the date, resolving days past the end of month.
     *
     * @param year  the year to represent, validated from MIN_YEAR to MAX_YEAR
     * @param month the month-of-year to represent, validated from 1 to 12
     * @param day   the day-of-month to represent, validated from 1 to 31
     * @return the resolved date, not null
     */
    private static Date resolvePreviousValid(int year, int month, int day) {
        switch (month) {
            case 2:
                day = Math.min(day, PersianChronology.INSTANCE.isLeapYear(year) ? 29 : 28);
                break;
            case 4:
            case 6:
            case 9:
            case 11:
                day = Math.min(day, 30);
                break;
        }
        //TODO
        return new Date(year, Month.of(month), day);
    }

    /**
     * Checks if the specified field is supported.
     *
     * @param field the field to check, null returns false
     * @return true if the field is supported on this date, false if not
     */
    @Override
    public boolean isSupported(TemporalField field) {
        if (field instanceof PersianField) {
            return field.isDateBased();
        }
        return ChronoLocalDate.super.isSupported(field);
    }

    /**
     * Checks if the specified unit is supported.
     *
     * @param unit the unit to check, null returns false
     * @return true if the unit can be added/subtracted, false if not
     */
    @Override
    public boolean isSupported(TemporalUnit unit) {
        return ChronoLocalDate.super.isSupported(unit);
    }

    /**
     * Gets the range of valid values for the specified field.
     *
     * @param field the field to query the range for, not null
     * @return the range of valid values for the field, not null
     * @throws DateTimeException                if the range for the field cannot be obtained
     * @throws UnsupportedTemporalTypeException if the field is not supported
     */
    @Override
    public ValueRange range(TemporalField field) {
        if (field instanceof ChronoField) {
            ChronoField f = (ChronoField) field;
            if (f.isDateBased()) {
                switch (f) {
                    case DAY_OF_MONTH:
                        return ValueRange.of(1, lengthOfMonth());
                    case DAY_OF_YEAR:
                        return ValueRange.of(1, lengthOfYear());
                    case ALIGNED_WEEK_OF_MONTH:
                        return ValueRange.of(1, getMonth() == Month.FEBRUARY && !isLeapYear() ? 4 : 5);
                    case YEAR_OF_ERA:
                        return (getYear() <= 0 ? ValueRange.of(1, Year.MAX_VALUE + 1) : ValueRange.of(1, Year.MAX_VALUE));
                }
                return field.range();
            }
            throw new UnsupportedTemporalTypeException("Unsupported field: " + field);
        }
        return field.rangeRefinedBy(this);
    }

    /**
     * Gets the value of the specified field from this date as an {@code int}.
     *
     * @param field the field to get, not null
     * @return the value for the field
     * @throws DateTimeException                if a value for the field cannot be obtained or
     *                                          the value is outside the range of valid values for the field
     * @throws UnsupportedTemporalTypeException if the field is not supported or
     *                                          the range of values exceeds an {@code int}
     * @throws ArithmeticException              if numeric overflow occurs
     */
    @Override
    public int get(TemporalField field) {
        if (field instanceof ChronoField) {
            return get0(field);
        }
        return ChronoLocalDate.super.get(field);
    }

    /**
     * Gets the value of the specified field from this date as a {@code long}.
     *
     * @param field the field to get, not null
     * @return the value for the field
     * @throws DateTimeException                if a value for the field cannot be obtained
     * @throws UnsupportedTemporalTypeException if the field is not supported
     * @throws ArithmeticException              if numeric overflow occurs
     */
    @Override
    public long getLong(TemporalField field) {
        if (field instanceof PersianField) {
            //TODO
            switch ((PersianField) field) {
                case DAY_OF_MONTH:
                    return calendar.get(Calendar.JALALI_DAY_OF_MONTH);
                case MONTH_OF_YEAR:
                    return calendar.get(Calendar.JALALI_MONTH);
                case YEAR:
                    return calendar.get(Calendar.JALALI_YEAR);
                default:
                    return 0;
            }
        }
        if (field instanceof ChronoField) {
            if (field == EPOCH_DAY) {
                return toEpochDay();
            }
            if (field == PROLEPTIC_MONTH) {
                return getProlepticMonth();
            }
            return get0(field);
        }
        return field.getFrom(this);
    }

    private int get0(TemporalField field) {
        switch ((ChronoField) field) {
            case DAY_OF_WEEK:
                return getDayOfWeek().getValue();
            case ALIGNED_DAY_OF_WEEK_IN_MONTH:
                return ((day - 1) % 7) + 1;
            case ALIGNED_DAY_OF_WEEK_IN_YEAR:
                return ((getDayOfYear() - 1) % 7) + 1;
            case DAY_OF_MONTH:
                return day;
            case DAY_OF_YEAR:
                return getDayOfYear();
            case EPOCH_DAY:
                throw new UnsupportedTemporalTypeException("Invalid field 'EpochDay' for get() method, use getLong() instead");
            case ALIGNED_WEEK_OF_MONTH:
                return ((day - 1) / 7) + 1;
            case ALIGNED_WEEK_OF_YEAR:
                return ((getDayOfYear() - 1) / 7) + 1;
            case MONTH_OF_YEAR:
                return month;
            case PROLEPTIC_MONTH:
                throw new UnsupportedTemporalTypeException("Invalid field 'ProlepticMonth' for get() method, use getLong() instead");
            case YEAR_OF_ERA:
                return (year >= 1 ? year : 1 - year);
            case YEAR:
                return year;
            case ERA:
                return (year >= 1 ? 1 : 0);
        }
        throw new UnsupportedTemporalTypeException("Unsupported field: " + field);
    }

    private long getProlepticMonth() {
        return (year * 12L + month - 1);
    }

    /**
     * Gets the chronology of this date, which is the ISO calendar system.
     *
     * @return the ISO chronology, not null
     */
    @Override
    public PersianChronology getChronology() {
        return PersianChronology.INSTANCE;
    }

    /**
     * Gets the era applicable at this date.
     *
     * @return the {@code PersianChronology} era constant applicable at this date, not null
     */
    @Override
    public Era getEra() {
        return ChronoLocalDate.super.getEra();
    }

    /**
     * Gets the year field.
     *
     * @return the year, from MIN_YEAR to MAX_YEAR
     */
    public int getYear() {
        return year;
    }

    public int getPersianYear() {
        return calendar.get(Calendar.JALALI_YEAR);
    }

    /**
     * Gets the month-of-year field from 1 to 12.
     *
     * @return the month-of-year, from 1 to 12
     * @see #getMonth()
     */
    public int getMonthValue() {
        return month;
    }

    /**
     * Gets the month-of-year field using the {@code Month} enum.
     *
     * @return the month-of-year, not null
     * @see #getMonthValue()
     */
    public Month getMonth() {
        return Month.of(month);
    }

    public PersianMonth getPersianMonth() {
        return PersianMonth.of(calendar.get(Calendar.JALALI_MONTH));
    }

    /**
     * Gets the day-of-month field.
     *
     * @return the day-of-month, from 1 to 31
     */
    public int getDayOfMonth() {
        return day;
    }

    /**
     * Gets the day-of-year field.
     *
     * @return the day-of-year, from 1 to 365, or 366 in a leap year
     */
    public int getDayOfYear() {
        return getMonth().firstDayOfYear(isLeapYear()) + day - 1;
    }

    /**
     * Gets the day-of-week field, which is an enum {@code DayOfWeek}.
     *
     * @return the day-of-week, not null
     */
    public DayOfWeek getDayOfWeek() {
        int dow0 = (int) Math.floorMod(toEpochDay() + 3, 7);
        return DayOfWeek.of(dow0 + 1);
    }

    /**
     * Checks if the year is a leap year, according to the ISO proleptic
     * calendar system rules.
     *
     * @return true if the year is leap, false otherwise
     */
    @Override
    public boolean isLeapYear() {
        return PersianChronology.INSTANCE.isLeapYear(year);
    }

    public boolean isPersianLeapYear() {
        return ((year % 33 == 1) || (year % 33 == 5) || (year % 33 == 9) || (year % 33 == 13)
                || (year % 33 == 17) || (year % 33 == 22) || (year % 33 == 26) || (year % 33 == 30));
    }

    /**
     * Returns the length of the month represented by this date.
     *
     * @return the length of the month in days
     */
    @Override
    public int lengthOfMonth() {
        switch (month) {
            case 2:
                return (isLeapYear() ? 29 : 28);
            case 4:
            case 6:
            case 9:
            case 11:
                return 30;
            default:
                return 31;
        }
    }

    public int persianLengthOfMonth() {
        switch (calendar.get(Calendar.JALALI_MONTH)) {
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
                return 30;
            case 12:
                return (isPersianLeapYear() ? 30 : 29);
            default:
                return 31;
        }
    }

    /**
     * Returns the length of the year represented by this date.
     *
     * @return 366 if the year is leap, 365 otherwise
     */
    @Override
    public int lengthOfYear() {
        return (isLeapYear() ? 366 : 365);
    }

    /**
     * Returns an adjusted copy of this date.
     *
     * @param adjuster the adjuster to use, not null
     * @return a {@code Date} based on {@code this} with the adjustment made, not null
     * @throws DateTimeException   if the adjustment cannot be made
     * @throws ArithmeticException if numeric overflow occurs
     */
    @Override
    public Date with(TemporalAdjuster adjuster) {
        if (adjuster instanceof Date) {
            return (Date) adjuster;
        }
        return (Date) adjuster.adjustInto(this);
    }

    /**
     * Returns a copy of this date with the specified field set to a new value.
     *
     * @param field    the field to set in the result, not null
     * @param newValue the new value of the field in the result
     * @return a {@code Date} based on {@code this} with the specified field set, not null
     * @throws DateTimeException                if the field cannot be set
     * @throws UnsupportedTemporalTypeException if the field is not supported
     * @throws ArithmeticException              if numeric overflow occurs
     */
    @Override
    public Date with(TemporalField field, long newValue) {
        if (field instanceof PersianField) {
            //TODO
        }
        if (field instanceof ChronoField) {
            ChronoField f = (ChronoField) field;
            f.checkValidValue(newValue);
            switch (f) {
                case DAY_OF_WEEK:
                    return plusDays(newValue - getDayOfWeek().getValue());
                case ALIGNED_DAY_OF_WEEK_IN_MONTH:
                    return plusDays(newValue - getLong(ALIGNED_DAY_OF_WEEK_IN_MONTH));
                case ALIGNED_DAY_OF_WEEK_IN_YEAR:
                    return plusDays(newValue - getLong(ALIGNED_DAY_OF_WEEK_IN_YEAR));
                case DAY_OF_MONTH:
                    return withDayOfMonth((int) newValue);
                case DAY_OF_YEAR:
                    return withDayOfYear((int) newValue);
                case EPOCH_DAY:
                    return Date.ofEpochDay(newValue);
                case ALIGNED_WEEK_OF_MONTH:
                    return plusWeeks(newValue - getLong(ALIGNED_WEEK_OF_MONTH));
                case ALIGNED_WEEK_OF_YEAR:
                    return plusWeeks(newValue - getLong(ALIGNED_WEEK_OF_YEAR));
                case MONTH_OF_YEAR:
                    return withMonth((int) newValue);
                case PROLEPTIC_MONTH:
                    return plusMonths(newValue - getProlepticMonth());
                case YEAR_OF_ERA:
                    return withYear((int) (year >= 1 ? newValue : 1 - newValue));
                case YEAR:
                    return withYear((int) newValue);
                case ERA:
                    return (getLong(ERA) == newValue ? this : withYear(1 - year));
            }
            throw new UnsupportedTemporalTypeException("Unsupported field: " + field);
        }
        return field.adjustInto(this, newValue);
    }

    /**
     * Returns a copy of this date with the year altered.
     * If the day-of-month is invalid for the year, it will be changed to the last valid day of the month.
     *
     * @param year the year to set in the result, from MIN_YEAR to MAX_YEAR
     * @return a {@code Date} based on this date with the requested year, not null
     * @throws DateTimeException if the year value is invalid
     */
    public Date withYear(int year) {
        if (this.year == year) {
            return this;
        }
        YEAR.checkValidValue(year);
        return resolvePreviousValid(year, month, day);
    }

    /**
     * Returns a copy of this date with the month-of-year altered.
     * If the day-of-month is invalid for the year, it will be changed to the last valid day of the month.
     *
     * @param month the month-of-year to set in the result, from 1 (January) to 12 (December)
     * @return a {@code Date} based on this date with the requested month, not null
     * @throws DateTimeException if the month-of-year value is invalid
     */
    public Date withMonth(int month) {
        if (this.month == month) {
            return this;
        }
        MONTH_OF_YEAR.checkValidValue(month);
        return resolvePreviousValid(year, month, day);
    }

    /**
     * Returns a copy of this date with the day-of-month altered.
     * If the resulting date is invalid, an exception is thrown.
     *
     * @param dayOfMonth the day-of-month to set in the result, from 1 to 28-31
     * @return a {@code Date} based on this date with the requested day, not null
     * @throws DateTimeException if the day-of-month value is invalid,
     *                           or if the day-of-month is invalid for the month-year
     */
    public Date withDayOfMonth(int dayOfMonth) {
        if (this.day == dayOfMonth) {
            return this;
        }
        return of(year, Month.of(month), dayOfMonth);
    }

    /**
     * Returns a copy of this date with the day-of-year altered.
     * If the resulting date is invalid, an exception is thrown.
     *
     * @param dayOfYear the day-of-year to set in the result, from 1 to 365-366
     * @return a {@code Date} based on this date with the requested day, not null
     * @throws DateTimeException if the day-of-year value is invalid,
     *                           or if the day-of-year is invalid for the year
     */
    public Date withDayOfYear(int dayOfYear) {
        if (this.getDayOfYear() == dayOfYear) {
            return this;
        }
        return ofYearDay(year, dayOfYear);
    }

    /**
     * Returns a copy of this date with the specified amount added.
     *
     * @param amountToAdd the amount to add, not null
     * @return a {@code Date} based on this date with the addition made, not null
     * @throws DateTimeException   if the addition cannot be made
     * @throws ArithmeticException if numeric overflow occurs
     */
    @Override
    public Date plus(TemporalAmount amountToAdd) {
        if (amountToAdd instanceof Period) {
            Period periodToAdd = (Period) amountToAdd;
            return plusMonths(periodToAdd.toTotalMonths()).plusDays(periodToAdd.getDays());
        }
        Objects.requireNonNull(amountToAdd, "amountToAdd");
        return (Date) amountToAdd.addTo(this);
    }

    /**
     * Returns a copy of this date with the specified amount added.
     *
     * @param amountToAdd the amount of the unit to add to the result, may be negative
     * @param unit        the unit of the amount to add, not null
     * @return a {@code Date} based on this date with the specified amount added, not null
     * @throws DateTimeException                if the addition cannot be made
     * @throws UnsupportedTemporalTypeException if the unit is not supported
     * @throws ArithmeticException              if numeric overflow occurs
     */
    @Override
    public Date plus(long amountToAdd, TemporalUnit unit) {
        if (unit instanceof ChronoUnit) {
            ChronoUnit f = (ChronoUnit) unit;
            switch (f) {
                case DAYS:
                    return plusDays(amountToAdd);
                case WEEKS:
                    return plusWeeks(amountToAdd);
                case MONTHS:
                    return plusMonths(amountToAdd);
                case YEARS:
                    return plusYears(amountToAdd);
                case DECADES:
                    return plusYears(Math.multiplyExact(amountToAdd, 10));
                case CENTURIES:
                    return plusYears(Math.multiplyExact(amountToAdd, 100));
                case MILLENNIA:
                    return plusYears(Math.multiplyExact(amountToAdd, 1000));
                case ERAS:
                    return with(ERA, Math.addExact(getLong(ERA), amountToAdd));
            }
            throw new UnsupportedTemporalTypeException("Unsupported unit: " + unit);
        }
        return unit.addTo(this, amountToAdd);
    }

    /**
     * Returns a copy of this {@code Date} with the specified period in years added.
     *
     * @param yearsToAdd the years to add, may be negative
     * @return a {@code Date} based on this date with the years added, not null
     * @throws DateTimeException if the result exceeds the supported date range
     */
    public Date plusYears(long yearsToAdd) {
        if (yearsToAdd == 0) {
            return this;
        }
        int newYear = YEAR.checkValidIntValue(year + yearsToAdd);
        return resolvePreviousValid(newYear, month, day);
    }

    /**
     * Returns a copy of this {@code Date} with the specified period in months added.
     *
     * @param monthsToAdd the months to add, may be negative
     * @return a {@code Date} based on this date with the months added, not null
     * @throws DateTimeException if the result exceeds the supported date range
     */
    public Date plusMonths(long monthsToAdd) {
        if (monthsToAdd == 0) {
            return this;
        }
        long monthCount = year * 12L + (month - 1);
        long calcMonths = monthCount + monthsToAdd;
        int newYear = YEAR.checkValidIntValue(Math.floorDiv(calcMonths, 12));
        int newMonth = (int) Math.floorMod(calcMonths, 12) + 1;
        return resolvePreviousValid(newYear, newMonth, day);
    }

    /**
     * Returns a copy of this {@code Date} with the specified period in weeks added.
     *
     * @param weeksToAdd the weeks to add, may be negative
     * @return a {@code Date} based on this date with the weeks added, not null
     * @throws DateTimeException if the result exceeds the supported date range
     */
    public Date plusWeeks(long weeksToAdd) {
        return plusDays(Math.multiplyExact(weeksToAdd, 7));
    }

    /**
     * Returns a copy of this {@code Date} with the specified number of days added.
     *
     * @param daysToAdd the days to add, may be negative
     * @return a {@code Date} based on this date with the days added, not null
     * @throws DateTimeException if the result exceeds the supported date range
     */
    public Date plusDays(long daysToAdd) {
        if (daysToAdd == 0) {
            return this;
        }
        long mjDay = Math.addExact(toEpochDay(), daysToAdd);
        return Date.ofEpochDay(mjDay);
    }

    /**
     * Returns a copy of this date with the specified amount subtracted.
     *
     * @param amountToSubtract the amount to subtract, not null
     * @return a {@code Date} based on this date with the subtraction made, not null
     * @throws DateTimeException   if the subtraction cannot be made
     * @throws ArithmeticException if numeric overflow occurs
     */
    @Override
    public Date minus(TemporalAmount amountToSubtract) {
        if (amountToSubtract instanceof Period) {
            Period periodToSubtract = (Period) amountToSubtract;
            return minusMonths(periodToSubtract.toTotalMonths()).minusDays(periodToSubtract.getDays());
        }
        Objects.requireNonNull(amountToSubtract, "amountToSubtract");
        return (Date) amountToSubtract.subtractFrom(this);
    }

    /**
     * Returns a copy of this date with the specified amount subtracted.
     *
     * @param amountToSubtract the amount of the unit to subtract from the result, may be negative
     * @param unit             the unit of the amount to subtract, not null
     * @return a {@code Date} based on this date with the specified amount subtracted, not null
     * @throws DateTimeException                if the subtraction cannot be made
     * @throws UnsupportedTemporalTypeException if the unit is not supported
     * @throws ArithmeticException              if numeric overflow occurs
     */
    @Override
    public Date minus(long amountToSubtract, TemporalUnit unit) {
        return (amountToSubtract == Long.MIN_VALUE ? plus(Long.MAX_VALUE, unit).plus(1, unit) : plus(-amountToSubtract, unit));
    }

    /**
     * Returns a copy of this {@code Date} with the specified period in years subtracted.
     *
     * @param yearsToSubtract the years to subtract, may be negative
     * @return a {@code Date} based on this date with the years subtracted, not null
     * @throws DateTimeException if the result exceeds the supported date range
     */
    public Date minusYears(long yearsToSubtract) {
        return (yearsToSubtract == Long.MIN_VALUE ? plusYears(Long.MAX_VALUE).plusYears(1) : plusYears(-yearsToSubtract));
    }

    /**
     * Returns a copy of this {@code Date} with the specified period in months subtracted.
     *
     * @param monthsToSubtract the months to subtract, may be negative
     * @return a {@code Date} based on this date with the months subtracted, not null
     * @throws DateTimeException if the result exceeds the supported date range
     */
    public Date minusMonths(long monthsToSubtract) {
        return (monthsToSubtract == Long.MIN_VALUE ? plusMonths(Long.MAX_VALUE).plusMonths(1) : plusMonths(-monthsToSubtract));
    }

    /**
     * Returns a copy of this {@code Date} with the specified period in weeks subtracted.
     *
     * @param weeksToSubtract the weeks to subtract, may be negative
     * @return a {@code Date} based on this date with the weeks subtracted, not null
     * @throws DateTimeException if the result exceeds the supported date range
     */
    public Date minusWeeks(long weeksToSubtract) {
        return (weeksToSubtract == Long.MIN_VALUE ? plusWeeks(Long.MAX_VALUE).plusWeeks(1) : plusWeeks(-weeksToSubtract));
    }

    /**
     * Returns a copy of this {@code Date} with the specified number of days subtracted.
     *
     * @param daysToSubtract the days to subtract, may be negative
     * @return a {@code Date} based on this date with the days subtracted, not null
     * @throws DateTimeException if the result exceeds the supported date range
     */
    public Date minusDays(long daysToSubtract) {
        return (daysToSubtract == Long.MIN_VALUE ? plusDays(Long.MAX_VALUE).plusDays(1) : plusDays(-daysToSubtract));
    }

    /**
     * Queries this date using the specified query.
     *
     * @param <R>   the type of the result
     * @param query the query to invoke, not null
     * @return the query result, null may be returned (defined by the query)
     * @throws DateTimeException   if unable to query (defined by the query)
     * @throws ArithmeticException if numeric overflow occurs (defined by the query)
     */
    @SuppressWarnings("unchecked")
    @Override
    public <R> R query(TemporalQuery<R> query) {
        if (query == TemporalQueries.localDate()) {
            return (R) this;
        }
        return ChronoLocalDate.super.query(query);
    }

    /**
     * Adjusts the specified temporal object to have the same date as this object.
     *
     * @param temporal the target object to be adjusted, not null
     * @return the adjusted object, not null
     * @throws DateTimeException   if unable to make the adjustment
     * @throws ArithmeticException if numeric overflow occurs
     */
    @Override
    public Temporal adjustInto(Temporal temporal) {
        return ChronoLocalDate.super.adjustInto(temporal);
    }

    /**
     * Calculates the amount of time until another date in terms of the specified unit.
     *
     * @param endExclusive the end date, exclusive, which is converted to a {@code Date}, not null
     * @param unit         the unit to measure the amount in, not null
     * @return the amount of time between this date and the end date
     * @throws DateTimeException                if the amount cannot be calculated, or the end
     *                                          temporal cannot be converted to a {@code Date}
     * @throws UnsupportedTemporalTypeException if the unit is not supported
     * @throws ArithmeticException              if numeric overflow occurs
     */
    @Override
    public long until(Temporal endExclusive, TemporalUnit unit) {
        Date end = Date.from(endExclusive);
        if (unit instanceof ChronoUnit) {
            switch ((ChronoUnit) unit) {
                case DAYS:
                    return daysUntil(end);
                case WEEKS:
                    return daysUntil(end) / 7;
                case MONTHS:
                    return monthsUntil(end);
                case YEARS:
                    return monthsUntil(end) / 12;
                case DECADES:
                    return monthsUntil(end) / 120;
                case CENTURIES:
                    return monthsUntil(end) / 1200;
                case MILLENNIA:
                    return monthsUntil(end) / 12000;
                case ERAS:
                    return end.getLong(ERA) - getLong(ERA);
            }
            throw new UnsupportedTemporalTypeException("Unsupported unit: " + unit);
        }
        return unit.between(this, end);
    }

    long daysUntil(Date end) {
        return end.toEpochDay() - toEpochDay();
    }

    private long monthsUntil(Date end) {
        long packed1 = getProlepticMonth() * 32L + getDayOfMonth();
        long packed2 = end.getProlepticMonth() * 32L + end.getDayOfMonth();
        return (packed2 - packed1) / 32;
    }

    /**
     * Calculates the period between this date and another date as a {@code Period}.
     *
     * @param endDateExclusive the end date, exclusive, which may be in any chronology, not null
     * @return the period between this date and the end date, not null
     */
    @Override
    public Period until(ChronoLocalDate endDateExclusive) {
        Date end = Date.from(endDateExclusive);
        long totalMonths = end.getProlepticMonth() - this.getProlepticMonth();
        int days = end.day - this.day;
        if (totalMonths > 0 && days < 0) {
            totalMonths--;
            Date calcDate = this.plusMonths(totalMonths);
            days = (int) (end.toEpochDay() - calcDate.toEpochDay());
        } else if (totalMonths < 0 && days > 0) {
            totalMonths++;
            days -= end.lengthOfMonth();
        }
        long years = totalMonths / 12;
        int months = (int) (totalMonths % 12);
        return Period.of(Math.toIntExact(years), months, days);
    }

    /**
     * Formats this date using the specified formatter.
     *
     * @param formatter the formatter to use, not null
     * @return the formatted date string, not null
     * @throws DateTimeException if an error occurs during printing
     */
    @Override
    public String format(DateTimeFormatter formatter) {
        Objects.requireNonNull(formatter, "formatter");
        return formatter.format(this);
    }

    /**
     * Combines this date with a time to create a {@code DateTime}.
     *
     * @param time the time to combine with, not null
     * @return the local date-time formed from this date and the specified time, not null
     */
    public DateTime atTime(Time time) {
        return DateTime.of(this, time);
    }

    /**
     * Combines this date with a time to create a {@code DateTime}.
     * <p>
     * This returns a {@code DateTime} formed from this date at the
     * specified hour and minute.
     * The seconds and nanosecond fields will be set to zero.
     * The individual time fields must be within their valid range.
     * All possible combinations of date and time are valid.
     *
     * @param hour   the hour-of-day to use, from 0 to 23
     * @param minute the minute-of-hour to use, from 0 to 59
     * @return the local date-time formed from this date and the specified time, not null
     * @throws DateTimeException if the value of any field is out of range
     */
    public DateTime atTime(int hour, int minute) {
        return atTime(Time.of(hour, minute));
    }

    /**
     * Combines this date with a time to create a {@code DateTime}.
     *
     * @param hour   the hour-of-day to use, from 0 to 23
     * @param minute the minute-of-hour to use, from 0 to 59
     * @param second the second-of-minute to represent, from 0 to 59
     * @return the local date-time formed from this date and the specified time, not null
     * @throws DateTimeException if the value of any field is out of range
     */
    public DateTime atTime(int hour, int minute, int second) {
        return atTime(Time.of(hour, minute, second));
    }

    /**
     * Combines this date with a time to create a {@code DateTime}.
     *
     * @param hour         the hour-of-day to use, from 0 to 23
     * @param minute       the minute-of-hour to use, from 0 to 59
     * @param second       the second-of-minute to represent, from 0 to 59
     * @param nanoOfSecond the nano-of-second to represent, from 0 to 999,999,999
     * @return the local date-time formed from this date and the specified time, not null
     * @throws DateTimeException if the value of any field is out of range
     */
    public DateTime atTime(int hour, int minute, int second, int nanoOfSecond) {
        return atTime(Time.of(hour, minute, second, nanoOfSecond));
    }

    /**
     * Combines this date with an offset time to create an {@code OffsetDateTime}.
     *
     * @param time the time to combine with, not null
     * @return the offset date-time formed from this date and the specified time, not null
     */
    public OffsetDateTime atTime(OffsetTime time) {
        return OffsetDateTime.of(DateTime.of(this, Time.from(time.toLocalTime())).toLocalDateTime(), time.getOffset());
    }

    /**
     * Combines this date with the time of midnight to create a {@code DateTime}
     * at the start of this date.
     *
     * @return the local date-time of midnight at the start of this date, not null
     */
    public DateTime atStartOfDay() {
        return DateTime.of(this, Time.MIDNIGHT);
    }

    /**
     * Returns a zoned date-time from this date at the earliest valid time according
     * to the rules in the time-zone.
     *
     * @param zone the zone ID to use, not null
     * @return the zoned date-time formed from this date and the earliest valid time for the zone, not null
     */
    public ZonedDateTime atStartOfDay(ZoneId zone) {
        Objects.requireNonNull(zone, "zone");
        DateTime ldt = atTime(Time.MIDNIGHT);
        if (!(zone instanceof ZoneOffset)) {
            ZoneRules rules = zone.getRules();
            ZoneOffsetTransition trans = rules.getTransition(ldt.toLocalDateTime());
            if (trans != null && trans.isGap()) {
                ldt = DateTime.from(trans.getDateTimeAfter());
            }
        }
        return ZonedDateTime.of(ldt.toLocalDateTime(), zone);
    }

    @Override
    public long toEpochDay() {
        long y = year;
        long m = month;
        long total = 0;
        total += 365 * y;
        if (y >= 0) {
            total += (y + 3) / 4 - (y + 99) / 100 + (y + 399) / 400;
        } else {
            total -= y / -4 - y / -100 + y / -400;
        }
        total += ((367 * m - 362) / 12);
        total += day - 1;
        if (m > 2) {
            total--;
            if (!isLeapYear()) {
                total--;
            }
        }
        return total - DAYS_0000_TO_1970;
    }

    /**
     * Compares this date to another date.
     *
     * @param other the other date to compare to, not null
     * @return the comparator value, negative if less, positive if greater
     */
    @Override
    public int compareTo(ChronoLocalDate other) {
        if (other instanceof Date) {
            return compareTo0((Date) other);
        }
        return ChronoLocalDate.super.compareTo(other);
    }

    int compareTo0(Date otherDate) {
        int cmp = (year - otherDate.year);
        if (cmp == 0) {
            cmp = (month - otherDate.month);
            if (cmp == 0) {
                cmp = (day - otherDate.day);
            }
        }
        return cmp;
    }

    /**
     * Checks if this date is after the specified date.
     *
     * @param other the other date to compare to, not null
     * @return true if this date is after the specified date
     */
    @Override
    public boolean isAfter(ChronoLocalDate other) {
        if (other instanceof Date) {
            return compareTo0((Date) other) > 0;
        }
        return ChronoLocalDate.super.isAfter(other);
    }

    /**
     * Checks if this date is before the specified date.
     *
     * @param other the other date to compare to, not null
     * @return true if this date is before the specified date
     */
    @Override
    public boolean isBefore(ChronoLocalDate other) {
        if (other instanceof Date) {
            return compareTo0((Date) other) < 0;
        }
        return ChronoLocalDate.super.isBefore(other);
    }

    /**
     * Checks if this date is equal to the specified date.
     *
     * @param other the other date to compare to, not null
     * @return true if this date is equal to the specified date
     */
    @Override
    public boolean isEqual(ChronoLocalDate other) {
        if (other instanceof Date) {
            return compareTo0((Date) other) == 0;
        }
        return ChronoLocalDate.super.isEqual(other);
    }

    /**
     * Checks if this date is equal to another date.
     *
     * @param obj the object to check, null returns false
     * @return true if this is equal to the other date
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof Date) {
            return compareTo0((Date) obj) == 0;
        }
        return false;
    }

    /**
     * A hash code for this date.
     *
     * @return a suitable hash code
     */
    @Override
    public int hashCode() {
        int yearValue = year;
        int monthValue = month;
        int dayValue = day;
        return (yearValue & 0xFFFFF800) ^ ((yearValue << 11) + (monthValue << 6) + (dayValue));
    }

    /**
     * Outputs this date as a {@code String}, such as {@code 2007-12-03}.
     *
     * @return a string representation of this date, not null
     */
    @Override
    public String toString() {
        //TODO
        if (calendar != null) {
            return calendar.toString();
        }
        int yearValue = year;
        int monthValue = month;
        int dayValue = day;
        int absYear = Math.abs(yearValue);
        StringBuilder buf = new StringBuilder(10);
        if (absYear < 1000) {
            if (yearValue < 0) {
                buf.append(yearValue - 10000).deleteCharAt(1);
            } else {
                buf.append(yearValue + 10000).deleteCharAt(0);
            }
        } else {
            if (yearValue > 9999) {
                buf.append('+');
            }
            buf.append(yearValue);
        }
        return buf.append(monthValue < 10 ? "-0" : "-").append(monthValue).append(dayValue < 10 ? "-0" : "-").append(dayValue).toString();
    }

    public String toString(String format) {
        return calendar.toString(format);
    }

    public <T extends ChronoLocalDate> boolean isBetween(T start, T end) {
        final boolean isAtOrAfterStart = this.compareTo(start) >= 0;
        final boolean isAtOrBeforeEnd = this.compareTo(end) <= 0;
        return isAtOrAfterStart && isAtOrBeforeEnd;
    }

    public LocalDate toLocalDate() {
        return LocalDate.of(year, month, day);
    }

}
