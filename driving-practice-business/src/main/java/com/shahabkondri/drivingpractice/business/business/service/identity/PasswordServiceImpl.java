package com.shahabkondri.drivingpractice.business.business.service.identity;

import com.shahabkondri.drivingpractice.api.GeneralService;
import com.shahabkondri.drivingpractice.api.identity.PasswordService;
import com.shahabkondri.drivingpractice.core.data.Criteria;
import com.shahabkondri.drivingpractice.entity.identity.Password;
import com.shahabkondri.drivingpractice.entity.identity.PasswordM;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.io.Serializable;

/**
 * Created by shahab on 5/28/15.
 */

@Stateless
public class PasswordServiceImpl implements PasswordService, Serializable {
    private static final long serialVersionUID = 7529054252494971101L;

    private final PasswordM passwordM = PasswordM.password1;

    @EJB
    private GeneralService generalService;

    public PasswordServiceImpl() {
    }

    @Override
    public Password selectPasswordByEmail(String email) throws Exception {
        Criteria<PasswordM> criteria = new Criteria<>(passwordM);
        criteria.where(passwordM.email.eq(email));
        return generalService.selectSingle(criteria.getMetaData());
    }
}
