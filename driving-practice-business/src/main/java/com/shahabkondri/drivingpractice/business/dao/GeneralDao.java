package com.shahabkondri.drivingpractice.business.dao;

import com.querydsl.core.types.Expression;
import com.querydsl.core.types.dsl.PathBuilder;
import com.querydsl.jpa.impl.JPADeleteClause;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAUpdateClause;
import com.shahabkondri.drivingpractice.business.dao.interfaces.GeneralDaoLocal;
import com.shahabkondri.drivingpractice.core.data.CriteriaMetaData;
import com.shahabkondri.drivingpractice.core.data.DeleteMetaData;
import com.shahabkondri.drivingpractice.core.data.QueryMetaData;
import com.shahabkondri.drivingpractice.core.data.UpdateMetaData;
import com.shahabkondri.drivingpractice.core.exception.ConcurrentException;
import com.shahabkondri.drivingpractice.core.exception.Exceptions;
import com.shahabkondri.drivingpractice.core.exception.ForeignKeyException;
import com.shahabkondri.drivingpractice.core.exception.UniqueException;
import com.shahabkondri.drivingpractice.entity.abstracts.BaseIdentity;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Map.Entry;

@Stateless
public class GeneralDao implements GeneralDaoLocal, Serializable {
    private static final long serialVersionUID = 7393836988648709250L;

    private static final String ENTITY_GRAPH = "javax.persistence.fetchgraph";

    @Inject
    private Exceptions exceptions;

    @PersistenceContext(unitName = "drivingpractice")
    private EntityManager entityManager;

    public <T extends Serializable> T save(T t) throws Exception {
        try {
            entityManager.persist(t);
            entityManager.flush();
        } catch (Exception e) {
            String uniqueException = exceptions.isUniqueException(e);
            if (uniqueException != null) {
                throw new UniqueException(e.getMessage(), uniqueException);
            } else {
                throw e;
            }
        }
        return t;
    }

    public <T extends Serializable> List<T> save(List<T> ts) throws Exception {
        ts.forEach(entityManager::persist);
        entityManager.flush();
        return ts;
    }

    public <T extends Serializable> T update(T t) throws Exception {
        try {
            t = entityManager.merge(t);
            entityManager.flush();
        } catch (Exception e) {
            String uniqueException = exceptions.isUniqueException(e);
            if (uniqueException != null) {
                throw new UniqueException(e.getMessage(), uniqueException);
            } else if (exceptions.isConcurrentException(e)) {
                throw new ConcurrentException(e.getMessage());
            } else {
                throw e;
            }
        }
        return t;
    }

    public <T extends Serializable> List<T> update(List<T> ts) throws Exception {
        ts.forEach(entityManager::merge);
        entityManager.flush();
        return ts;
    }

    public <T extends Serializable> T saveOrUpdate(T t) throws Exception {
        try {
            final BaseIdentity<?> baseIdentity = (BaseIdentity<?>) t;
            if (baseIdentity.getId() == null) {
                entityManager.persist(t);
            } else {
                entityManager.merge(t);
            }
            entityManager.flush();
        } catch (Exception e) {
            String uniqueException = exceptions.isUniqueException(e);
            if (uniqueException != null) {
                throw new UniqueException(e.getMessage(), uniqueException);
            } else if (exceptions.isConcurrentException(e)) {
                throw new ConcurrentException(e.getMessage());
            } else {
                throw e;
            }
        }
        return t;
    }

    public <T extends Serializable> List<T> saveOrUpdate(List<T> ts) throws Exception {
        for (T t : ts) {
            final BaseIdentity<?> baseIdentity = (BaseIdentity<?>) t;
            if (baseIdentity.getId() == null) {
                entityManager.persist(t);
            } else {
                entityManager.merge(t);
            }
            entityManager.flush();
        }
        return ts;
    }

    public <T extends BaseIdentity<?>> void delete(Class<T> clas, Serializable... ids) throws Exception {
        try {
            for (Serializable id : ids) {
                final T t = entityManager.getReference(clas, id);
                entityManager.remove(t);
            }
            entityManager.flush();
        } catch (Exception e) {
            String foreignKeyException = exceptions.isForeignKeyException(e);
            if (foreignKeyException != null) {
                throw new ForeignKeyException(e.getMessage(), foreignKeyException);
            } else {
                throw e;
            }
        }
    }

    public <T extends Serializable> T selectById(Class<T> clas, Serializable id) throws Exception {
        return entityManager.find(clas, id);
    }

    @SuppressWarnings("unchecked")
    public <T extends Serializable> T selectById(Class<T> clas, Serializable id, String graph) throws Exception {
        final JPAQuery<T> jpaQuery = new JPAQuery<>(entityManager);
        final PathBuilder<T> entityPath = new PathBuilder<>(clas, "entity");
        jpaQuery.from(entityPath).where(entityPath.get("id").eq(id));
        final Query query = jpaQuery.createQuery();
        final EntityGraph<?> entityGraph = entityManager.createEntityGraph(graph);
        query.setHint(ENTITY_GRAPH, entityGraph);
        List<T> result = query.getResultList();
        return result.isEmpty() ? null : result.get(0);
    }

    @SuppressWarnings("unchecked")
    public <T extends Serializable> T selectSingle(QueryMetaData metaData) throws Exception {
        final Query query = entityManager.createNamedQuery(metaData.getQuery());
        if (metaData.getParams() != null) {
            metaData.getParams().forEach(query::setParameter);
        }
        if (metaData.getGraph() != null) {
            final EntityGraph<?> entityGraph = entityManager.createEntityGraph(metaData.getGraph());
            query.setHint(ENTITY_GRAPH, entityGraph);
        }
        final List<T> result = query.getResultList();
        if (result.size() > 1) {
            throw new PersistenceException("More than one result found.");
        }
        return result.isEmpty() ? null : result.get(0);
    }

    @SuppressWarnings("unchecked")
    public <T extends Serializable> T selectFirst(QueryMetaData metaData) throws Exception {
        final Query query = entityManager.createNamedQuery(metaData.getQuery());
        if (metaData.getParams() != null) {
            metaData.getParams().forEach(query::setParameter);
        }
        if (metaData.getGraph() != null) {
            final EntityGraph<?> entityGraph = entityManager.createEntityGraph(metaData.getGraph());
            query.setHint(ENTITY_GRAPH, entityGraph);
        }
        if (metaData.getFirst() != null) {
            query.setFirstResult(metaData.getFirst());
        }
        query.setMaxResults(1);
        final List<T> result = query.getResultList();
        return result.isEmpty() ? null : result.get(0);
    }

    @SuppressWarnings("unchecked")
    public <T extends Serializable> List<T> select(QueryMetaData metaData) throws Exception {
        final Query query = entityManager.createNamedQuery(metaData.getQuery());
        if (metaData.getParams() != null) {
            metaData.getParams().forEach(query::setParameter);
        }
        if (metaData.getGraph() != null) {
            final EntityGraph<?> entityGraph = entityManager.createEntityGraph(metaData.getGraph());
            query.setHint(ENTITY_GRAPH, entityGraph);
        }
        if (metaData.getFirst() != null) {
            query.setFirstResult(metaData.getFirst());
        }
        if (metaData.getMax() != null) {
            query.setMaxResults(metaData.getMax());
        }
        return query.getResultList();
    }


    @SuppressWarnings("unchecked")
    public <T extends Serializable> T selectQuerySingle(QueryMetaData metaData) throws Exception {
        final Query query = entityManager.createQuery(metaData.getQuery());
        if (metaData.getParams() != null) {
            metaData.getParams().forEach(query::setParameter);
        }
        if (metaData.getGraph() != null) {
            final EntityGraph<?> entityGraph = entityManager.createEntityGraph(metaData.getGraph());
            query.setHint(ENTITY_GRAPH, entityGraph);
        }
        final List<T> result = query.getResultList();
        if (result.size() > 1) {
            throw new PersistenceException("More than one result found.");
        }
        return result.isEmpty() ? null : result.get(0);
    }

    @SuppressWarnings("unchecked")
    public <T extends Serializable> T selectQueryFirst(QueryMetaData metaData) throws Exception {
        final Query query = entityManager.createQuery(metaData.getQuery());
        if (metaData.getParams() != null) {
            metaData.getParams().forEach(query::setParameter);
        }
        if (metaData.getGraph() != null) {
            final EntityGraph<?> entityGraph = entityManager.createEntityGraph(metaData.getGraph());
            query.setHint(ENTITY_GRAPH, entityGraph);
        }
        if (metaData.getFirst() != null) {
            query.setFirstResult(metaData.getFirst());
        }
        query.setMaxResults(1);
        final List<T> result = query.getResultList();
        return result.isEmpty() ? null : result.get(0);
    }

    @SuppressWarnings("unchecked")
    public <T extends Serializable> List<T> selectQuery(QueryMetaData metaData) throws Exception {
        final Query query = entityManager.createQuery(metaData.getQuery());
        if (metaData.getParams() != null) {
            metaData.getParams().forEach(query::setParameter);
        }
        if (metaData.getGraph() != null) {
            final EntityGraph<?> entityGraph = entityManager.createEntityGraph(metaData.getGraph());
            query.setHint(ENTITY_GRAPH, entityGraph);
        }
        if (metaData.getFirst() != null) {
            query.setFirstResult(metaData.getFirst());
        }
        if (metaData.getMax() != null) {
            query.setMaxResults(metaData.getMax());
        }
        return query.getResultList();
    }

    @SuppressWarnings("unchecked")
    public <T extends Serializable> T selectSingle(CriteriaMetaData metaData) throws Exception {
        final JPAQuery<T> jpaQuery = new JPAQuery<>(entityManager, metaData.getMetadata());
        if (metaData.getResult() != null) {
            jpaQuery.select(metaData.getResult());
        }
        final Query query = jpaQuery.createQuery();
        if (metaData.getGraph() != null) {
            final EntityGraph<?> entityGraph = entityManager.createEntityGraph(metaData.getGraph());
            query.setHint(ENTITY_GRAPH, entityGraph);
        }
        final List<T> result = query.getResultList();
        if (result.size() > 1) {
            throw new PersistenceException("More than one result found.");
        }
        return result.isEmpty() ? null : result.get(0);
    }

    @SuppressWarnings("unchecked")
    public <T extends Serializable> T selectFirst(CriteriaMetaData metaData) throws Exception {
        final JPAQuery<T> jpaQuery = new JPAQuery<>(entityManager, metaData.getMetadata());
        if (metaData.getResult() != null) {
            jpaQuery.select(metaData.getResult());
        }
        final Query query = jpaQuery.createQuery();
        if (metaData.getGraph() != null) {
            final EntityGraph<?> entityGraph = entityManager.createEntityGraph(metaData.getGraph());
            query.setHint(ENTITY_GRAPH, entityGraph);
        }
        if (metaData.getFirst() != null) {
            query.setFirstResult(metaData.getFirst());
        }
        query.setMaxResults(1);
        final List<T> result = query.getResultList();
        return result.isEmpty() ? null : result.get(0);
    }

    @SuppressWarnings("unchecked")
    public <T extends Serializable> List<T> select(CriteriaMetaData metaData) throws Exception {
        final JPAQuery<T> jpaQuery = new JPAQuery<>(entityManager, metaData.getMetadata());
        if (metaData.getResult() != null) {
            jpaQuery.select(metaData.getResult());
        }
        final Query query = jpaQuery.createQuery();
        if (metaData.getGraph() != null) {
            final EntityGraph<?> entityGraph = entityManager.createEntityGraph(metaData.getGraph());
            query.setHint(ENTITY_GRAPH, entityGraph);
        }
        if (metaData.getFirst() != null) {
            query.setFirstResult(metaData.getFirst());
        }
        if (metaData.getMax() != null) {
            query.setMaxResults(metaData.getMax());
        }
        return query.getResultList();
    }

    @SuppressWarnings("unchecked")
    public <T extends Serializable> T selectStoredProcedureSingle(QueryMetaData metaData) throws Exception {
        StoredProcedureQuery storedProcedureQuery = entityManager.createStoredProcedureQuery(metaData.getQuery());
        if (metaData.getParams() != null) {
            metaData.getParams().forEach(storedProcedureQuery::setParameter);
        }
        final List<T> result = storedProcedureQuery.getResultList();
        if (result.size() > 1) {
            throw new PersistenceException("More than one result found.");
        }
        return result.isEmpty() ? null : result.get(0);
    }

    @SuppressWarnings("unchecked")
    public <T extends Serializable> T selectStoredProcedureFirst(QueryMetaData metaData) throws Exception {
        StoredProcedureQuery storedProcedureQuery = entityManager.createStoredProcedureQuery(metaData.getQuery());
        if (metaData.getParams() != null) {
            metaData.getParams().forEach(storedProcedureQuery::setParameter);
        }
        storedProcedureQuery.setMaxResults(1);
        final List<T> result = storedProcedureQuery.getResultList();
        return result.isEmpty() ? null : result.get(0);
    }

    @SuppressWarnings("unchecked")
    public <T extends Serializable> List<T> selectStoredProcedure(QueryMetaData metaData) throws Exception {
        StoredProcedureQuery storedProcedureQuery = entityManager.createStoredProcedureQuery(metaData.getQuery());
        if (metaData.getParams() != null) {
            metaData.getParams().forEach(storedProcedureQuery::setParameter);
        }
        return storedProcedureQuery.getResultList();
    }

    public int executeNamedStoredProcedureQuery(QueryMetaData metaData) throws Exception {
        StoredProcedureQuery storedProcedureQuery = entityManager.createStoredProcedureQuery(metaData.getQuery());
        if (metaData.getParams() != null) {
            metaData.getParams().forEach(storedProcedureQuery::setParameter);
        }
        return storedProcedureQuery.executeUpdate();
    }

    public int executeNamedQuery(QueryMetaData metaData) throws Exception {
        Query query = entityManager.createNamedQuery(metaData.getQuery());
        if (metaData.getParams() != null) {
            metaData.getParams().forEach(query::setParameter);
        }
        return query.executeUpdate();
    }

    public int executeQuery(QueryMetaData metaData) throws Exception {
        Query query = entityManager.createQuery(metaData.getQuery());
        if (metaData.getParams() != null) {
            metaData.getParams().forEach(query::setParameter);
        }
        return query.executeUpdate();
    }

    @SuppressWarnings("unchecked")
    public int update(UpdateMetaData updateMetaData) throws Exception {
        try {
            JPAUpdateClause updateClause = new JPAUpdateClause(entityManager, updateMetaData.getEntityPath()).where(updateMetaData.getPredicates());
            updateMetaData.getUpdates().forEach((k, v) -> updateClause.set(k, (Expression) v));
            int result = ((Number) updateClause.execute()).intValue();
            entityManager.flush();
            entityManager.clear();
            return result;
        } catch (Exception e) {
            String uniqueException = exceptions.isUniqueException(e);
            if (uniqueException != null) {
                throw new UniqueException(e.getMessage(), uniqueException);
            } else if (exceptions.isConcurrentException(e)) {
                throw new ConcurrentException(e.getMessage());
            } else {
                throw e;
            }
        }
    }

    public int delete(DeleteMetaData deleteMetaData) throws Exception {
        try {
            int result = ((Number) new JPADeleteClause(entityManager, deleteMetaData.getEntityPath()).where(deleteMetaData.getPredicates()).execute()).intValue();
            entityManager.flush();
            entityManager.clear();
            return result;
        } catch (Exception e) {
            String foreignKeyException = exceptions.isForeignKeyException(e);
            if (foreignKeyException != null) {
                throw new ForeignKeyException(e.getMessage(), foreignKeyException);
            } else {
                throw e;
            }
        }
    }

    public int selectCount(CriteriaMetaData metaData) throws Exception {
        final JPAQuery<?> jpaQuery = new JPAQuery<>(entityManager, metaData.getMetadata());
        return ((Number) jpaQuery.fetchCount()).intValue();
    }


    public void flush() throws Exception {
        try {
            entityManager.flush();
        } catch (Exception e) {
            String uniqueException = exceptions.isUniqueException(e);
            if (uniqueException != null) {
                throw new UniqueException(e.getMessage(), uniqueException);
            } else if (exceptions.isConcurrentException(e)) {
                throw new ConcurrentException(e.getMessage());
            } else {
                String foreignKeyException = exceptions.isForeignKeyException(e);
                if (foreignKeyException != null) {
                    throw new ForeignKeyException(e.getMessage(), foreignKeyException);
                } else {
                    throw e;
                }
            }
        }
    }

    public <T> void detach(T t) throws Exception {
        entityManager.detach(t);
    }

    public void clear() throws Exception {
        entityManager.clear();
    }

    private void setParameters(Query query, QueryMetaData metaData) {
        if (metaData.getParams() != null) {
            metaData.getParams().forEach(query::setParameter);
        }
        if (metaData.getPositionParams() != null) {
            metaData.getPositionParams().forEach(query::setParameter);
        }
    }

}