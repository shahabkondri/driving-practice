package com.shahabkondri.drivingpractice.business.business.service;

import com.shahabkondri.drivingpractice.api.GeneralService;
import com.shahabkondri.drivingpractice.api.subscribe.SubscribeService;
import com.shahabkondri.drivingpractice.core.data.Criteria;
import com.shahabkondri.drivingpractice.entity.Subscribe;
import com.shahabkondri.drivingpractice.entity.SubscribeM;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

/**
 * Created by shahab on 16/12/15.
 */

@Stateless
@Remote(SubscribeService.class)
public class SubscribeServiceImpl implements SubscribeService {

    private final SubscribeM subscribeM = SubscribeM.subscribe;

    @EJB
    private GeneralService generalService;

    @Override
    public Subscribe selectSubscribeByEmail(String email) throws Exception {
        Criteria<Subscribe> criteria = new Criteria<>(subscribeM);
        criteria.where(subscribeM.email.eq(email));
        return generalService.selectSingle(criteria.getMetaData());
    }

    @Override
    public Subscribe selectSubscribeByEmailAndToken(String email, String token) throws Exception {
        Criteria<Subscribe> criteria = new Criteria<>(subscribeM);
        criteria.where(subscribeM.email.eq(email), subscribeM.token.eq(token));
        return generalService.selectSingle(criteria.getMetaData());
    }
}
