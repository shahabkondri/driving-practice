package com.shahabkondri.drivingpractice.business.business.service.identity;

import com.shahabkondri.drivingpractice.api.GeneralService;
import com.shahabkondri.drivingpractice.api.identity.AccountRelationshipService;
import com.shahabkondri.drivingpractice.core.data.Criteria;
import com.shahabkondri.drivingpractice.entity.identity.AccountRelationship;
import com.shahabkondri.drivingpractice.entity.identity.AccountRelationshipM;

import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 * Created by shahab on 5/28/15.
 */

@Stateless
public class AccountRelationshipServiceImpl implements AccountRelationshipService {

    private final AccountRelationshipM accountRelationshipM = AccountRelationshipM.accountRelationship;

    @EJB
    private GeneralService generalService;

    public AccountRelationshipServiceImpl() {
    }

    @Override
    public AccountRelationship selectAccountRelationshipByEmailAndPassword(String email, String password) throws Exception {
        Criteria<AccountRelationshipM> criteria = new Criteria<>(accountRelationshipM);
        criteria.where(accountRelationshipM.password.email.eq(email).and(accountRelationshipM.password.password.endsWith(password)));
        return generalService.selectSingle(criteria.getMetaData());
    }

    @Override
    public AccountRelationship selectAccountRelationshipByEmail(String email) throws Exception {
        Criteria<AccountRelationshipM> criteria = new Criteria<>(accountRelationshipM);
        criteria.where(accountRelationshipM.password.email.eq(email));
        return generalService.selectSingle(criteria.getMetaData());
    }

    @Override
    public AccountRelationship selectAccountRelationshipByEmailAndActivationToken(String email, String token) throws Exception {
        Criteria<AccountRelationshipM> criteria = new Criteria<>(accountRelationshipM);
        criteria.where(accountRelationshipM.account.email.eq(email).and(accountRelationshipM.activation.token.eq(token)));
        return generalService.selectSingle(criteria.getMetaData());
    }

}