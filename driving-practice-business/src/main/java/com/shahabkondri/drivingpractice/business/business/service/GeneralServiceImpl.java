package com.shahabkondri.drivingpractice.business.business.service;


import com.shahabkondri.drivingpractice.api.GeneralService;
import com.shahabkondri.drivingpractice.business.dao.interfaces.GeneralDaoLocal;
import com.shahabkondri.drivingpractice.core.data.CriteriaMetaData;
import com.shahabkondri.drivingpractice.core.data.DeleteMetaData;
import com.shahabkondri.drivingpractice.core.data.QueryMetaData;
import com.shahabkondri.drivingpractice.core.data.UpdateMetaData;
import com.shahabkondri.drivingpractice.entity.abstracts.BaseIdentity;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.io.Serializable;
import java.util.List;

@Stateless
@Remote(GeneralService.class)
public class GeneralServiceImpl implements GeneralService, Serializable {
    private static final long serialVersionUID = -4556864252327429002L;

    @EJB
    private GeneralDaoLocal dao;

    public GeneralServiceImpl() {
    }


    @Override
    public <T extends Serializable> T selectSingle(CriteriaMetaData metaData) throws Exception {
        return dao.selectSingle(metaData);
    }

    @Override
    public <T extends Serializable> T selectFirst(CriteriaMetaData metaData) throws Exception {
        return dao.selectFirst(metaData);
    }

    @Override
    public <T extends Serializable> List<T> select(CriteriaMetaData metaData) throws Exception {
        return dao.select(metaData);
    }

    @Override
    public int selectCount(CriteriaMetaData metaData) throws Exception {
        return dao.selectCount(metaData);
    }

    @Override
    public <T extends Serializable> T save(T t) throws Exception {
        return dao.save(t);
    }

    @Override
    public <T extends Serializable> List<T> save(List<T> ts) throws Exception {
        return dao.save(ts);
    }

    @Override
    public <T extends Serializable> T update(T t) throws Exception {
        return dao.update(t);
    }

    @Override
    public <T extends Serializable> List<T> update(List<T> ts) throws Exception {
        return dao.update(ts);
    }

    @Override
    public <T extends Serializable> List<T> saveOrUpdate(List<T> ts) throws Exception {
        return dao.saveOrUpdate(ts);
    }

    @Override
    public <T extends Serializable> T saveOrUpdate(T t) throws Exception {
        return dao.saveOrUpdate(t);
    }

    @Override
    public <T extends BaseIdentity<?>> void delete(Class<T> clas, Serializable... ids) throws Exception {
        dao.delete(clas, ids);
    }

    @Override
    public <T extends Serializable> T selectById(Class<T> clas, Serializable id) throws Exception {
        return dao.selectById(clas, id);
    }

    @Override
    public <T extends Serializable> T selectById(Class<T> clas, Serializable id, String graph) throws Exception {
        return dao.selectById(clas, id, graph);
    }

    @Override
    public <T extends Serializable> T selectSingle(QueryMetaData metaData) throws Exception {
        return dao.selectSingle(metaData);
    }

    @Override
    public <T extends Serializable> T selectFirst(QueryMetaData metaData) throws Exception {
        return dao.selectFirst(metaData);
    }

    @Override
    public <T extends Serializable> List<T> select(QueryMetaData metaData) throws Exception {
        return dao.select(metaData);
    }

    @Override
    public <T extends Serializable> T selectQuerySingle(QueryMetaData metaData) throws Exception {
        return dao.selectQuerySingle(metaData);
    }

    @Override
    public <T extends Serializable> T selectQueryFirst(QueryMetaData metaData) throws Exception {
        return dao.selectQueryFirst(metaData);
    }

    @Override
    public <T extends Serializable> List<T> selectQuery(QueryMetaData metaData) throws Exception {
        return dao.selectQuery(metaData);
    }

    @Override
    public <T extends Serializable> T selectStoredProcedureSingle(QueryMetaData metaData) throws Exception {
        return dao.selectStoredProcedureSingle(metaData);
    }

    @Override
    public <T extends Serializable> T selectStoredProcedureFirst(QueryMetaData metaData) throws Exception {
        return dao.selectStoredProcedureFirst(metaData);
    }

    @Override
    public <T extends Serializable> List<T> selectStoredProcedure(QueryMetaData metaData) throws Exception {
        return dao.selectStoredProcedure(metaData);
    }

    @Override
    public int executeNamedStoredProcedureQuery(QueryMetaData metaData) throws Exception {
        return dao.executeNamedStoredProcedureQuery(metaData);
    }

    @Override
    public int executeNamedQuery(QueryMetaData metaData) throws Exception {
        return dao.executeNamedQuery(metaData);
    }

    @Override
    public int executeQuery(QueryMetaData metaData) throws Exception {
        return dao.executeQuery(metaData);
    }

    @Override
    public int update(UpdateMetaData updateMetaData) throws Exception {
        return dao.update(updateMetaData);
    }

    @Override
    public int delete(DeleteMetaData deleteMetaData) throws Exception {
        return dao.delete(deleteMetaData);
    }

    @Override
    public void flush() throws Exception {
        dao.flush();
    }

    @Override
    public void clear() throws Exception {
        dao.clear();
    }
}
