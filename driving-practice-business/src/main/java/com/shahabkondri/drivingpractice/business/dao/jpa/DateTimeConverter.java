package com.shahabkondri.drivingpractice.business.dao.jpa;

import com.shahabkondri.drivingpractice.core.time.DateTime;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.sql.Timestamp;

@Converter(autoApply = true)
public class DateTimeConverter implements AttributeConverter<DateTime, Timestamp> {

    /**
     * Converts DateTime to database timestamp.
     *
     * @param dateTime DateTime
     * @return database timestamp
     */
    @Override
    public Timestamp convertToDatabaseColumn(DateTime dateTime) {
        if (dateTime == null) {
            return null;
        }
        return Timestamp.valueOf(dateTime.toLocalDateTime());
    }

    /**
     * Converts database timestamp to DateTime.
     *
     * @param timestamp database timestamp
     * @return DateTime
     */
    @Override
    public DateTime convertToEntityAttribute(Timestamp timestamp) {
        if (timestamp == null) {
            return null;
        }
        return DateTime.from(timestamp.toLocalDateTime());
    }

}
