package com.shahabkondri.drivingpractice.business.business.service;


import com.shahabkondri.drivingpractice.api.GeneralService;
import com.shahabkondri.drivingpractice.api.question.QuestionService;
import com.shahabkondri.drivingpractice.core.data.Criteria;
import com.shahabkondri.drivingpractice.entity.Question;
import com.shahabkondri.drivingpractice.entity.QuestionM;
import com.shahabkondri.drivingpractice.entity.enumerations.ExamType;
import com.shahabkondri.drivingpractice.entity.enumerations.QuestionType;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.util.Collections;
import java.util.List;

/**
 * Created by shahab on 5/28/15.
 */

@Stateless
@Remote(QuestionService.class)
public class QuestionServiceImpl implements QuestionService {

    private final QuestionM questionM = QuestionM.question;

    @EJB
    private GeneralService generalService;

    public QuestionServiceImpl() {
    }

    @Override
    public List<Question> selectQuestionsByQuestionTypeAndExamType(QuestionType questionType, ExamType examType) throws Exception {
        Criteria<Question> criteria = new Criteria<>(questionM);
        criteria.where(questionM.questionType.eq(questionType), questionM.examType.eq(examType));
        return generalService.select(criteria.getMetaData());
    }

    @Override
    public Question selectQuestionByRowNumberAndQuestionTypeAndQuestionExam(int rowNumber, QuestionType questionType, ExamType examType) throws Exception {
        Criteria<Question> criteria = new Criteria<>(questionM);
        criteria.where(questionM.questionType.eq(questionType), questionM.examType.eq(examType), questionM.questionNumber.eq(rowNumber));
        Question question = generalService.selectSingle(criteria.getMetaData());
        if (question != null) {
            Collections.shuffle(question.getOptions());
        }
        return question;
    }

    @Override
    public long selectCount(QuestionType questionType, ExamType examType) throws Exception {
        Criteria<Question> criteria = new Criteria<>(questionM);
        criteria.where(questionM.questionType.eq(questionType), questionM.examType.eq(examType));
        long count = generalService.selectCount(criteria.getMetaData());
        return count;
    }
}
