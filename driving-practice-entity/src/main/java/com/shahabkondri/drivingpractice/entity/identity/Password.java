package com.shahabkondri.drivingpractice.entity.identity;

import com.shahabkondri.drivingpractice.entity.abstracts.BaseEntity;
import org.hibernate.envers.Audited;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;


@Entity
@Table(name = "PASSWORDS",
        indexes = @Index(name = "IDX_PASSWORD_EMAIL", columnList = "EMAIL"),
        uniqueConstraints = @UniqueConstraint(name = "UK_PASSWORD_EMAIL", columnNames = {"EMAIL"}))
@Audited
public class Password extends BaseEntity<Long> {
    private static final long serialVersionUID = -1374514583615628231L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PasswordSeq")
    @SequenceGenerator(name = "PasswordSeq", sequenceName = "PASSWORDS_SEQ")
    @Column(name = "PASSWORD_ID")
    private Long id;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "PASSWORD")
    private String password;

    public Password() {
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @NotBlank(message = "{email.not.blank}")
    @Email(message = "{email.is.invalid}")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @NotBlank(message = "{password.not.blank}")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Password password1 = (Password) o;

        if (id != null ? !id.equals(password1.id) : password1.id != null) return false;
        if (email != null ? !email.equals(password1.email) : password1.email != null) return false;
        return password != null ? password.equals(password1.password) : password1.password == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        return result;
    }
}
