package com.shahabkondri.drivingpractice.entity.abstracts;

import java.io.Serializable;

public abstract class BaseNonentity<ID extends Serializable> implements Serializable {
    private static final long serialVersionUID = -3164199209983061534L;

    public BaseNonentity() {
    }

    public abstract ID getId();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BaseNonentity that = (BaseNonentity) o;
        return !(getId() != null ? !getId().equals(that.getId()) : that.getId() != null);
    }

    @Override
    public int hashCode() {
        return getId() != null ? getId().hashCode() : 0;
    }

}
