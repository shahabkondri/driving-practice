package com.shahabkondri.drivingpractice.entity.enumerations;

/**
 * Created by shahab on 05/12/15.
 */
public enum ResultType {
    FAILED_SIGN, FAILED_RULE, PASSED_SIGN, PASSED_RULE;
}
