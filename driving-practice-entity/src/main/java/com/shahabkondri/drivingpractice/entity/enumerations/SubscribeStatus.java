package com.shahabkondri.drivingpractice.entity.enumerations;

/**
 * Created by shahab on 16/12/15.
 */
public enum SubscribeStatus {

    UN_CONFIRMED, CONFIRMED, UN_SUBSCRIBE;
}
