package com.shahabkondri.drivingpractice.entity.identity;

import com.shahabkondri.drivingpractice.core.time.DateTime;
import com.shahabkondri.drivingpractice.entity.abstracts.BaseEntity;
import org.hibernate.envers.Audited;
import org.hibernate.validator.constraints.Email;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "STAY_LOGINS",
        indexes = @Index(name = "IDX_STAY_LOGIN_EMAIL", columnList = "EMAIL"),
        uniqueConstraints = @UniqueConstraint(name = "IDX_STAY_LOGIN_EMAIL", columnNames = {"EMAIL"}))
@Audited
public class StayLogin extends BaseEntity<Long> {
    private static final long serialVersionUID = -5623652961709669239L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "StayLoginSeq")
    @SequenceGenerator(name = "StayLoginSeq", sequenceName = "STAY_LOGIN_SEQ")
    @Column(name = "STAY_LOGIN_ID")
    private Long id;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "TOKEN")
    private String token;

    @Column(name = "DE_ACTIVE_TIME_FROM")
    private DateTime createTime;

    public StayLogin() {
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @NotNull
    @Email(message = "{email.is.invalid}")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @NotNull
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @NotNull
    public DateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(DateTime createTime) {
        this.createTime = createTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        StayLogin stayLogin = (StayLogin) o;

        if (id != null ? !id.equals(stayLogin.id) : stayLogin.id != null) return false;
        if (email != null ? !email.equals(stayLogin.email) : stayLogin.email != null) return false;
        if (token != null ? !token.equals(stayLogin.token) : stayLogin.token != null) return false;
        return createTime != null ? createTime.equals(stayLogin.createTime) : stayLogin.createTime == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (token != null ? token.hashCode() : 0);
        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
        return result;
    }
}
