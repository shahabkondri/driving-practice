package com.shahabkondri.drivingpractice.entity.enumerations;

/**
 * Created by shahabfereydooni on 8/21/15.
 */
public enum Language {

    PERSIAN("Persian", "فارسی"),
    ENGLISH("English", "English"),
    TURKISH("Turkish", "Türkçe"),
    GERMAN("German", "Deutsch");

    private String english;
    private String locale;

    Language(String english, String locale) {
        this.english = english;
        this.locale = locale;
    }

    public String getEnglish() {
        return english;
    }

    public String getLocale() {
        return locale;
    }

    public void setEnglish(String english) {
        this.english = english;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }
}
