package com.shahabkondri.drivingpractice.entity;

import com.shahabkondri.drivingpractice.entity.abstracts.BaseEntity;
import com.shahabkondri.drivingpractice.entity.identity.enumeration.UserAgentType;
import eu.bitwalker.useragentutils.Browser;
import eu.bitwalker.useragentutils.OperatingSystem;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by shahabfereydooni on 8/9/15.
 */

@Entity
@Table(name = "USER_AGENTS")
public class UserAgent extends BaseEntity<Long> {
    private static final long serialVersionUID = 3436532377384543224L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "UserAgentSeq")
    @SequenceGenerator(name = "UserAgentSeq", sequenceName = "USER_AGENT_SEQ")
    @Column(name = "USER_AGENR_ID")
    private Long id;

    @Column(name = "BROWSER")
    @Enumerated(EnumType.STRING)
    private Browser browser;

    @Column(name = "OPERATION_SYSTEM")
    @Enumerated(EnumType.STRING)
    private OperatingSystem operatingSystem;

    @Column(name = "IP")
    private String ip;

    @Column(name = "NAME")
    @Enumerated(EnumType.STRING)
    private UserAgentType type;

    public UserAgent() {
    }

    public UserAgent(Browser browser, OperatingSystem operatingSystem, String ip) {
        this.browser = browser;
        this.operatingSystem = operatingSystem;
        this.ip = ip;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @NotNull
    public Browser getBrowser() {
        return browser;
    }

    public void setBrowser(Browser browser) {
        this.browser = browser;
    }

    @NotNull
    public OperatingSystem getOperatingSystem() {
        return operatingSystem;
    }

    public void setOperatingSystem(OperatingSystem operatingSystem) {
        this.operatingSystem = operatingSystem;
    }

    @NotNull
    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public UserAgentType getType() {
        return type;
    }

    public void setType(UserAgentType type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        UserAgent userAgent = (UserAgent) o;

        if (id != null ? !id.equals(userAgent.id) : userAgent.id != null) return false;
        if (browser != userAgent.browser) return false;
        if (operatingSystem != userAgent.operatingSystem) return false;
        if (ip != null ? !ip.equals(userAgent.ip) : userAgent.ip != null) return false;
        return type == userAgent.type;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + (browser != null ? browser.hashCode() : 0);
        result = 31 * result + (operatingSystem != null ? operatingSystem.hashCode() : 0);
        result = 31 * result + (ip != null ? ip.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        return result;
    }
}
