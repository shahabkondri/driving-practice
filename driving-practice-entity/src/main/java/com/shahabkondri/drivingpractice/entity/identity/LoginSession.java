package com.shahabkondri.drivingpractice.entity.identity;

import com.shahabkondri.drivingpractice.entity.UserAgent;

import java.io.Serializable;

/**
 * Created by shahabfereydooni on 8/9/15.
 */
public class LoginSession implements Serializable {
    private static final long serialVersionUID = 7890264753962717043L;

    private AccountRelationship accountRelationship;

    private UserAgent userAgent;

    public LoginSession() {
    }

    public LoginSession(AccountRelationship accountRelationship, UserAgent userAgent) {
        this.accountRelationship = accountRelationship;
        this.userAgent = userAgent;
    }

    public AccountRelationship getAccountRelationship() {
        return accountRelationship;
    }

    public void setAccountRelationship(AccountRelationship accountRelationship) {
        this.accountRelationship = accountRelationship;
    }

    public UserAgent getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(UserAgent userAgent) {
        this.userAgent = userAgent;
    }
}
