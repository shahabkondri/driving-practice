package com.shahabkondri.drivingpractice.entity;

import com.shahabkondri.drivingpractice.entity.abstracts.BaseEntity;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by shahabkondri on 26/11/15.
 */


@Entity
@Table(name = "OPTION")
public class Option extends BaseEntity<Long> {
    private static final long serialVersionUID = 4444446873194262505L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "OptionSeq")
    @SequenceGenerator(name = "OptionSeq", sequenceName = "OPTION_SEQ")
    @Column(name = "OPTION_ID")
    private Long id;

    @Column(name = "TITLE")
    private String title;

    @Column(name = "CORRECT_ANSWER")
    private boolean correctAnswer;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "QUESTION_ID")
    private Question question;


    public Option() {
    }

    public Option(String title, boolean correctAnswer, Question question) {
        this.title = title;
        this.correctAnswer = correctAnswer;
        this.question = question;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @NotBlank
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @NotNull
    public boolean isCorrectAnswer() {
        return correctAnswer;
    }

    public void setCorrectAnswer(boolean correctAnswer) {
        this.correctAnswer = correctAnswer;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Option option = (Option) o;

        if (correctAnswer != option.correctAnswer) return false;
        if (id != null ? !id.equals(option.id) : option.id != null) return false;
        if (title != null ? !title.equals(option.title) : option.title != null) return false;
        return !(question != null ? !question.equals(option.question) : option.question != null);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (correctAnswer ? 1 : 0);
        result = 31 * result + (question != null ? question.hashCode() : 0);
        return result;
    }
}
