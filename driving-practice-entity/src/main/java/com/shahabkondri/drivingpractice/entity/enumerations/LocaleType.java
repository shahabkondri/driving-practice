package com.shahabkondri.drivingpractice.entity.enumerations;


/**
 * Created by shahab on 5/28/15.
 */
public enum LocaleType {

    ENGLISH("en", Language.ENGLISH),
    PERSIAN("fa", Language.PERSIAN),
    TURKISH("tr", Language.TURKISH),
    GERMAN("de", Language.GERMAN);

    private String locale;
    private Language language;

    private static final LocaleType DEFAULT = LocaleType.PERSIAN;

    LocaleType(String locale, Language language) {
        this.locale = locale;
        this.language = language;
    }

    public static boolean isDefault(LocaleType localeType) {
        return localeType.equals(DEFAULT);
    }

    public static LocaleType getLocaleType(String locale) {
        LocaleType localeType = PERSIAN;
        for (LocaleType type : LocaleType.values()) {
            if (!type.getLocale().equalsIgnoreCase(locale)) continue;
            localeType = type;
            break;
        }
        return localeType;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }
}
