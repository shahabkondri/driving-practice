package com.shahabkondri.drivingpractice.entity.identity;

import com.shahabkondri.drivingpractice.core.time.DateTime;
import com.shahabkondri.drivingpractice.entity.abstracts.BaseEntity;
import com.shahabkondri.drivingpractice.entity.identity.enumeration.ForgotPasswordType;
import org.hibernate.envers.Audited;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "FORGOT_PASSWORDS")
@Audited
public class ForgotPassword extends BaseEntity<Long> {
    private static final long serialVersionUID = -717237225077104591L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ForgotPasswordSeq")
    @SequenceGenerator(name = "ForgotPasswordSeq", sequenceName = "FORGOT_PASSWORDS_SEQ")
    @Column(name = "FORGOT_PASSWORD_ID")
    private Long id;

    @Column(name = "TOKEN")
    private String token;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "REQUEST_TIME")
    private DateTime requestTime;

    @Column(name = "RESET_PASSWORD_TIME")
    private DateTime resetPasswordTime;

    @Column(name = "FORGET_PASSWORD_TYPE")
    @Enumerated(EnumType.STRING)
    private ForgotPasswordType forgotPasswordType;

    public ForgotPassword() {
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @NotBlank
    public String getToken() {
        return token;
    }

    public void setToken(String forgotCode) {
        this.token = forgotCode;
    }

    @NotBlank(message = "{email.not.blank}")
    @Email(message = "{email.is.invalid}")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @NotNull
    public DateTime getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(DateTime requestTime) {
        this.requestTime = requestTime;
    }

    public DateTime getResetPasswordTime() {
        return resetPasswordTime;
    }

    public void setResetPasswordTime(DateTime resetPasswordTime) {
        this.resetPasswordTime = resetPasswordTime;
    }

    @NotNull
    public ForgotPasswordType getForgotPasswordType() {
        return forgotPasswordType;
    }

    public void setForgotPasswordType(ForgotPasswordType forgotPasswordType) {
        this.forgotPasswordType = forgotPasswordType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        ForgotPassword that = (ForgotPassword) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (token != null ? !token.equals(that.token) : that.token != null) return false;
        return email != null ? email.equals(that.email) : that.email == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + (token != null ? token.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        return result;
    }
}
