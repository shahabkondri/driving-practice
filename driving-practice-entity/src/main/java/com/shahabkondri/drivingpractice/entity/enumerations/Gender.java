package com.shahabkondri.drivingpractice.entity.enumerations;

public enum Gender {

    MALE, FEMALE
}
