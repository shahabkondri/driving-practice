package com.shahabkondri.drivingpractice.entity.enumerations;

/**
 * Created by shahab on 05/12/15.
 */
public enum TestType {

    SIGN_MARATHON, RULE_MARATHON, EXAM_MODE, AZ_WRITTEN_EXAMINATION;

}
