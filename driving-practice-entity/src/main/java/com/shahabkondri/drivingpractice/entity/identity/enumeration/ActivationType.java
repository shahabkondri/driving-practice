package com.shahabkondri.drivingpractice.entity.identity.enumeration;

public enum ActivationType {

    Pending(false), Active(true), DeActive(false);

    private boolean active;

    ActivationType(boolean active) {
        this.active = active;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
