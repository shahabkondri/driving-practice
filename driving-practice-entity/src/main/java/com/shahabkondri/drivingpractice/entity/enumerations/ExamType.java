package com.shahabkondri.drivingpractice.entity.enumerations;

/**
 * Created by shahabkondri on 29/11/15.
 */
public enum ExamType {
    DRIVER, MOTORCYCLE, BUS, TRUCK, AIR_BRAKE
}
