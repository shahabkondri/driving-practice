package com.shahabkondri.drivingpractice.entity;

import com.shahabkondri.drivingpractice.entity.abstracts.BaseEntity;
import com.shahabkondri.drivingpractice.entity.enumerations.SubscribeStatus;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created by shahab on 16/12/15.
 */

@Entity
@Table(name = "SUBSCRIBE", uniqueConstraints = {@UniqueConstraint(name = "UK_SUBSCRIBE_EMAIL", columnNames = {"EMAIL"})})
public class Subscribe extends BaseEntity<Long> {
    private static final long serialVersionUID = -101407557870763942L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SubscribeSeq")
    @SequenceGenerator(name = "SubscribeSeq", sequenceName = "SUBSCRIBE_SEQ")
    @Column(name = "SUBSCRIBE_ID")
    private Long id;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;

    @Column(name = "TOKEN")
    private String token;

    @Column(name = "STATUS")
    @Enumerated(EnumType.STRING)
    private SubscribeStatus status;

    public Subscribe() {
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @NotBlank(message = "{email.not.blank}")
    @Email(message = "{email.is.invalid}")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @NotNull
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @NotNull
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @NotNull
    public SubscribeStatus getStatus() {
        return status;
    }

    public void setStatus(SubscribeStatus status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Subscribe subscribe = (Subscribe) o;

        if (id != null ? !id.equals(subscribe.id) : subscribe.id != null) return false;
        if (email != null ? !email.equals(subscribe.email) : subscribe.email != null) return false;
        if (date != null ? !date.equals(subscribe.date) : subscribe.date != null) return false;
        return !(token != null ? !token.equals(subscribe.token) : subscribe.token != null);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + (token != null ? token.hashCode() : 0);
        return result;
    }
}
