package com.shahabkondri.drivingpractice.entity.identity;


import com.shahabkondri.drivingpractice.core.time.Date;
import com.shahabkondri.drivingpractice.entity.abstracts.BaseEntity;
import com.shahabkondri.drivingpractice.entity.enumerations.Gender;
import org.hibernate.envers.Audited;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;

@Entity
@Table(name = "ACCOUNTS",
        indexes = @Index(name = "IDX_ACCOUNT_EMAIL", columnList = "EMAIL"),
        uniqueConstraints = @UniqueConstraint(name = "UK_ACCOUNT_EMAIL", columnNames = {"EMAIL"}))
@Audited
public class Account extends BaseEntity<Long> {
    private static final long serialVersionUID = 6801286189953397084L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "AccountSeq")
    @SequenceGenerator(name = "AccountSeq", sequenceName = "ACCOUNTS_SEQ")
    @Column(name = "ACCOUNT_ID")
    private Long id;

    @Column(name = "FIRST_NAME")
    private String firstName;

    @Column(name = "LAST_NAME")
    private String lastName;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "BIRTHDAY")
    private Date birthday;

    @Column(name = "GENDER")
    @Enumerated(EnumType.STRING)
    private Gender gender;

    @Column(name = "PROFILE_IMAGE")
    @Lob
    private byte[] profileImage;

    public Account() {
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @NotBlank(message = "{first.name.not.blank}")
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @NotBlank(message = "{last.name.not.blank}")
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @NotBlank(message = "{email.not.blank}")
    @Email(message = "{email.is.invalid}")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public byte[] getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(byte[] profileImage) {
        this.profileImage = profileImage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Account account = (Account) o;

        if (id != null ? !id.equals(account.id) : account.id != null) return false;
        return email != null ? email.equals(account.email) : account.email == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        return result;
    }
}
