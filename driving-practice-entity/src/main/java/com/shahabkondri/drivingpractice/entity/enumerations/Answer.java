package com.shahabkondri.drivingpractice.entity.enumerations;

public enum Answer {

    CORRECT, WRONG, NO_ANSWERING;
}
