package com.shahabkondri.drivingpractice.entity.identity.enumeration;

/**
 * Created by shahab on 16/12/15.
 */
public enum UserAgentType {

    CONTACT, SUBSCRIBE, SESSION;
}
