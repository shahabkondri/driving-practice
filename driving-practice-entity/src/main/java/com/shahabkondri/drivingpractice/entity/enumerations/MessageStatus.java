package com.shahabkondri.drivingpractice.entity.enumerations;

/**
 * Created by shahab on 15/12/15.
 */
public enum MessageStatus {
    READ, UN_READ, ANSWERED
}
