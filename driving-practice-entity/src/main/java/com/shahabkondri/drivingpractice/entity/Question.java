package com.shahabkondri.drivingpractice.entity;

import com.shahabkondri.drivingpractice.entity.abstracts.BaseEntity;
import com.shahabkondri.drivingpractice.entity.enumerations.Answer;
import com.shahabkondri.drivingpractice.entity.enumerations.ExamType;
import com.shahabkondri.drivingpractice.entity.enumerations.Location;
import com.shahabkondri.drivingpractice.entity.enumerations.QuestionType;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.List;

/**
 * Created by shahabkondri on 26/11/15.
 */


@Entity
@Table(name = "QUESTION")
public class Question extends BaseEntity<Long> {
    private static final long serialVersionUID = 6402101328553442252L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "QuestionSeq")
    @SequenceGenerator(name = "QuestionSeq", sequenceName = "Question_SEQ")
    @Column(name = "QUESTION_ID")
    private Long id;

    @Column(name = "TITLE")
    private String title;

    @Column(name = "DESCRIPTION", columnDefinition="TEXT")
    private String description;

    @Column(name = "HINT")
    private String hint;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "question")
    private List<Option> options;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Option selectedOption;

    @Column(name = "IMAGE")
    @Lob
    private byte[] image;

    @Column(name = "QUESTION_TYPE")
    @Enumerated(EnumType.STRING)
    private QuestionType questionType;

    @Column(name = "EXAM_TYPE")
    @Enumerated(EnumType.STRING)
    private ExamType examType;

    @Column(name = "LOCATION")
    @Enumerated(EnumType.STRING)
    private Location location;

    @Column(name = "ANSWER")
    @Enumerated(EnumType.STRING)
    private Answer answer;

    @Column(name = "QUESTION_NUMBER")
    private int questionNumber;

    @Column(name = "TOKEN")
    private String token;


    public Question() {
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @NotBlank
    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHint() {
        return hint;
    }

    public void setHint(String hint) {
        this.hint = hint;
    }

    public void setTitle(String question) {
        this.title = question;
    }

    @NotNull
    public List<Option> getOptions() {
        return options;
    }

    public void setOptions(List<Option> options) {
        this.options = options;
    }

    public Option getSelectedOption() {
        return selectedOption;
    }

    public void setSelectedOption(Option selectedOption) {
        this.selectedOption = selectedOption;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    @NotNull
    public QuestionType getQuestionType() {
        return questionType;
    }

    public void setQuestionType(QuestionType questionType) {
        this.questionType = questionType;
    }

    @NotNull
    public ExamType getExamType() {
        return examType;
    }

    public void setExamType(ExamType examType) {
        this.examType = examType;
    }

    @NotNull
    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Answer getAnswer() {
        return answer;
    }

    public void setAnswer(Answer answer) {
        this.answer = answer;
    }

    @NotNull
    public int getQuestionNumber() {
        return questionNumber;
    }

    public void setQuestionNumber(int questionRowNumber) {
        this.questionNumber = questionRowNumber;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Question question = (Question) o;

        if (id != null ? !id.equals(question.id) : question.id != null) return false;
        if (title != null ? !title.equals(question.title) : question.title != null) return false;
        if (options != null ? !options.equals(question.options) : question.options != null) return false;
        if (!Arrays.equals(image, question.image)) return false;
        if (questionType != question.questionType) return false;
        if (examType != question.examType) return false;
        if (location != question.location) return false;
        return answer == question.answer;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (options != null ? options.hashCode() : 0);
        result = 31 * result + Arrays.hashCode(image);
        result = 31 * result + (questionType != null ? questionType.hashCode() : 0);
        result = 31 * result + (examType != null ? examType.hashCode() : 0);
        result = 31 * result + (location != null ? location.hashCode() : 0);
        result = 31 * result + (answer != null ? answer.hashCode() : 0);
        return result;
    }
}
