package com.shahabkondri.drivingpractice.entity.enumerations;

/**
 * Created by shahabkondri on 29/11/15.
 */
public enum QuestionType {
    SIGN, RULE, AZ_WRITTEN
}