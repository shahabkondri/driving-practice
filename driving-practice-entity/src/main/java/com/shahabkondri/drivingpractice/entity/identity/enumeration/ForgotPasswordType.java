package com.shahabkondri.drivingpractice.entity.identity.enumeration;

/**
 * Created by shahab on 5/4/15.
 */
public enum ForgotPasswordType {

    WAITING, CHANGED, EXPIRED

}
