package com.shahabkondri.drivingpractice.entity.abstracts;

import java.io.Serializable;

public interface BaseIdentity<ID extends Serializable> extends Serializable {

    ID getId();

    void setId(ID id);

}
