package com.shahabkondri.drivingpractice.entity.identity;


import com.shahabkondri.drivingpractice.entity.abstracts.BaseEntity;
import com.shahabkondri.drivingpractice.entity.enumerations.LocaleType;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "ACCOUNT_RELATIONSHIPS")
@Audited
public class AccountRelationship extends BaseEntity<Long> {
    private static final long serialVersionUID = 359636247725414617L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "AccountRelationshipSeq")
    @SequenceGenerator(name = "AccountRelationshipSeq", sequenceName = "ACCOUNT_RELATIONSHIP_SEQ")
    @Column(name = "ACCOUNT_RELATIONSHIP_ID")
    private Long id;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Account account;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Password password;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Activation activation;

    @Enumerated(EnumType.STRING)
    private LocaleType locale;

    public AccountRelationship() {
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @NotNull
    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    @NotNull
    public Password getPassword() {
        return password;
    }

    public void setPassword(Password password) {
        this.password = password;
    }

//    @NotNull
    public Activation getActivation() {
        return activation;
    }

    public void setActivation(Activation activation) {
        this.activation = activation;
    }

    public LocaleType getLocale() {
        return locale;
    }

    public void setLocale(LocaleType locale) {
        this.locale = locale;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        AccountRelationship that = (AccountRelationship) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (account != null ? !account.equals(that.account) : that.account != null) return false;
        return password != null ? password.equals(that.password) : that.password == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + (account != null ? account.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        return result;
    }
}
