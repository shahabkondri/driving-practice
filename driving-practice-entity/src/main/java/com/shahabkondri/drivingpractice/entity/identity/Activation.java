package com.shahabkondri.drivingpractice.entity.identity;

import com.shahabkondri.drivingpractice.core.time.DateTime;
import com.shahabkondri.drivingpractice.entity.abstracts.BaseEntity;
import com.shahabkondri.drivingpractice.entity.identity.enumeration.ActivationType;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "ACTIVATIONS")
@Audited
public class Activation extends BaseEntity<Long> {
    private static final long serialVersionUID = -8394892352731424496L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ActivationSeq")
    @SequenceGenerator(name = "ActivationSeq", sequenceName = "ACTIVATIONS_SEQ")
    @Column(name = "ACTIVATION_ID")
    private Long id;

    @Column(name = "ACTIVATION_STATUS")
    @Enumerated(EnumType.STRING)
    private ActivationType activationStatus;

    @Column(name = "DE_ACTIVE_TIME_FROM")
    private DateTime deActiveTimeFrom;

    @Column(name = "DE_ACTIVE_TIME_To")
    private DateTime deActiveTimeTo;

    @Column(name = "REGISTER_DATE")
    private DateTime registerTime;

    @Column(name = "TOKEN")
    private String token;

    public Activation() {
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @NotNull
    public ActivationType getActivationStatus() {
        return activationStatus;
    }

    public void setActivationStatus(ActivationType activationStatus) {
        this.activationStatus = activationStatus;
    }

    public DateTime getDeActiveTimeFrom() {
        return deActiveTimeFrom;
    }

    public void setDeActiveTimeFrom(DateTime deActiveTimeFrom) {
        this.deActiveTimeFrom = deActiveTimeFrom;
    }

    public DateTime getDeActiveTimeTo() {
        return deActiveTimeTo;
    }

    public void setDeActiveTimeTo(DateTime deActiveTimeTo) {
        this.deActiveTimeTo = deActiveTimeTo;
    }

    @NotNull
    public DateTime getRegisterTime() {
        return registerTime;
    }

    public void setRegisterTime(DateTime registerTime) {
        this.registerTime = registerTime;
    }

    @NotNull
    public String getToken() {
        return token;
    }

    public void setToken(String activationCode) {
        this.token = activationCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Activation that = (Activation) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        return token != null ? token.equals(that.token) : that.token == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + (token != null ? token.hashCode() : 0);
        return result;
    }
}
